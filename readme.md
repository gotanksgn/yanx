# 技术选型

    前端：vue
    后端：jdk8+springboot
    缓存：caffeine
    模板：freemarker
    缓存：redis
    数据库：MySQL
    持久层：写：jpa，读：jooq
    连接池：hikari
    解析excel：easyexcel
    JSON：jackson
    解析XML：XStream+dom4j
    消息：RabbitMQ
    构建：gradle
    
    对于services下的业务服务：
    领域模型采用贫血模型（非失血模型）
    分层采用ddd+cqrs模式
    
    ......

---------------------------

## 环境须知：

- JDK8
- Gradle6.9.1
- IDEA`lombok插件` 并设置Setting - Build - Compiler - AnnotationProcessors - 开启右侧勾选

## 运行步骤:

- 1、以gradle方式导入工程
- 2、刷新gradle，自动下载jar包
- 3、配置都已设置好，直接在对应模块下开发

## 版本说明


### v2.2.0

#### 2024-09-18

###### 功能优化

1. 增加CommonExecutor，用于处理非数据库逻辑，以及复杂业务
2. 将CommandExecutor废弃，改名为DomainExecutor
3. 同步修改代码生成模块

###### 组件升级

无

---

### v2.1.2

#### 2024-07-16

###### 功能优化

1. 增加报错信息发送短信注解@ErrorNotice

###### 组件升级

1. springcloud alibaba 升级到2021.0.6.1

---

### v2.1.1

#### 2024-2-21

###### 功能优化

1. 启动类增加ipv6地址
2. 业务异常类增加构造方法
3. 将逻辑删除从基类中剥离出到BaseLogicEntity中

###### 组件升级

1. 无

---

### v2.1.0

#### 2023-12-31

###### 功能优化

1. 暂无

###### 组件升级

1. springboot升级到2.7.18
2. springcloud升级到2021.0.9
3. hutool升级到5.8.24
4. fastjson2升级到2.0.43

---

### v2.0.0

#### 2023-06-11

###### 功能优化

1. 异常类优化
2. 命令模式代码优化
3. 完善登录鉴权功能
4. common优化：代码生成模块去掉@Column判断
5. common优化：修改部分属性
6. Json_增加toTree方法
7. 异常返回状态码优化
8. 增加操作日志注解
9. 通用tree模型优化调整
10. 增加操作日志入库切片
11. 增加地址查询工具类，增加国际化工具类
12. 增加链式调用命令方法

###### 组件升级

1. springboot升级到2.7.12
2. springcloud升级到2021.0.7
3. springcloud alibaba 升级到2021.0.5.0
4. mapstruct升级到1.5.5.Final
5. hutool升级到5.8.19
6. springdoc升级到1.7.0
7. fastjson2升级到2.0.33
8. 引入oshi-core 6.4.2

---

### v1.6.0

#### 2022-03-08

###### 功能优化

1. 引入QueryDsl

###### 组件升级

1. springboot升级为2.6.4
2. springcloud升级为2021.0.1
3. springboot alibaba 升级为2021.0.1.0
4. dubbo升级为3.0.5
5. swagger改为springdoc

---

### Version: 1.5.0

    Date: 20200413
    Modify By: gotanks 
    Desc:
    1、增加分页功能
    2、网关增加ssl
    3、增加redis工具

### Version: 1.4.0

    Date: 20200228
    Modify By: gotanks 
    Desc:
    1、引入MapStruct，优化代码逻辑
    2、BaseEntity增加部分方法
    3、将api模块独立出来

### Version: 1.3.0

    Date: 20200208
    Modify By: gotanks 
    Desc:
    1、springcloud升级为Hoxton.SR1
    2、springboot升级为2.2.4
    3、springcloud alibaba升级为2.2.0
    4、优化异常返回代码逻辑

### Version: 1.2.0

    Date: 20191127
    Modify By: gotanks 
    Desc:
    1、更改目录结构
    2、增加jooq自动生成代码

### Version: 1.1.0

    Date: 20191120
    Modify By: gotanks 
    Desc:
    1、更改目录结构
    2、更新部分jar包版本

### Version: 1.0.0

    Date: 20191027
    Modify By: gotanks 
    Desc:
    1、初始化工程