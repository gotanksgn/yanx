package com.yanx.system.base.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.yanx.common.core.vo.BasePageVo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 菜单权限VO
 *
 * @author: gotanks
 * @create: 2023-2-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "菜单权限视图模型")
public class SysRoleVo extends BasePageVo<Long> {

    private static final long serialVersionUID = 1L;

    //兼容ruoyi
    @JsonSerialize(using = ToStringSerializer.class)
    @Schema(name = "角色ID")
    public Long getRoleId() {
        return this.getId();
    }

    @Schema(name = "角色名称")
    private String roleName;

    @Schema(name = "角色ID")
    private String roleKey;

    @Schema(name = "角色ID")
    private Integer roleSort;

    @Schema(name = "数据范围")
    private String dataScope;

    @Schema(name = "菜单树选择项是否关联显示")
    private boolean menuCheckStrictly;

    @Schema(name = "部门树选择项是否关联显示")
    private boolean deptCheckStrictly;

    @Schema(name = "角色状态")
    private String status;

    public boolean isAdmin() {
        return isAdmin(this.getRoleId());
    }

    public static boolean isAdmin(Long roleId) {
        return roleId != null && 1L == roleId;
    }
}
