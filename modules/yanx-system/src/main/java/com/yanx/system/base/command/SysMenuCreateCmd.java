package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.system.base.infra.mapper.SysMenuMapper;
import com.yanx.system.base.model.dto.SysMenuDto;

/**
 * 菜单权限 新增命令
 *
 * @author: gotanks
 * @create: 2023-2-9
 */
public class SysMenuCreateCmd extends CrudCommand<SysMenuDto> {

    /**
     * 新增
     *
     * @param sysMenuDto
     */
    public SysMenuCreateCmd(SysMenuDto sysMenuDto) {
        super(sysMenuDto);
    }

    /**
     * 新增时实体类转换
     *
     * @return
     */
    @Override
    public BaseEntity toEntity() {
        return SysMenuMapper.INSTANCE.toSysMenu(this.getDto());
    }

}