package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.system.base.domain.entity.SysDictData;
import com.yanx.system.base.infra.mapper.SysDictDataMapper;
import com.yanx.system.base.model.dto.SysDictDataDto;

/**
 * 字典数据 修改命令
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
public class SysDictDataUpdateCmd extends CrudCommand<SysDictDataDto> {

    /**
     * 修改
     *
     * @param id
    * @param sysDictDataDto
     */
    public SysDictDataUpdateCmd(Long id, SysDictDataDto sysDictDataDto) {
        super(id, sysDictDataDto);
    }

    /**
     * 修改时实体类转换
     *
     * @param entity
     * @return
     */
    @Override
    public BaseEntity toEntity(BaseEntity entity) {
        return SysDictDataMapper.INSTANCE.toSysDictData(this.getDto(), (SysDictData) entity);
    }

}