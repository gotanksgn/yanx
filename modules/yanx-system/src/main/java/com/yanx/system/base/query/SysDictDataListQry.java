package com.yanx.system.base.query;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.yanx.common.command.Executor;
import com.yanx.common.core.command.PageQryCommand;
import com.yanx.system.base.domain.entity.QSysDictData;
import com.yanx.system.base.model.qo.SysDictDataQo;
import com.yanx.system.base.model.vo.SysDictDataVo;
import lombok.AllArgsConstructor;

import java.util.List;

/**
 * 字典数据分页查询
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
@AllArgsConstructor
public class SysDictDataListQry extends PageQryCommand<List<SysDictDataVo>> {

    private SysDictDataQo sysDictDataQo;

    @Override
    public List<SysDictDataVo> execute(Executor executor) {
        QSysDictData sysDictData = QSysDictData.sysDictData;

        //查询条件
        BooleanBuilder condition = new BooleanBuilder();
        condition.and(sysDictData.deleted.eq(false));
        if (sysDictDataQo != null) {
            //自定义查询条件
            if (sysDictDataQo.getDictLabel() != null && !sysDictDataQo.getDictLabel().equals("")) {
                condition.and(sysDictData.dictLabel.contains(sysDictDataQo.getDictLabel()));
            }
            if (sysDictDataQo.getDictType() != null && !sysDictDataQo.getDictType().equals("")) {
                condition.and(sysDictData.dictType.eq(sysDictDataQo.getDictType()));
            }
            if (sysDictDataQo.getStatus() != null && !sysDictDataQo.getStatus().equals("")) {
                condition.and(sysDictData.status.eq(sysDictDataQo.getStatus()));
            }
            if (sysDictDataQo.getBeginTime() != null) {
                condition.and(sysDictData.createTime.gt(sysDictDataQo.getBeginTime()));
            }
            if (sysDictDataQo.getEndTime() != null) {
                condition.and(sysDictData.createTime.lt(sysDictDataQo.getEndTime()));
            }
        }

        JPAQuery<SysDictDataVo> query = queryFactory.select(SysDictDataByIdQry.fields())
                .from(sysDictData)
                .where(condition)
                .orderBy(sysDictData.dictSort.asc());

        return this.pageList(query);
    }

}