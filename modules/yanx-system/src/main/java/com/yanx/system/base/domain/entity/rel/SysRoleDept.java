package com.yanx.system.base.domain.entity.rel;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 角色和部门关联 sys_role_dept
 *
 * @author gotanks
 */
@Data
@Entity
@Table(name = "SYS_ROLE_DEPT")
@IdClass(value = SysRoleDept.class)
public class SysRoleDept implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 角色ID
     */
    @Id
    @JoinColumn(foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    private Long roleId;

    /**
     * 部门ID
     */
    @Id
    @JoinColumn(foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    private Long deptId;
}
