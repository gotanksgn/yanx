package com.yanx.system.base.api;

import com.yanx.common.annotation.ApiResult;
import com.yanx.common.annotation.Pagination;
import com.yanx.common.core.api.BaseApi;
import com.yanx.system.base.command.SysUserCreateCmd;
import com.yanx.system.base.command.SysUserDeleteCmd;
import com.yanx.system.base.command.SysUserUpdateCmd;
import com.yanx.system.base.model.dto.SysUserDto;
import com.yanx.system.base.model.qo.SysUserQo;
import com.yanx.system.base.model.vo.SysUserVo;
import com.yanx.system.base.query.SysUserByIdQry;
import com.yanx.system.base.query.SysUserListQry;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

/**
 * 用户 接口
 *
 * @author: gotanks
 * @create: 2023-4-12
 */
@Tag(name = "用户", description = "用户")
@RestController
@RequestMapping(value = "/api/sys-user")
public class SysUserApi extends BaseApi {

    @ApiResult
    @Operation(summary = "根据ID查询用户")
    @GetMapping("/{id}")
    public SysUserVo get(@PathVariable Long id) {
        return queryExecutor.execute(new SysUserByIdQry(id));
    }

    @Pagination(total = true)
    @ApiResult
    @Operation(summary = "分页查询用户")
    @GetMapping
    public List<SysUserVo> getList(SysUserQo sysUserQo) {
        return queryExecutor.execute(new SysUserListQry(sysUserQo));
    }

    @ApiResult
    @Operation(summary = "新增用户")
    @PostMapping
    public void create(@Valid @RequestBody SysUserDto sysUserDto) {
        commandExecutor.execute(new SysUserCreateCmd(sysUserDto));
    }

    @ApiResult
    @Operation(summary = "更新用户")
    @PutMapping("/{id}")
    public void update(@PathVariable("id") Long id, @Valid @RequestBody SysUserDto sysUserDto) {
        commandExecutor.execute(new SysUserUpdateCmd(id, sysUserDto));
    }

    @ApiResult
    @Operation(summary = "删除用户")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        commandExecutor.execute(new SysUserDeleteCmd(id));
    }

    @ApiResult
    @Operation(summary = "批量删除用户")
    @DeleteMapping
    public void batchDelete(@RequestBody Set<Long> ids) {
        commandExecutor.execute(new SysUserDeleteCmd(ids));
    }
}