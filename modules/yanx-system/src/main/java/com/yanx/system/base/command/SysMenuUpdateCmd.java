package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.system.base.domain.entity.SysMenu;
import com.yanx.system.base.infra.mapper.SysMenuMapper;
import com.yanx.system.base.model.dto.SysMenuDto;

/**
 * 菜单权限 修改命令
 *
 * @author: gotanks
 * @create: 2023-2-9
 */
public class SysMenuUpdateCmd extends CrudCommand<SysMenuDto> {

    /**
     * 修改
     *
     * @param id
    * @param sysMenuDto
     */
    public SysMenuUpdateCmd(Long id, SysMenuDto sysMenuDto) {
        super(id, sysMenuDto);
    }

    /**
     * 修改时实体类转换
     *
     * @param entity
     * @return
     */
    @Override
    public BaseEntity toEntity(BaseEntity entity) {
        return SysMenuMapper.INSTANCE.toSysMenu(this.getDto(), (SysMenu) entity);
    }

}