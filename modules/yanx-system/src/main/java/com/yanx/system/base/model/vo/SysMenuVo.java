package com.yanx.system.base.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.yanx.common.core.vo.BaseTreeVo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 菜单权限VO
 *
 * @author: gotanks
 * @create: 2023-2-9
 */
@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "菜单权限视图模型")
public class SysMenuVo extends BaseTreeVo<Long> {

    private static final long serialVersionUID = 1L;

    //兼容ruoyi
    @JsonSerialize(using = ToStringSerializer.class)
    @Schema(name = "菜单ID")
    public Long getMenuId() {
        return this.getId();
    }

    @Schema(name = "菜单名称")
    private String menuName;

//    @Schema(name = "父菜单名称")
//    private String parentName;
//
//    @Schema(name = "父菜单ID")
//    private Long parentId;

    @Schema(name = "显示顺序")
    private Integer orderNum;

    @Schema(name = "路由地址")
    private String path;

    @Schema(name = "组件路径")
    private String component;

    @Schema(name = "路由参数")
    private String query;

    @Schema(name = "是否为外链")
    private String isFrame;

    @Schema(name = "是否缓存")
    private String isCache;

    @Schema(name = "类型")
    private String menuType;

    @Schema(name = "显示状态")
    private String visible;

    @Schema(name = "菜单状态")
    private String status;

    @Schema(name = "权限字符串")
    private String perms;

    @Schema(name = "菜单图标")
    private String icon;

}
