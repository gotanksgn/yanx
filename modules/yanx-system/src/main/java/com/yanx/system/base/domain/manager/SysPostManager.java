package com.yanx.system.base.domain.manager;

import com.yanx.common.core.domain.manager.BaseManager;
import com.yanx.system.base.domain.entity.SysPost;
import com.yanx.system.base.domain.repository.SysPostRepository;
import com.yanx.system.base.domain.repository.SysUserPostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 岗位领域服务
 *
 * @author: gotanks
 * @create: 2023-5-23
 */
@Service
public class SysPostManager extends BaseManager<SysPost, SysPostRepository> {
    @Autowired
    private SysUserPostRepository sysUserPostRepository;

    public boolean checkPostNameUnique(String postName, Long id) {
        int num = repository.countByPostNameAndIdNot(postName, id);
        return num > 0;
    }

    public boolean checkPostCodeUnique(String postCode, Long id) {
        int num = repository.countByPostCodeAndIdNot(postCode, id);
        return num > 0;
    }

    public boolean checkUserPostExist(Long postId) {
        int num = sysUserPostRepository.countByPostId(postId);
        return num > 0;
    }
}
