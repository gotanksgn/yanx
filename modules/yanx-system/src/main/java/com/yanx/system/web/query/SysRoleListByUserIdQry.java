package com.yanx.system.web.query;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.yanx.common.command.Executor;
import com.yanx.common.core.command.PageQryCommand;
import com.yanx.system.base.domain.entity.QSysRole;
import com.yanx.system.base.domain.entity.rel.QSysUserRole;
import com.yanx.system.base.model.vo.SysRoleVo;
import com.yanx.system.base.query.SysRoleByIdQry;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 根据用户查询角色集合
 *
 * @author: gotanks
 * @create: 2023-6-7
 */
@NoArgsConstructor
@AllArgsConstructor
public class SysRoleListByUserIdQry extends PageQryCommand<List<SysRoleVo>> {

    private Long userId;

    @Override
    public List<SysRoleVo> execute(Executor executor) {
        QSysRole sysRole = QSysRole.sysRole;
        QSysUserRole sysUserRole = QSysUserRole.sysUserRole;

        //查询条件
        BooleanBuilder condition = new BooleanBuilder();
        condition.and(sysRole.deleted.eq(false));
        JPAQuery<SysRoleVo> query = queryFactory.select(SysRoleByIdQry.fields())
                .from(sysRole)
                .leftJoin(sysUserRole).on(sysUserRole.roleId.eq(sysRole.id))
                .where(condition, sysUserRole.userId.eq(userId));

        return this.pageList(query);
    }

}