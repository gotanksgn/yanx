package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.system.base.infra.mapper.SysDeptMapper;
import com.yanx.system.base.model.dto.SysDeptDto;

/**
 * 部门 新增命令
 *
 * @author: gotanks
 * @create: 2023-2-8
 */
public class SysDeptCreateCmd extends CrudCommand<SysDeptDto> {

    /**
     * 新增
     *
     * @param sysDeptDto
     */
    public SysDeptCreateCmd(SysDeptDto sysDeptDto) {
        super(sysDeptDto);
    }

    /**
     * 新增时实体类转换
     *
     * @return
     */
    @Override
    public BaseEntity toEntity() {
        return SysDeptMapper.INSTANCE.toSysDept(this.getDto());
    }

}