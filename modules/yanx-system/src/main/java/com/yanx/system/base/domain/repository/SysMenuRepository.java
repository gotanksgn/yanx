package com.yanx.system.base.domain.repository;

import com.yanx.common.core.domain.repository.BaseRepository;
import com.yanx.system.base.domain.entity.SysMenu;
import org.springframework.data.jpa.repository.Query;

import java.io.Serializable;

/**
 * 菜单权限仓库
 *
 * @author: gotanks
 * @create: 2023-2-9
 */
public interface SysMenuRepository extends BaseRepository<SysMenu, Serializable> {
    int countByMenuNameAndParentId(String menuName, Long parentId);

    int countByMenuNameAndParentIdAndIdNot(String menuName, Long parentId, Long menuId);

    int countByParentId(Long menuId);

    @Query("select count(t) from SysRoleMenu t where t.menuId = ?1")
    int countRoleByMenuId(Long menuId);
}
