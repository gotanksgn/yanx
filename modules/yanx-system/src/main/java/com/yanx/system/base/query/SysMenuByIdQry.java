package com.yanx.system.base.query;

import com.querydsl.core.types.Projections;
import com.querydsl.core.types.QBean;
import com.yanx.common.command.Executor;
import com.yanx.common.core.command.QryCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.system.base.domain.entity.QSysMenu;
import com.yanx.system.base.model.vo.SysMenuVo;
import lombok.AllArgsConstructor;

/**
 * 根据ID查询菜单权限
 *
 * @author: gotanks
 * @create: 2023-4-12
 */
@AllArgsConstructor
public class SysMenuByIdQry extends QryCommand<SysMenuVo> {

    private Long id;

    @Override
    public SysMenuVo execute(Executor executor) {
        if (id == null) {
            throw new BusinessException("菜单权限ID不能为空");
        }
        QSysMenu sysMenu = QSysMenu.sysMenu;
        return queryFactory.select(this.fields())
                .from(sysMenu)
                .where(sysMenu.deleted.eq(false), sysMenu.id.eq(id))
                .fetchOne();
    }

    /**
     * 菜单权限VO映射
     *
     * @return QBean<SysMenuVo>
     */
    public static QBean<SysMenuVo> fields() {
        QSysMenu sysMenu = QSysMenu.sysMenu;
        return Projections.fields(
                SysMenuVo.class,
                sysMenu.menuName,
                sysMenu.parentName,
                sysMenu.parentId,
                sysMenu.orderNum,
                sysMenu.path,
                sysMenu.component,
                sysMenu.query,
                sysMenu.isFrame,
                sysMenu.isCache,
                sysMenu.menuType,
                sysMenu.visible,
                sysMenu.status,
                sysMenu.perms,
                sysMenu.icon,
                sysMenu.createTime,
                sysMenu.id
        );
    }
}