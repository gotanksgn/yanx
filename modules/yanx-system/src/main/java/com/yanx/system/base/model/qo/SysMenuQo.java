package com.yanx.system.base.model.qo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 * 菜单权限QO
 *
 * @author: gotanks
 * @create: 2023-2-9
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "菜单权限查询模型")
public class SysMenuQo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Schema(name = "菜单名称")
    private String menuName;

    @Schema(name = "显示状态")
    private String visible;

    @Schema(name = "菜单状态")
    private String status;
}
