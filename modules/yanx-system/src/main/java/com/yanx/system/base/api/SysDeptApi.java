package com.yanx.system.base.api;

import com.yanx.common.annotation.ApiResult;
import com.yanx.common.annotation.Pagination;
import com.yanx.common.core.api.BaseApi;
import com.yanx.system.base.command.SysDeptCreateCmd;
import com.yanx.system.base.command.SysDeptDeleteCmd;
import com.yanx.system.base.command.SysDeptUpdateCmd;
import com.yanx.system.base.model.dto.SysDeptDto;
import com.yanx.system.base.model.qo.SysDeptQo;
import com.yanx.system.base.model.vo.SysDeptVo;
import com.yanx.system.base.query.SysDeptByIdQry;
import com.yanx.system.base.query.SysDeptListQry;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

/**
 * 部门 接口
 *
 * @author: gotanks
 * @create: 2023-4-12
 */
@Tag(name = "部门", description = "部门")
@RestController
@RequestMapping(value = "/api/sys-dept")
public class SysDeptApi extends BaseApi {

    @ApiResult
    @Operation(summary = "根据ID查询部门")
    @GetMapping("/{id}")
    public SysDeptVo get(@PathVariable Long id) {
        return queryExecutor.execute(new SysDeptByIdQry(id));
    }

    @Pagination(total = true)
    @ApiResult
    @Operation(summary = "分页查询部门")
    @GetMapping
    public List<SysDeptVo> getList(SysDeptQo sysDeptQo) {
        return queryExecutor.execute(new SysDeptListQry(sysDeptQo));
    }

    @ApiResult
    @Operation(summary = "新增部门")
    @PostMapping
    public void create(@Valid @RequestBody SysDeptDto sysDeptDto) {
        commandExecutor.execute(new SysDeptCreateCmd(sysDeptDto));
    }

    @ApiResult
    @Operation(summary = "更新部门")
    @PutMapping("/{id}")
    public void update(@PathVariable("id") Long id, @Valid @RequestBody SysDeptDto sysDeptDto) {
        commandExecutor.execute(new SysDeptUpdateCmd(id, sysDeptDto));
    }

    @ApiResult
    @Operation(summary = "删除部门")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        commandExecutor.execute(new SysDeptDeleteCmd(id));
    }

    @ApiResult
    @Operation(summary = "批量删除部门")
    @DeleteMapping
    public void batchDelete(@RequestBody Set<Long> ids) {
        commandExecutor.execute(new SysDeptDeleteCmd(ids));
    }
}