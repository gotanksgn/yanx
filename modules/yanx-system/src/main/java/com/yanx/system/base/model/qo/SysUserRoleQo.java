package com.yanx.system.base.model.qo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户QO
 *
 * @author: gotanks
 * @create: 2023-2-6
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "用户查询模型")
public class SysUserRoleQo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(name = "角色ID")
    private Long roleId;

    @Schema(name = "用户账号")
    private String userName;

    @Schema(name = "手机号码")
    private String phonenumber;
}
