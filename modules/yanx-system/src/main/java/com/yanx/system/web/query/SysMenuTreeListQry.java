package com.yanx.system.web.query;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.QryCommand;
import com.yanx.system.base.model.qo.SysMenuQo;
import com.yanx.system.base.model.vo.SysMenuVo;
import com.yanx.system.web.model.vo.SysMenuTreeVo;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 菜单权限树查询
 *
 * @author: gotanks
 * @create: 2023-2-9
 */
@NoArgsConstructor
@AllArgsConstructor
public class SysMenuTreeListQry extends QryCommand<List<SysMenuTreeVo>> {

    private SysMenuQo sysMenuQo;

    @Override
    public List<SysMenuTreeVo> execute(Executor executor) {
        List<SysMenuVo> sysMenuVos = executor.execute(new SysMenuListByUserIdQry(sysMenuQo));

        List<SysMenuVo> returnList = (List<SysMenuVo>) SysMenuTreeByUserIdQry.getChildPerms(sysMenuVos);
//        List<Long> tempList = sysMenuVos.stream().map(SysMenuVo::getId).collect(Collectors.toList());
//        for (SysMenuVo menu : sysMenuVos) {
//            // 如果是顶级节点, 遍历该父节点的所有子节点
//            if (!tempList.contains(menu.getParentId())) {
//                recursionFn(sysMenuVos, menu);
//                returnList.add(menu);
//            }
//        }
        if (returnList.isEmpty()) {
            returnList = sysMenuVos;
        }
        return returnList.stream().map(SysMenuTreeVo::new).collect(Collectors.toList());

    }

    /**
     * 递归列表
     *
     * @param list 分类表
     * @param t    子节点
     */
    private void recursionFn(List<SysMenuVo> list, SysMenuVo t) {
        // 得到子节点列表
        List<SysMenuVo> childList = getChildList(list, t);
        t.setChildren(childList);
        for (SysMenuVo tChild : childList) {
            if (hasChild(list, tChild)) {
                recursionFn(list, tChild);
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<SysMenuVo> getChildList(List<SysMenuVo> list, SysMenuVo t) {
        return list.stream()
                .filter(m -> m.getParentId().longValue() == t.getId().longValue())
                .collect(Collectors.toList());
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<SysMenuVo> list, SysMenuVo t) {
        return getChildList(list, t).size() > 0;
    }
}