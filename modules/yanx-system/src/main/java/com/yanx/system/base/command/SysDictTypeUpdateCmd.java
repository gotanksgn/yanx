package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.system.base.domain.entity.SysDictType;
import com.yanx.system.base.infra.mapper.SysDictTypeMapper;
import com.yanx.system.base.model.dto.SysDictTypeDto;

/**
 * 字典类型 修改命令
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
public class SysDictTypeUpdateCmd extends CrudCommand<SysDictTypeDto> {

    /**
     * 修改
     *
     * @param id
    * @param sysDictTypeDto
     */
    public SysDictTypeUpdateCmd(Long id, SysDictTypeDto sysDictTypeDto) {
        super(id, sysDictTypeDto);
    }

    /**
     * 修改时实体类转换
     *
     * @param entity
     * @return
     */
    @Override
    public BaseEntity toEntity(BaseEntity entity) {
        return SysDictTypeMapper.INSTANCE.toSysDictType(this.getDto(), (SysDictType) entity);
    }

}