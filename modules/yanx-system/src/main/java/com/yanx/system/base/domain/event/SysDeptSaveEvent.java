package com.yanx.system.base.domain.event;

import com.yanx.system.base.domain.entity.SysDept;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SysDeptSaveEvent {

    private SysDept sysDept;

}
