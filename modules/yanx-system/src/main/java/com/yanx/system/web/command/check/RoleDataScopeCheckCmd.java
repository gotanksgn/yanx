package com.yanx.system.web.command.check;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.VoidCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.common.utils.Collect_;
import com.yanx.system.base.model.qo.SysRoleQo;
import com.yanx.system.base.model.vo.SysRoleVo;
import com.yanx.system.base.query.SysRoleListQry;
import lombok.AllArgsConstructor;

import java.util.List;

/**
 * 校验角色是否允许修改
 *
 * @author: gotanks
 * @create: 2023-6-7
 */
@AllArgsConstructor
public class RoleDataScopeCheckCmd extends VoidCommand {

    private Long roleId;

    @Override
    public void handler(Executor executor) {
        SysRoleQo sysRoleQo = new SysRoleQo();
        sysRoleQo.setRoleId(roleId);
        List<SysRoleVo> roleVos = executor.execute(new SysRoleListQry(sysRoleQo));
        if (Collect_.isEmpty(roleVos)) {
            throw new BusinessException("没有权限访问角色数据！");
        }
    }
}