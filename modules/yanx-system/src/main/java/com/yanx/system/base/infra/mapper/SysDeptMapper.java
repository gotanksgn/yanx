package com.yanx.system.base.infra.mapper;

import com.yanx.system.base.domain.entity.SysDept;
import com.yanx.system.base.model.dto.SysDeptDto;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
 * 部门 实体转换类
 *
 * @author: gotanks
 * @create: 2023-2-8
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface SysDeptMapper {

    SysDeptMapper INSTANCE = Mappers.getMapper(SysDeptMapper.class);

    SysDept toSysDept(SysDeptDto dto);

    SysDept toSysDept(SysDeptDto dto, @MappingTarget SysDept entity);

}
