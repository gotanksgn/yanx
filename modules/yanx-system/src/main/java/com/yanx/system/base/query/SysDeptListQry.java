package com.yanx.system.base.query;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.yanx.common.command.Executor;
import com.yanx.common.core.command.PageQryCommand;
import com.yanx.system.base.domain.entity.QSysDept;
import com.yanx.system.base.model.qo.SysDeptQo;
import com.yanx.system.base.model.vo.SysDeptVo;
import com.yanx.system.web.infra.utils.DataScopeUtil;
import lombok.AllArgsConstructor;

import java.util.List;

/**
 * 部门分页查询
 *
 * @author: gotanks
 * @create: 2023-4-12
 */
@AllArgsConstructor
public class SysDeptListQry extends PageQryCommand<List<SysDeptVo>> {

    private SysDeptQo sysDeptQo;

    @Override
    public List<SysDeptVo> execute(Executor executor) {
        QSysDept sysDept = QSysDept.sysDept;

        //查询条件
        BooleanBuilder condition = DataScopeUtil.getDeptScopeCondition();//数据范围权限
        condition.and(sysDept.deleted.eq(false));
        if (sysDeptQo != null) {
            //自定义查询条件
            if (sysDeptQo.getDeptId() != null && sysDeptQo.getDeptId() != 0) {
                condition.and(sysDept.id.eq(sysDeptQo.getDeptId()));
            }
            if (sysDeptQo.getParentId() != null && sysDeptQo.getParentId() != 0) {
                condition.and(sysDept.parentId.eq(sysDeptQo.getParentId()));
            }
            if (sysDeptQo.getDeptName() != null && !sysDeptQo.getDeptName().equals("")) {
                condition.and(sysDept.deptName.contains(sysDeptQo.getDeptName()));
            }
            if (sysDeptQo.getStatus() != null && !sysDeptQo.getStatus().equals("")) {
                condition.and(sysDept.status.eq(sysDeptQo.getStatus()));
            }
        }

        JPAQuery<SysDeptVo> query = queryFactory.select(SysDeptByIdQry.fields())
                .from(sysDept)
                .where(condition);

        return this.pageList(query);
    }

}