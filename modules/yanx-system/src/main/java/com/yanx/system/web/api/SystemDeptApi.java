package com.yanx.system.web.api;

import com.yanx.common.annotation.ApiResult;
import com.yanx.common.annotation.OperationLog;
import com.yanx.common.core.api.BaseApi;
import com.yanx.common.enums.BusinessType;
import com.yanx.common.security.preauthorize.PreAuthorize;
import com.yanx.common.utils.String_;
import com.yanx.system.base.model.dto.SysDeptDto;
import com.yanx.system.base.model.qo.SysDeptQo;
import com.yanx.system.base.model.vo.SysDeptVo;
import com.yanx.system.base.query.SysDeptByIdQry;
import com.yanx.system.base.query.SysDeptListQry;
import com.yanx.system.web.command.SystemDeptAddCmd;
import com.yanx.system.web.command.SystemDeptEditCmd;
import com.yanx.system.web.command.SystemDeptRemoveCmd;
import com.yanx.system.web.command.check.DeptDataScopeCheckCmd;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 系统部门管理 接口
 *
 * @author: gotanks
 * @create: 2023-4-18
 */
@Tag(name = "部门管理", description = "部门管理")
@RestController
@RequestMapping(value = "/system/dept")
public class SystemDeptApi extends BaseApi {

    @PreAuthorize(hasPermi = "system:dept:list")
    @ApiResult
    @Operation(summary = "获取部门列表")
    @GetMapping("/list")
    public List<SysDeptVo> list(SysDeptQo sysDeptQo) {
        return queryExecutor.execute(new SysDeptListQry(sysDeptQo));
    }

    /**
     * 查询部门列表（排除节点）
     */
    @PreAuthorize(hasPermi = "system:dept:list")
    @ApiResult
    @Operation(summary = "查询部门列表（排除节点）")
    @GetMapping("/list/exclude/{deptId}")
    public List<SysDeptVo> excludeChild(@PathVariable(value = "deptId", required = false) Long deptId) {
        List<SysDeptVo> sysDeptVos = queryExecutor.execute(new SysDeptListQry(null));
        sysDeptVos.removeIf(d -> d.getId().intValue() == deptId || String_.split(d.getAncestors(), ",").contains(deptId + ""));
        return sysDeptVos;
    }

    /**
     * 根据部门编号获取详细信息
     */
    @PreAuthorize(hasPermi = "system:dept:query")
    @ApiResult
    @Operation(summary = "根据部门编号获取详细信息")
    @GetMapping(value = "/{deptId}")
    public SysDeptVo getInfo(@PathVariable Long deptId) {
        commandExecutor.execute(new DeptDataScopeCheckCmd(deptId));
//        deptService.checkDeptDataScope(deptId);
        return queryExecutor.execute(new SysDeptByIdQry(deptId));
    }

    /**
     * 新增部门
     */
    @PreAuthorize(hasPermi = "system:dept:add")
    @OperationLog(title = "部门管理", businessType = BusinessType.INSERT)
    @ApiResult
    @Operation(summary = "新增部门")
    @PostMapping
    public void add(@Validated @RequestBody SysDeptDto dept) {
        commandExecutor.execute(new SystemDeptAddCmd(dept));
    }

    /**
     * 修改部门
     */
    @PreAuthorize(hasPermi = "system:dept:edit")
    @OperationLog(title = "部门管理", businessType = BusinessType.UPDATE)
    @ApiResult
    @Operation(summary = "修改部门")
    @PutMapping
    public void edit(@Validated @RequestBody SysDeptDto dept) {
        commandExecutor.execute(new SystemDeptEditCmd(dept));
    }

    /**
     * 删除部门
     */
    @PreAuthorize(hasPermi = "system:dept:remove")
    @OperationLog(title = "部门管理", businessType = BusinessType.DELETE)
    @ApiResult
    @Operation(summary = "删除部门")
    @DeleteMapping("/{deptId}")
    public void remove(@PathVariable Long deptId) {
        commandExecutor.execute(new SystemDeptRemoveCmd(deptId));
    }
}