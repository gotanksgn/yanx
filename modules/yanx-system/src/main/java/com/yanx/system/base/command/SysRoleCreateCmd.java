package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.system.base.infra.mapper.SysRoleMapper;
import com.yanx.system.base.model.dto.SysRoleDto;

/**
 * 菜单权限 新增命令
 *
 * @author: gotanks
 * @create: 2023-2-10
 */
public class SysRoleCreateCmd extends CrudCommand<SysRoleDto> {

    /**
     * 新增
     *
     * @param sysRoleDto
     */
    public SysRoleCreateCmd(SysRoleDto sysRoleDto) {
        super(sysRoleDto);
    }

    /**
     * 新增时实体类转换
     *
     * @return
     */
    @Override
    public BaseEntity toEntity() {
        return SysRoleMapper.INSTANCE.toSysRole(this.getDto());
    }

}