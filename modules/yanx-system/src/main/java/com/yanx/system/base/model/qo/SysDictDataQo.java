package com.yanx.system.base.model.qo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yanx.common.enums.DataStatus;
import com.yanx.common.utils.DateTime_;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * 字典数据QO
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "字典数据查询模型")
public class SysDictDataQo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(name = "字典标签")
    private String dictLabel;

    @Schema(name = "字典类型")
    private String dictType;

    @Schema(name = "状态")
    private DataStatus status;

    @Schema(name = "请求参数")
    private Map<String, Object> params = new HashMap<>();

    public LocalDateTime getBeginTime() {
        Object beginTime = params.get("beginTime");
        if (beginTime != null) {
            return DateTime_.parseDateTime(beginTime.toString());
        }
        return null;
    }

    public LocalDateTime getEndTime() {
        Object endTime = params.get("endTime");
        if (endTime != null) {
            return DateTime_.parseDateTime(endTime.toString());
        }
        return null;
    }
}
