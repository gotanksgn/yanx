package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.system.base.model.dto.SysDeptDto;

import java.util.Collection;

/**
 * 部门 删除命令
 *
 * @author: gotanks
 * @create: 2023-2-8
 */
public class SysDeptDeleteCmd extends CrudCommand<SysDeptDto> {

    /**
     * 删除
     *
     * @param id
     */
    public SysDeptDeleteCmd(Long id) {
        super(id);
    }

    /**
     * 批量删除
     *
     * @param ids
     */
    public SysDeptDeleteCmd(Collection<Long> ids) {
        super(ids);
    }

}