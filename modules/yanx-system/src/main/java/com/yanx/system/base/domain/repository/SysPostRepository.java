package com.yanx.system.base.domain.repository;

import com.yanx.common.core.domain.repository.BaseRepository;
import com.yanx.system.base.domain.entity.SysPost;

import java.io.Serializable;

/**
 * 岗位仓库
 *
 * @author: gotanks
 * @create: 2023-5-23
 */
public interface SysPostRepository extends BaseRepository<SysPost, Serializable> {

    int countByPostNameAndIdNot(String postName, Long id);

    int countByPostCodeAndIdNot(String postCode, Long id);
}
