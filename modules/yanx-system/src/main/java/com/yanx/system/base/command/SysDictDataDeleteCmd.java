package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.system.base.domain.entity.SysDictData;
import com.yanx.system.base.infra.mapper.SysDictDataMapper;
import com.yanx.system.base.model.dto.SysDictDataDto;

import java.util.Collection;

/**
 * 字典数据 删除命令
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
public class SysDictDataDeleteCmd extends CrudCommand<SysDictDataDto> {

    /**
     * 删除
     *
     * @param id
     */
    public SysDictDataDeleteCmd(Long id) {
        super(id);
    }

    /**
     * 批量删除
     *
     * @param ids
     */
    public SysDictDataDeleteCmd(Collection<Long> ids) {
        super(ids);
    }

}