package com.yanx.system.base.domain.repository;

import com.yanx.system.base.domain.entity.rel.SysUserPost;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;

/**
 * 用户和岗位关系仓库
 *
 * @author: gotanks
 * @create: 2023-5-23
 */
public interface SysUserPostRepository extends JpaRepository<SysUserPost, Serializable> {

    int countByPostId(Long postId);
}
