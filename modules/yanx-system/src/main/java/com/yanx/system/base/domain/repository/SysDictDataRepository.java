package com.yanx.system.base.domain.repository;

import com.yanx.common.core.domain.repository.BaseRepository;
import com.yanx.system.base.domain.entity.SysDictData;

import java.io.Serializable;

/**
 * 字典数据仓库
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
public interface SysDictDataRepository extends BaseRepository<SysDictData, Serializable> {

}
