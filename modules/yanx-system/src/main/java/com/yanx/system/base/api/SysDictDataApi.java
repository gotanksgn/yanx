package com.yanx.system.base.api;

import com.yanx.common.annotation.ApiResult;
import com.yanx.common.annotation.Pagination;
import com.yanx.common.core.api.BaseApi;
import com.yanx.system.base.command.SysDictDataCreateCmd;
import com.yanx.system.base.command.SysDictDataDeleteCmd;
import com.yanx.system.base.command.SysDictDataUpdateCmd;
import com.yanx.system.base.model.dto.SysDictDataDto;
import com.yanx.system.base.model.qo.SysDictDataQo;
import com.yanx.system.base.model.vo.SysDictDataVo;
import com.yanx.system.base.query.SysDictDataByIdQry;
import com.yanx.system.base.query.SysDictDataListQry;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

/**
 * 字典数据 接口
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
@Tag(name = "字典数据", description = "字典数据")
@RestController
@RequestMapping(value = "/api/sys-dict-data")
public class SysDictDataApi extends BaseApi {

    @ApiResult
    @Operation(summary = "根据ID查询字典数据")
    @GetMapping("/{id}")
    public SysDictDataVo get(@PathVariable Long id) {
        return queryExecutor.execute(new SysDictDataByIdQry(id));
    }

    @Pagination(total = true)
    @ApiResult
    @Operation(summary = "分页查询字典数据")
    @GetMapping
    public List<SysDictDataVo> getList(SysDictDataQo sysDictDataQo) {
        return queryExecutor.execute(new SysDictDataListQry(sysDictDataQo));
    }

    @ApiResult
    @Operation(summary = "新增字典数据")
    @PostMapping
    public void create(@Valid @RequestBody SysDictDataDto sysDictDataDto) {
        commandExecutor.execute(new SysDictDataCreateCmd(sysDictDataDto));
    }

    @ApiResult
    @Operation(summary = "更新字典数据")
    @PutMapping("/{id}")
    public void update(@PathVariable("id") Long id, @Valid @RequestBody SysDictDataDto sysDictDataDto) {
        commandExecutor.execute(new SysDictDataUpdateCmd(id, sysDictDataDto));
    }

    @ApiResult
    @Operation(summary = "删除字典数据")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        commandExecutor.execute(new SysDictDataDeleteCmd(id));
    }

    @ApiResult
    @Operation(summary = "批量删除字典数据")
    @DeleteMapping
    public void batchDelete(@RequestBody Set<Long> ids) {
        commandExecutor.execute(new SysDictDataDeleteCmd(ids));
    }
}