package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.system.base.infra.mapper.SysConfigMapper;
import com.yanx.system.base.model.dto.SysConfigDto;

/**
 * 参数配置 新增命令
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
public class SysConfigCreateCmd extends CrudCommand<SysConfigDto> {

    /**
     * 新增
     *
     * @param sysConfigDto
     */
    public SysConfigCreateCmd(SysConfigDto sysConfigDto) {
        super(sysConfigDto);
    }

    /**
     * 新增时实体类转换
     *
     * @return
     */
    @Override
    public BaseEntity toEntity() {
        return SysConfigMapper.INSTANCE.toSysConfig(this.getDto());
    }

}