package com.yanx.system.web.query;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.PageQryCommand;
import com.yanx.common.enums.DataStatus;
import com.yanx.system.base.model.qo.SysDictDataQo;
import com.yanx.system.base.model.vo.SysDictDataVo;
import com.yanx.system.base.query.SysDictDataListQry;
import lombok.AllArgsConstructor;

import java.util.List;

/**
 * 根据字典类型查询字典数据
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
@AllArgsConstructor
public class SysDictDataListByTypeQry extends PageQryCommand<List<SysDictDataVo>> {

    private String dictType;

    @Override
    public List<SysDictDataVo> execute(Executor executor) {
        SysDictDataQo sysDictDataQo = new SysDictDataQo();
        sysDictDataQo.setStatus(DataStatus.ENABLE);
        sysDictDataQo.setDictType(dictType);
        return executor.execute(new SysDictDataListQry(sysDictDataQo));
    }

}