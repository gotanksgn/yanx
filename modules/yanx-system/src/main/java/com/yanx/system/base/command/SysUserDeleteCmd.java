package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.system.base.model.dto.SysUserDto;

import java.util.Collection;

/**
 * 用户 删除命令
 *
 * @author: gotanks
 * @create: 2023-2-6
 */
public class SysUserDeleteCmd extends CrudCommand<SysUserDto> {

    /**
     * 删除
     *
     * @param id
     */
    public SysUserDeleteCmd(Long id) {
        super(id);
    }

    /**
     * 批量删除
     *
     * @param ids
     */
    public SysUserDeleteCmd(Collection<Long> ids) {
        super(ids);
    }

}