package com.yanx.system.base.domain.event;

import com.yanx.system.base.domain.entity.SysConfig;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SysConfigSaveEvent {

    private SysConfig sysConfig;

}
