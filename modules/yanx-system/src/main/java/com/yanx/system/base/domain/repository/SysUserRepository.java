package com.yanx.system.base.domain.repository;

import com.yanx.common.core.domain.repository.BaseRepository;
import com.yanx.system.base.domain.entity.SysUser;

import java.io.Serializable;

/**
 * 用户仓库
 *
 * @author: gotanks
 * @create: 2023-2-6
 */
public interface SysUserRepository extends BaseRepository<SysUser, Serializable> {

    SysUser findByUserName(String username);

    int countByUserNameAndIdNot(String userName, Long id);

    int countByPhonenumberAndIdNot(String phonenumber, Long id);

    int countByEmailAndIdNot(String email, Long id);
}
