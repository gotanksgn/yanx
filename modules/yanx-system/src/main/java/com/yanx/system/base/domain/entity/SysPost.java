package com.yanx.system.base.domain.entity;

import com.yanx.codegen.annotation.CodeGenClass;
import com.yanx.codegen.annotation.CodeGenField;
import com.yanx.common.core.domain.entity.BaseTimeEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 岗位表 sys_post
 *
 * @author gotanks
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "SYS_POST")
@CodeGenClass(value = "岗位", author = "gotanks")
public class SysPost extends BaseTimeEntity {
    private static final long serialVersionUID = 1L;

    @CodeGenField("岗位编码")
    private String postCode;

    @CodeGenField("岗位名称")
    private String postName;

    @CodeGenField("岗位排序")
    private Integer postSort;

    @CodeGenField("状态")
    private String status;

}
