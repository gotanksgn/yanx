package com.yanx.system.base.infra.mapper;

import com.yanx.system.base.model.dto.SysDictDataDto;
import com.yanx.system.base.domain.entity.SysDictData;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
 * 字典数据 实体转换类
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface SysDictDataMapper {

    SysDictDataMapper INSTANCE = Mappers.getMapper(SysDictDataMapper.class);

    SysDictData toSysDictData(SysDictDataDto dto);

    SysDictData toSysDictData(SysDictDataDto dto, @MappingTarget SysDictData entity);

}
