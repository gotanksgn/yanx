package com.yanx.system.web.command;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.VoidCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.common.security.SecurityUtils;
import com.yanx.system.base.command.SysUserDeleteCmd;
import lombok.AllArgsConstructor;

import java.util.Set;

/**
 * 用户信息 删除
 *
 * @author: gotanks
 * @create: 2023-5-23
 */
@AllArgsConstructor
public class SystemUserRemoveCmd extends VoidCommand {
    private Set<Long> userIds;

    @Override
    public void handler(Executor executor) {
        if (userIds.contains(SecurityUtils.getLoginUser().getUserId())) {
            throw new BusinessException("当前用户不能删除");
        }
        executor.execute(new SysUserDeleteCmd(userIds));
    }
}