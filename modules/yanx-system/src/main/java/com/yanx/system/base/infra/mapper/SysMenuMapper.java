package com.yanx.system.base.infra.mapper;

import com.yanx.system.base.domain.entity.SysMenu;
import com.yanx.system.base.model.dto.SysMenuDto;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
 * 菜单权限 实体转换类
 *
 * @author: gotanks
 * @create: 2023-2-9
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface SysMenuMapper {

    SysMenuMapper INSTANCE = Mappers.getMapper(SysMenuMapper.class);

    SysMenu toSysMenu(SysMenuDto dto);

    SysMenu toSysMenu(SysMenuDto dto, @MappingTarget SysMenu entity);

}
