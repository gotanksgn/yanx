package com.yanx.system.web.command.check;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.VoidCommand;
import com.yanx.common.exception.BusinessException;
import lombok.AllArgsConstructor;

/**
 * 校验角色是否允许修改
 *
 * @author: gotanks
 * @create: 2023-6-7
 */
@AllArgsConstructor
public class UserAllowedCheckCmd extends VoidCommand {

    private Long userId;

    @Override
    public void handler(Executor executor) {
        if (userId != null && userId == 1) {
            throw new BusinessException("不允许操作超级管理员用户");
        }
    }
}