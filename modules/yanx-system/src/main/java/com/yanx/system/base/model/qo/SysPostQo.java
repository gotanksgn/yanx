package com.yanx.system.base.model.qo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 * 岗位QO
 *
 * @author: gotanks
 * @create: 2023-5-23
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "岗位查询模型")
public class SysPostQo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(name = "岗位编码")
    private String postCode;

    @Schema(name = "岗位名称")
    private String postName;

    @Schema(name = "状态")
    private String status;
}
