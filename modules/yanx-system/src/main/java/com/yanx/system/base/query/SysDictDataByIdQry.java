package com.yanx.system.base.query;

import com.querydsl.core.types.Projections;
import com.querydsl.core.types.QBean;
import com.yanx.common.command.Executor;
import com.yanx.common.core.command.QryCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.system.base.domain.entity.QSysDictData;
import com.yanx.system.base.model.vo.SysDictDataVo;
import lombok.AllArgsConstructor;

/**
 * 根据ID查询字典数据
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
@AllArgsConstructor
public class SysDictDataByIdQry extends QryCommand<SysDictDataVo> {

    private Long id;

    @Override
    public SysDictDataVo execute(Executor executor) {
        if (id == null) {
            throw new BusinessException("字典数据ID不能为空");
        }
        QSysDictData sysDictData = QSysDictData.sysDictData;
        return queryFactory.select(this.fields())
                .from(sysDictData)
                .where(sysDictData.deleted.eq(false), sysDictData.id.eq(id))
                .fetchOne();
    }

    /**
     * 字典数据VO映射
     *
     * @return QBean<SysDictDataVo>
     */
    public static QBean<SysDictDataVo> fields() {
        QSysDictData sysDictData = QSysDictData.sysDictData;
        return Projections.fields(
            SysDictDataVo.class,
            sysDictData.dictSort,
            sysDictData.dictLabel,
            sysDictData.dictValue,
            sysDictData.dictType,
            sysDictData.isDefault,
            sysDictData.status,
            sysDictData.id
        );
    }
}