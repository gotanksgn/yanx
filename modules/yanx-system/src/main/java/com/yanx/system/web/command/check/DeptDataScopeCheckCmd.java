package com.yanx.system.web.command.check;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.VoidCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.common.utils.Collect_;
import com.yanx.system.base.model.qo.SysDeptQo;
import com.yanx.system.base.model.vo.SysDeptVo;
import com.yanx.system.base.query.SysDeptListQry;
import lombok.AllArgsConstructor;

import java.util.List;

/**
 * 校验角色是否允许修改
 *
 * @author: gotanks
 * @create: 2023-6-7
 */
@AllArgsConstructor
public class DeptDataScopeCheckCmd extends VoidCommand {

    private Long deptId;

    @Override
    public void handler(Executor executor) {
        SysDeptQo sysDeptQo = new SysDeptQo();
        sysDeptQo.setDeptId(deptId);
        List<SysDeptVo> sysDeptVos = executor.execute(new SysDeptListQry(sysDeptQo));
        if (Collect_.isEmpty(sysDeptVos)) {
            throw new BusinessException("没有权限访问部门数据！");
        }
    }
}