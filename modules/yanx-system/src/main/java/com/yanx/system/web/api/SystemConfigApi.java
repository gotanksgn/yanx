package com.yanx.system.web.api;

import com.yanx.common.annotation.ApiResult;
import com.yanx.common.annotation.OperationLog;
import com.yanx.common.annotation.Pagination;
import com.yanx.common.core.api.BaseApi;
import com.yanx.common.enums.BusinessType;
import com.yanx.common.security.preauthorize.PreAuthorize;
import com.yanx.system.base.model.dto.SysConfigDto;
import com.yanx.system.base.model.qo.SysConfigQo;
import com.yanx.system.base.model.vo.SysConfigVo;
import com.yanx.system.base.query.SysConfigByIdQry;
import com.yanx.system.base.query.SysConfigListQry;
import com.yanx.system.web.command.SystemConfigAddCmd;
import com.yanx.system.web.command.SystemConfigEditCmd;
import com.yanx.system.web.command.SystemConfigRefreshCmd;
import com.yanx.system.web.command.SystemConfigRemoveCmd;
import com.yanx.system.web.query.SysConfigByKeyQry;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 参数配置管理 接口
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@Tag(name = "参数配置", description = "参数配置")
@RestController
@RequestMapping(value = "/system/config")
public class SystemConfigApi extends BaseApi {

    /**
     * 获取参数配置列表
     */
    @PreAuthorize(hasPermi = "system:config:list")
    @Pagination(total = true)
    @ApiResult
    @Operation(summary = "获取参数配置列表")
    @GetMapping("/list")
    public List<SysConfigVo> list(SysConfigQo config) {
        return queryExecutor.execute(new SysConfigListQry(config));
    }

//    @PreAuthorize(hasPermi = "system:config:export")
//    @ApiResult
//    @Operation(summary = "获取参数配置列表")
//    @OperationLog(title = "参数管理", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, SysConfig config) {
//        List<SysConfig> list = configService.selectConfigList(config);
//        ExcelUtil<SysConfig> util = new ExcelUtil<SysConfig>(SysConfig.class);
//        util.exportExcel(response, list, "参数数据");
//    }

    /**
     * 根据参数编号获取详细信息
     */
    @PreAuthorize(hasPermi = "system:config:query")
    @ApiResult
    @Operation(summary = "根据参数编号获取详细信息")
    @GetMapping(value = "/{configId}")
    public SysConfigVo getInfo(@PathVariable Long configId) {
        return queryExecutor.execute(new SysConfigByIdQry(configId));
    }

    /**
     * 根据参数键名查询参数值
     */
    @PreAuthorize(hasPermi = "system:config:query")
    @ApiResult
    @Operation(summary = "根据参数键名查询参数值")
    @GetMapping(value = "/configKey/{configKey}")
    public String getConfigKey(@PathVariable String configKey) {
        return queryExecutor.execute(new SysConfigByKeyQry(configKey));
    }

    /**
     * 新增参数配置
     */
    @PreAuthorize(hasPermi = "system:config:add")
    @ApiResult
    @Operation(summary = "新增参数配置")
    @OperationLog(title = "参数管理", businessType = BusinessType.INSERT)
    @PostMapping
    public void add(@Validated @RequestBody SysConfigDto config) {
        commandExecutor.execute(new SystemConfigAddCmd(config));
    }

    /**
     * 修改参数配置
     */
    @PreAuthorize(hasPermi = "system:config:edit")
    @ApiResult
    @Operation(summary = "修改参数配置")
    @OperationLog(title = "参数管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public void edit(@Validated @RequestBody SysConfigDto config) {
        commandExecutor.execute(new SystemConfigEditCmd(config));
    }

    /**
     * 删除参数配置
     */
    @PreAuthorize(hasPermi = "system:config:remove")
    @ApiResult
    @Operation(summary = "删除参数配置")
    @OperationLog(title = "参数管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{configIds}")
    public void remove(@PathVariable Long[] configIds) {
        commandExecutor.execute(new SystemConfigRemoveCmd(configIds));
    }

    /**
     * 刷新参数缓存
     */
    @PreAuthorize(hasPermi = "system:config:remove")
    @ApiResult
    @Operation(summary = "刷新参数缓存")
    @OperationLog(title = "参数管理", businessType = BusinessType.CLEAN)
    @DeleteMapping("/refreshCache")
    public void refreshCache() {
        commandExecutor.execute(new SystemConfigRefreshCmd());
    }
}