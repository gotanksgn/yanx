package com.yanx.system.web.query;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.QryCommand;
import com.yanx.system.base.domain.entity.QSysRole;
import com.yanx.system.base.domain.entity.rel.QSysUserRole;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 根据用户ID获取角色
 *
 * @author gotanks
 * @date 2021-04-20 09:49:00
 */
@Slf4j
@AllArgsConstructor
public class RoleKeysByUserIdQry extends QryCommand<Set<String>> {

    private Long userId;

    @Override
    public Set<String> execute(Executor executor) {
        QSysUserRole sysUserRole = QSysUserRole.sysUserRole;
        QSysRole sysRole = QSysRole.sysRole;
        List<String> roleKeys = queryFactory
                .select(sysRole.roleKey)
                .from(sysUserRole)
                .leftJoin(sysRole).on(sysRole.id.eq(sysUserRole.roleId))
                .where(sysUserRole.userId.eq(userId))
                .fetch();
        log.info("用户：{}，获取角色数量：{}", userId, roleKeys.size());
        return new HashSet<>(roleKeys);
    }


}
