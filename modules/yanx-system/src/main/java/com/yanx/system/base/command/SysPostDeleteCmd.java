package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.system.base.domain.entity.SysPost;
import com.yanx.system.base.infra.mapper.SysPostMapper;
import com.yanx.system.base.model.dto.SysPostDto;

import java.util.Collection;

/**
 * 岗位 删除命令
 *
 * @author: gotanks
 * @create: 2023-5-23
 */
public class SysPostDeleteCmd extends CrudCommand<SysPostDto> {

    /**
     * 删除
     *
     * @param id
     */
    public SysPostDeleteCmd(Long id) {
        super(id);
    }

    /**
     * 批量删除
     *
     * @param ids
     */
    public SysPostDeleteCmd(Collection<Long> ids) {
        super(ids);
    }

}