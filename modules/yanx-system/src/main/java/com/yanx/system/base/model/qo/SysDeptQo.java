package com.yanx.system.base.model.qo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 * 部门QO
 *
 * @author: gotanks
 * @create: 2023-2-8
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "部门查询模型")
public class SysDeptQo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(name = "部门ID")
    private Long deptId;

    @Schema(name = "上级部门ID")
    private Long parentId;

    @Schema(name = "部门名称")
    private String deptName;

    @Schema(name = "部门状态")
    private String status;

}
