package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.system.base.infra.mapper.SysDictDataMapper;
import com.yanx.system.base.model.dto.SysDictDataDto;

/**
 * 字典数据 新增命令
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
public class SysDictDataCreateCmd extends CrudCommand<SysDictDataDto> {

    /**
     * 新增
     *
     * @param sysDictDataDto
     */
    public SysDictDataCreateCmd(SysDictDataDto sysDictDataDto) {
        super(sysDictDataDto);
    }

    /**
     * 新增时实体类转换
     *
     * @return
     */
    @Override
    public BaseEntity toEntity() {
        return SysDictDataMapper.INSTANCE.toSysDictData(this.getDto());
    }

}