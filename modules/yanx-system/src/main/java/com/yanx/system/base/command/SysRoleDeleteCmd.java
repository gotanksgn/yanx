package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.system.base.model.dto.SysRoleDto;

import java.util.Collection;

/**
 * 菜单权限 删除命令
 *
 * @author: gotanks
 * @create: 2023-2-10
 */
public class SysRoleDeleteCmd extends CrudCommand<SysRoleDto> {

    /**
     * 删除
     *
     * @param id
     */
    public SysRoleDeleteCmd(Long id) {
        super(id);
    }

    /**
     * 批量删除
     *
     * @param ids
     */
    public SysRoleDeleteCmd(Collection<Long> ids) {
        super(ids);
    }

}