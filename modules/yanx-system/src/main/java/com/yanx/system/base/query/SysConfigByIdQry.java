package com.yanx.system.base.query;

import com.querydsl.core.types.Projections;
import com.querydsl.core.types.QBean;
import com.yanx.common.command.Executor;
import com.yanx.common.core.command.QryCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.system.base.domain.entity.QSysConfig;
import com.yanx.system.base.model.vo.SysConfigVo;
import lombok.AllArgsConstructor;

/**
 * 根据ID查询参数配置
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@AllArgsConstructor
public class SysConfigByIdQry extends QryCommand<SysConfigVo> {

    private Long id;

    @Override
    public SysConfigVo execute(Executor executor) {
        if (id == null) {
            throw new BusinessException("参数配置ID不能为空");
        }
        QSysConfig sysConfig = QSysConfig.sysConfig;
        return queryFactory.select(this.fields())
                .from(sysConfig)
                .where(sysConfig.deleted.eq(false), sysConfig.id.eq(id))
                .fetchOne();
    }

    /**
     * 参数配置VO映射
     *
     * @return QBean<SysConfigVo>
     */
    public static QBean<SysConfigVo> fields() {
        QSysConfig sysConfig = QSysConfig.sysConfig;
        return Projections.fields(
                SysConfigVo.class,
                sysConfig.configName,
                sysConfig.configKey,
                sysConfig.configValue,
                sysConfig.configType,
                sysConfig.remark,
                sysConfig.createTime,
                sysConfig.id
        );
    }
}