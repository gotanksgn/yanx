package com.yanx.system.base.domain.repository;

import com.yanx.common.core.domain.repository.BaseRepository;
import com.yanx.system.base.domain.entity.SysRole;

import java.io.Serializable;

/**
 * 菜单权限仓库
 *
 * @author: gotanks
 * @create: 2023-2-10
 */
public interface SysRoleRepository extends BaseRepository<SysRole, Serializable> {

    int countByRoleNameAndIdNot(String roleName, Long id);

    int countByRoleKeyAndIdNot(String roleKey, Long id);

//    @Transactional
//    @Modifying
//    @Query("delete from SysUserRole ur where ur.roleId = ?1 and ur.userId in ?2")
//    void deleteByRoleIdAndUserIdIn(Long roleId, Long[] userIds);
}
