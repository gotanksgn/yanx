package com.yanx.system.base.infra.mapper;

import com.yanx.system.base.model.dto.SysConfigDto;
import com.yanx.system.base.domain.entity.SysConfig;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
 * 参数配置 实体转换类
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface SysConfigMapper {

    SysConfigMapper INSTANCE = Mappers.getMapper(SysConfigMapper.class);

    SysConfig toSysConfig(SysConfigDto dto);

    SysConfig toSysConfig(SysConfigDto dto, @MappingTarget SysConfig entity);

}
