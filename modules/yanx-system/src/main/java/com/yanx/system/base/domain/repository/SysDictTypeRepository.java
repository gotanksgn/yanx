package com.yanx.system.base.domain.repository;

import com.yanx.common.core.domain.repository.BaseRepository;
import com.yanx.system.base.domain.entity.SysDictType;

import java.io.Serializable;

/**
 * 字典类型仓库
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
public interface SysDictTypeRepository extends BaseRepository<SysDictType, Serializable> {

    int countByDictType(String dictType);

    int countByDictTypeAndIdNot(String dictType, Long id);
}
