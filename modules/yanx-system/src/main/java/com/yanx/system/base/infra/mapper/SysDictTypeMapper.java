package com.yanx.system.base.infra.mapper;

import com.yanx.system.base.model.dto.SysDictTypeDto;
import com.yanx.system.base.domain.entity.SysDictType;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
 * 字典类型 实体转换类
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface SysDictTypeMapper {

    SysDictTypeMapper INSTANCE = Mappers.getMapper(SysDictTypeMapper.class);

    SysDictType toSysDictType(SysDictTypeDto dto);

    SysDictType toSysDictType(SysDictTypeDto dto, @MappingTarget SysDictType entity);

}
