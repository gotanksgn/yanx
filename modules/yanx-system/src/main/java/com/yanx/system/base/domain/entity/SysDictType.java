package com.yanx.system.base.domain.entity;

import cn.hutool.extra.spring.SpringUtil;
import com.yanx.codegen.annotation.CodeGenClass;
import com.yanx.codegen.annotation.CodeGenField;
import com.yanx.common.core.domain.entity.BaseTimeEntity;
import com.yanx.common.enums.DataStatus;
import com.yanx.system.base.domain.event.SysDictDeleteEvent;
import com.yanx.system.base.domain.event.SysDictSaveEvent;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.domain.DomainEvents;

import javax.persistence.Entity;
import javax.persistence.PostRemove;
import javax.persistence.Table;

/**
 * 字典类型表 sys_dict_type
 *
 * @author gotanks
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "SYS_DICT_TYPE")
@CodeGenClass(value = "字典类型", author = "gotanks")
public class SysDictType extends BaseTimeEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 字典名称
     */
    @CodeGenField("字典名称")
    private String dictName;

    /**
     * 字典类型
     */
    @CodeGenField("字典类型")
    private String dictType;

    /**
     * 状态（0正常 1停用）
     */
    @CodeGenField("状态")
    private DataStatus status;


    @DomainEvents
    Object domainEvents() {
        return new SysDictSaveEvent(dictType);
    }

    @PostRemove
    public void onPostRemove() {
        System.out.println("delete dict type success");
        SpringUtil.getApplicationContext().publishEvent(new SysDictDeleteEvent(dictType));
    }

}
