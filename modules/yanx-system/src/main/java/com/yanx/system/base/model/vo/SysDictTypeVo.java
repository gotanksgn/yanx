package com.yanx.system.base.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yanx.common.core.vo.BasePageVo;
import com.yanx.common.enums.DataStatus;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 字典类型VO
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "字典类型视图模型")
public class SysDictTypeVo extends BasePageVo<Long> {

    private static final long serialVersionUID = 1L;

    @Schema(name = "字典名称")
    private String dictName;

    @Schema(name = "字典类型")
    private String dictType;

    @Schema(name = "状态")
    private DataStatus status;

}
