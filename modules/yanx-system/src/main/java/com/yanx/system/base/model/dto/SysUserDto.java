package com.yanx.system.base.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yanx.common.core.dto.BaseDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 用户DTO
 *
 * @author: gotanks
 * @create: 2023-2-6
 */
@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "用户入参模型")
public class SysUserDto extends BaseDto<Long> {

    private static final long serialVersionUID = 1L;

    //兼容ruoyi
    @Schema(name = "用户ID")
    public void setUserId(Long userId) {
        this.setId(userId);
    }

    @Schema(name = "用户账号")
    private String userName;

    @Schema(name = "用户密码")
    private String password;

    @Schema(name = "用户昵称")
    private String nickName;

    @Schema(name = "用户类型")
    private String userType;

    @Schema(name = "帐号状态")
    private String status;

    @Schema(name = "用户邮箱")
    private String email;

    @Schema(name = "手机号码")
    private String phonenumber;

    @Schema(name = "用户头像")
    private String avatar;

    @Schema(name = "用户性别")
    private String sex;

    @Schema(name = "最后登录IP")
    private String loginIp;

    @Schema(name = "最后登录时间")
    private LocalDateTime loginDate;

    @Schema(name = "部门ID")
    private Long deptId;

    //extra
    @Schema(name = "角色组")
    private List<Long> roleIds;

    @Schema(name = "岗位组")
    private List<Long> postIds;

}
