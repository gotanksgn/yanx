package com.yanx.system.base.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.yanx.common.core.vo.BasePageVo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 岗位VO
 *
 * @author: gotanks
 * @create: 2023-5-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "岗位视图模型")
public class SysPostVo extends BasePageVo<Long> {

    private static final long serialVersionUID = 1L;

    //兼容ruoyi
    @JsonSerialize(using = ToStringSerializer.class)
    @Schema(name = "岗位ID")
    public Long getPostId() {
        return getId();
    }

    @Schema(name = "岗位编码")
    private String postCode;

    @Schema(name = "岗位名称")
    private String postName;

    @Schema(name = "岗位排序")
    private Integer postSort;

    @Schema(name = "状态")
    private String status;

}
