package com.yanx.system.base.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yanx.common.core.vo.BasePageVo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;


/**
 * 参数配置VO
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "参数配置视图模型")
public class SysConfigVo extends BasePageVo<Long> {

    private static final long serialVersionUID = 1L;

    @Schema(name = "参数名称")
    private String configName;

    @Schema(name = "参数键名")
    private String configKey;

    @Schema(name = "参数键值")
    private String configValue;

    @Schema(name = "系统内置")
    private String configType;

    @Schema(name = "创建时间")
    private LocalDateTime createTime;

    @Schema(name = "备注")
    private String remark;

}
