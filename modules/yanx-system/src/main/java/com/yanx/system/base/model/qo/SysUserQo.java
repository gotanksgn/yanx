package com.yanx.system.base.model.qo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yanx.common.utils.DateTime_;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * 用户QO
 *
 * @author: gotanks
 * @create: 2023-2-6
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "用户查询模型")
public class SysUserQo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(name = "用户ID")
    private Long userId;

    @Schema(name = "用户账号")
    private String userName;

    @Schema(name = "状态")
    private String status;

    @Schema(name = "手机号码")
    private String phonenumber;

    @Schema(name = "部门ID")
    private Long deptId;

    @Schema(name = "请求参数")
    private Map<String, Object> params = new HashMap<>();

    public LocalDateTime getBeginTime() {
        Object beginTime = params.get("beginTime");
        if (beginTime != null) {
            return DateTime_.parseDateTime(beginTime.toString());
        }
        return null;
    }

    public LocalDateTime getEndTime() {
        Object endTime = params.get("endTime");
        if (endTime != null) {
            return DateTime_.parseDateTime(endTime.toString());
        }
        return null;
    }
}
