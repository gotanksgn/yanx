package com.yanx.system.web.infra.cache;

import cn.hutool.extra.spring.SpringUtil;
import com.yanx.common.command.executor.QueryExecutor;
import com.yanx.common.utils.String_;
import com.yanx.framework.redis.RedisCacheManager;
import com.yanx.system.base.model.vo.SysConfigVo;
import com.yanx.system.base.query.SysConfigListQry;

import java.util.List;

/**
 * 系统配置缓存
 */
public enum SysConfigCache implements RedisCacheManager<String> {

    INSTANCE;

    @Override
    public String key(Object... keyParts) {
        return String_.format("sys_config:{}", keyParts);
    }

    public void loadingAll() {
        QueryExecutor queryExecutor = SpringUtil.getBean(QueryExecutor.class);
        List<SysConfigVo> sysConfigVos = queryExecutor.execute(new SysConfigListQry(null));
        for (SysConfigVo config : sysConfigVos) {
            SysConfigCache.INSTANCE.set(new String[]{config.getConfigKey()}, config.getConfigValue());
        }
    }

//    @Override
//    public Integer timeout() {
//        return 3600;
//    }
}
