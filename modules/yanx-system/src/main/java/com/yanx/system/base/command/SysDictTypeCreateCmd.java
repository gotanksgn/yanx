package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.system.base.infra.mapper.SysDictTypeMapper;
import com.yanx.system.base.model.dto.SysDictTypeDto;

/**
 * 字典类型 新增命令
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
public class SysDictTypeCreateCmd extends CrudCommand<SysDictTypeDto> {

    /**
     * 新增
     *
     * @param sysDictTypeDto
     */
    public SysDictTypeCreateCmd(SysDictTypeDto sysDictTypeDto) {
        super(sysDictTypeDto);
    }

    /**
     * 新增时实体类转换
     *
     * @return
     */
    @Override
    public BaseEntity toEntity() {
        return SysDictTypeMapper.INSTANCE.toSysDictType(this.getDto());
    }

}