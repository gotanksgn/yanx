package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.system.base.infra.mapper.SysUserMapper;
import com.yanx.system.base.model.dto.SysUserDto;

/**
 * 用户 新增命令
 *
 * @author: gotanks
 * @create: 2023-2-6
 */
public class SysUserCreateCmd extends CrudCommand<SysUserDto> {

    /**
     * 新增
     *
     * @param sysUserDto
     */
    public SysUserCreateCmd(SysUserDto sysUserDto) {
        super(sysUserDto);
    }

    /**
     * 新增时实体类转换
     *
     * @return
     */
    @Override
    public BaseEntity toEntity() {
        return SysUserMapper.INSTANCE.toSysUser(this.getDto());
    }

}