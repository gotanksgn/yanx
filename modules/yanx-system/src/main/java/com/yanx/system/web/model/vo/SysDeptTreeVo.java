package com.yanx.system.web.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yanx.common.core.vo.BaseTreeVo;
import com.yanx.system.base.model.vo.SysDeptVo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.stream.Collectors;


/**
 * 部门树模型
 *
 * @author: gotanks
 * @create: 2023-2-9
 */
@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "部门树模型")
public class SysDeptTreeVo extends BaseTreeVo<Long> {

    private static final long serialVersionUID = 1L;

    /**
     * 节点名称
     */
    private String label;

    public SysDeptTreeVo(SysDeptVo deptVo) {
        this.setId(deptVo.getId());
        this.setLabel(deptVo.getDeptName());
        this.setChildren(deptVo.getChildren().stream().map(t -> new SysDeptTreeVo((SysDeptVo) t)).collect(Collectors.toList()));
    }

}
