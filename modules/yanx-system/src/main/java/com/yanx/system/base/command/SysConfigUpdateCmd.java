package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.system.base.domain.entity.SysConfig;
import com.yanx.system.base.infra.mapper.SysConfigMapper;
import com.yanx.system.base.model.dto.SysConfigDto;

/**
 * 参数配置 修改命令
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
public class SysConfigUpdateCmd extends CrudCommand<SysConfigDto> {

    /**
     * 修改
     *
     * @param id
    * @param sysConfigDto
     */
    public SysConfigUpdateCmd(Long id, SysConfigDto sysConfigDto) {
        super(id, sysConfigDto);
    }

    /**
     * 修改时实体类转换
     *
     * @param entity
     * @return
     */
    @Override
    public BaseEntity toEntity(BaseEntity entity) {
        return SysConfigMapper.INSTANCE.toSysConfig(this.getDto(), (SysConfig) entity);
    }

}