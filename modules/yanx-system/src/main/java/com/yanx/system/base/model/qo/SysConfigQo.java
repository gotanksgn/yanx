package com.yanx.system.base.model.qo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yanx.common.utils.DateTime_;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * 参数配置QO
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "参数配置查询模型")
public class SysConfigQo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(name = "参数名称")
    private String configName;

    @Schema(name = "参数键名")
    private String configKey;

    @Schema(name = "参数键值")
    private String configValue;

    @Schema(name = "系统内置")
    private String configType;

    @Schema(name = "备注")
    private String remark;

    @Schema(name = "请求参数")
    private Map<String, Object> params = new HashMap<>();

    public LocalDateTime getBeginTime() {
        Object beginTime = params.get("beginTime");
        if (beginTime != null) {
            return DateTime_.parseDateTime(beginTime.toString());
        }
        return null;
    }

    public LocalDateTime getEndTime() {
        Object endTime = params.get("endTime");
        if (endTime != null) {
            return DateTime_.parseDateTime(endTime.toString());
        }
        return null;
    }
}
