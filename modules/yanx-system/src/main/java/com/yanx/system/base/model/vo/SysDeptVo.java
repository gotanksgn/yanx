package com.yanx.system.base.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.yanx.common.core.vo.BaseTreeVo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 部门VO
 *
 * @author: gotanks
 * @create: 2023-2-8
 */
@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "部门视图模型")
public class SysDeptVo extends BaseTreeVo<Long> {

    private static final long serialVersionUID = 1L;

    //兼容ruoyi
    @JsonSerialize(using = ToStringSerializer.class)
    @Schema(name = "部门ID")
    public Long getDeptId() {
        return this.getId();
    }

    @Schema(name = "祖先节点")
    private String ancestors;

//    @Schema(name = "父部门名称")
//    private String parentName;
//
//    @Schema(name = "父部门ID")
//    private Long parentId;

    @Schema(name = "部门名称")
    private String deptName;

    @Schema(name = "显示顺序")
    private Integer orderNum;

    @Schema(name = "负责人")
    private String leader;

    @Schema(name = "联系电话")
    private String phone;

    @Schema(name = "邮箱")
    private String email;

    @Schema(name = "部门状态")
    private String status;

}
