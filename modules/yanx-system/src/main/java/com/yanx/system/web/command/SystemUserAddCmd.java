package com.yanx.system.web.command;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.VoidCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.common.security.SecurityUtils;
import com.yanx.system.base.command.SysUserCreateCmd;
import com.yanx.system.base.domain.manager.SysUserManager;
import com.yanx.system.base.model.dto.SysUserDto;
import lombok.AllArgsConstructor;

/**
 * 用户信息 新增命令
 *
 * @author: gotanks
 * @create: 2023-5-23
 */
@AllArgsConstructor
public class SystemUserAddCmd extends VoidCommand {
    private SysUserDto user;

    @Override
    public void handler(Executor executor) {
        SysUserManager sysUserManager = executor.getReceiver(SysUserManager.class);
        if (sysUserManager.checkUserNameUnique(user.getUserName(), user.getId())) {
            throw new BusinessException("新增用户'{}'失败，登录账号已存在", user.getUserName());
        }
        if (sysUserManager.checkPhoneUnique(user.getPhonenumber(), user.getId())) {
            throw new BusinessException("新增用户'{}'失败，手机号码已存在", user.getUserName());
        }
        if (sysUserManager.checkEmailUnique(user.getEmail(), user.getId())) {
            throw new BusinessException("新增用户'{}'失败，邮箱账号已存在", user.getUserName());
        }

//        user.setCreateBy(getUsername());
        user.setPassword(SecurityUtils.encryptPassword(user.getPassword()));
        executor.execute(new SysUserCreateCmd(user));
    }
}