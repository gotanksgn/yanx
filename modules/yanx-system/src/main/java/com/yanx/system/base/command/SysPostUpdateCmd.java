package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.system.base.domain.entity.SysPost;
import com.yanx.system.base.infra.mapper.SysPostMapper;
import com.yanx.system.base.model.dto.SysPostDto;

/**
 * 岗位 修改命令
 *
 * @author: gotanks
 * @create: 2023-5-23
 */
public class SysPostUpdateCmd extends CrudCommand<SysPostDto> {

    /**
     * 修改
     *
     * @param id
    * @param sysPostDto
     */
    public SysPostUpdateCmd(Long id, SysPostDto sysPostDto) {
        super(id, sysPostDto);
    }

    /**
     * 修改时实体类转换
     *
     * @param entity
     * @return
     */
    @Override
    public BaseEntity toEntity(BaseEntity entity) {
        return SysPostMapper.INSTANCE.toSysPost(this.getDto(), (SysPost) entity);
    }

}