package com.yanx.system.base.domain.entity;

import cn.hutool.extra.spring.SpringUtil;
import com.yanx.codegen.annotation.CodeGenClass;
import com.yanx.codegen.annotation.CodeGenField;
import com.yanx.common.core.domain.entity.BaseTimeEntity;
import com.yanx.system.base.domain.event.SysConfigDeleteEvent;
import com.yanx.system.base.domain.event.SysConfigSaveEvent;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.domain.DomainEvents;

import javax.persistence.Entity;
import javax.persistence.PostRemove;
import javax.persistence.Table;

/**
 * 参数配置表 sys_config
 *
 * @author gotanks
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "SYS_CONFIG")
@CodeGenClass(value = "参数配置", author = "gotanks")
public class SysConfig extends BaseTimeEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 参数名称
     */
    @CodeGenField("参数名称")
    private String configName;

    /**
     * 参数键名
     */
    @CodeGenField("参数键名")
    private String configKey;

    /**
     * 参数键值
     */
    @CodeGenField("参数键值")
    private String configValue;

    /**
     * 系统内置（Y是 N否）
     */
    @CodeGenField("系统内置")
    private String configType;

    /**
     * 备注
     */
    @CodeGenField("备注")
    private String remark;

    @DomainEvents
    Object domainEvents() {
        return new SysConfigSaveEvent(this);
    }

    @PostRemove
    public void onPostRemove() {
        System.out.println("delete config success");
        SpringUtil.getApplicationContext().publishEvent(new SysConfigDeleteEvent(this));
    }
}
