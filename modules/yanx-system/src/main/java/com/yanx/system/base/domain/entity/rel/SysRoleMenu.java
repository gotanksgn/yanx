package com.yanx.system.base.domain.entity.rel;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 角色和菜单关联 sys_role_menu
 *
 * @author gotanks
 */
@Data
@Entity
@Table(name = "SYS_ROLE_MENU")
@IdClass(value = SysRoleMenu.class)
public class SysRoleMenu implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 角色ID
     */
    @Id
    @JoinColumn(foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    private Long roleId;

    /**
     * 菜单ID
     */
    @Id
    @JoinColumn(foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    private Long menuId;

}
