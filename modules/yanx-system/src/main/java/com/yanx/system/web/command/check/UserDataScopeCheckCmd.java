package com.yanx.system.web.command.check;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.VoidCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.common.utils.Collect_;
import com.yanx.system.base.model.qo.SysUserQo;
import com.yanx.system.web.model.vo.SysUserWithDeptVo;
import com.yanx.system.web.query.SysUserWithDeptListQry;
import lombok.AllArgsConstructor;

import java.util.List;

/**
 * 校验角色是否允许修改
 *
 * @author: gotanks
 * @create: 2023-6-7
 */
@AllArgsConstructor
public class UserDataScopeCheckCmd extends VoidCommand {

    private Long userId;

    @Override
    public void handler(Executor executor) {
        SysUserQo sysUserQo = new SysUserQo();
        sysUserQo.setUserId(userId);
        List<SysUserWithDeptVo> userWithDeptVos = executor.execute(new SysUserWithDeptListQry(sysUserQo));
        if (Collect_.isEmpty(userWithDeptVos)) {
            throw new BusinessException("没有权限访问用户数据！");
        }
    }
}