package com.yanx.system.base.query;

import com.querydsl.core.types.Projections;
import com.querydsl.core.types.QBean;
import com.yanx.common.command.Executor;
import com.yanx.common.core.command.QryCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.system.base.domain.entity.QSysDictType;
import com.yanx.system.base.model.vo.SysDictTypeVo;
import lombok.AllArgsConstructor;

/**
 * 根据ID查询字典类型
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
@AllArgsConstructor
public class SysDictTypeByIdQry extends QryCommand<SysDictTypeVo> {

    private Long id;

    @Override
    public SysDictTypeVo execute(Executor executor) {
        if (id == null) {
            throw new BusinessException("字典类型ID不能为空");
        }
        QSysDictType sysDictType = QSysDictType.sysDictType;
        return queryFactory.select(this.fields())
                .from(sysDictType)
                .where(sysDictType.deleted.eq(false), sysDictType.id.eq(id))
                .fetchOne();
    }

    /**
     * 字典类型VO映射
     *
     * @return QBean<SysDictTypeVo>
     */
    public static QBean<SysDictTypeVo> fields() {
        QSysDictType sysDictType = QSysDictType.sysDictType;
        return Projections.fields(
            SysDictTypeVo.class,
            sysDictType.dictName,
            sysDictType.dictType,
            sysDictType.status,
            sysDictType.id
        );
    }
}