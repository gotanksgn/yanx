package com.yanx.system.web.api;

import com.yanx.common.annotation.ApiResult;
import com.yanx.common.annotation.OperationLog;
import com.yanx.common.annotation.Pagination;
import com.yanx.common.core.api.BaseApi;
import com.yanx.common.enums.BusinessType;
import com.yanx.common.security.preauthorize.PreAuthorize;
import com.yanx.system.base.model.dto.SysDictTypeDto;
import com.yanx.system.base.model.qo.SysDictTypeQo;
import com.yanx.system.base.model.vo.SysDictTypeVo;
import com.yanx.system.base.query.SysDictTypeByIdQry;
import com.yanx.system.base.query.SysDictTypeListQry;
import com.yanx.system.web.command.SystemDictTypeAddCmd;
import com.yanx.system.web.command.SystemDictTypeEditCmd;
import com.yanx.system.web.command.SystemDictTypeRefreshCmd;
import com.yanx.system.web.command.SystemDictTypeRemoveCmd;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 数据字典 接口
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
@Tag(name = "数据字典", description = "数据字典")
@RestController
@RequestMapping(value = "/system/dict/type")
public class SystemDictTypeApi extends BaseApi {

    @PreAuthorize(hasPermi = "system:dict:list")
    @Pagination(total = true)
    @ApiResult
    @Operation(summary = "获取数据字典列表")
    @GetMapping("/list")
    public List<SysDictTypeVo> list(SysDictTypeQo dictType) {
        return queryExecutor.execute(new SysDictTypeListQry(dictType));
    }

//    @OperationLog(title = "字典类型", businessType = BusinessType.EXPORT)
//    @PreAuthorize(hasPermi = "system:dict:export")
//    @ApiResult
//    @Operation(summary = "查询部门列表（排除节点）")
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, SysDictType dictType) {
//        List<SysDictType> list = dictTypeService.selectDictTypeList(dictType);
//        ExcelUtil<SysDictType> util = new ExcelUtil<SysDictType>(SysDictType.class);
//        util.exportExcel(response, list, "字典类型");
//    }

    @PreAuthorize(hasPermi = "system:dict:query")
    @ApiResult
    @Operation(summary = "查询字典类型详细")
    @GetMapping(value = "/{dictId}")
    public SysDictTypeVo getInfo(@PathVariable Long dictId) {
        return queryExecutor.execute(new SysDictTypeByIdQry(dictId));
    }

    /**
     * 新增字典类型
     */
    @PreAuthorize(hasPermi = "system:dict:add")
    @OperationLog(title = "字典类型", businessType = BusinessType.INSERT)
    @ApiResult
    @Operation(summary = "新增字典类型")
    @PostMapping
    public void add(@Validated @RequestBody SysDictTypeDto dict) {
        commandExecutor.execute(new SystemDictTypeAddCmd(dict));
    }

    @PreAuthorize(hasPermi = "system:dict:edit")
    @OperationLog(title = "字典类型", businessType = BusinessType.UPDATE)
    @ApiResult
    @Operation(summary = "修改字典类型")
    @PutMapping
    public void edit(@Validated @RequestBody SysDictTypeDto dict) {
        commandExecutor.execute(new SystemDictTypeEditCmd(dict));
    }

    @PreAuthorize(hasPermi = "system:dict:remove")
    @OperationLog(title = "字典类型", businessType = BusinessType.DELETE)
    @ApiResult
    @Operation(summary = "删除字典类型")
    @DeleteMapping("/{dictIds}")
    public void remove(@PathVariable Long[] dictIds) {
        commandExecutor.execute(new SystemDictTypeRemoveCmd(dictIds));
    }

    @PreAuthorize(hasPermi = "system:dict:remove")
    @OperationLog(title = "字典类型", businessType = BusinessType.CLEAN)
    @ApiResult
    @Operation(summary = "刷新字典缓存")
    @DeleteMapping("/refreshCache")
    public void refreshCache() {
        commandExecutor.execute(new SystemDictTypeRefreshCmd());
    }

    /**
     * 获取字典选择框列表
     */
    @ApiResult
    @Operation(summary = "获取字典选择框列表")
    @GetMapping("/optionselect")
    public List<SysDictTypeVo> optionselect() {
        return queryExecutor.execute(new SysDictTypeListQry(new SysDictTypeQo()));
    }
}