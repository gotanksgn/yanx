package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.system.base.domain.entity.SysDictType;
import com.yanx.system.base.infra.mapper.SysDictTypeMapper;
import com.yanx.system.base.model.dto.SysDictTypeDto;

import java.util.Collection;

/**
 * 字典类型 删除命令
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
public class SysDictTypeDeleteCmd extends CrudCommand<SysDictTypeDto> {

    /**
     * 删除
     *
     * @param id
     */
    public SysDictTypeDeleteCmd(Long id) {
        super(id);
    }

    /**
     * 批量删除
     *
     * @param ids
     */
    public SysDictTypeDeleteCmd(Collection<Long> ids) {
        super(ids);
    }

}