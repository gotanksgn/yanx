package com.yanx.system.web.command;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.VoidCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.system.base.command.SysDeptDeleteCmd;
import com.yanx.system.base.domain.manager.SysDeptManager;
import lombok.AllArgsConstructor;

/**
 * 部门 删除命令
 *
 * @author: gotanks
 * @create: 2023-2-9
 */
@AllArgsConstructor
public class SystemDeptRemoveCmd extends VoidCommand {

    private Long deptId;

    @Override
    public void handler(Executor executor) {
        SysDeptManager sysDeptManager = executor.getReceiver(SysDeptManager.class);
        if (sysDeptManager.hasChildByDeptId(deptId)) {
            throw new BusinessException("存在下级部门,不允许删除");
        }
        if (sysDeptManager.checkDeptExistUser(deptId)) {
            throw new BusinessException("部门存在用户,不允许删除");
        }
//        deptService.checkDeptDataScope(deptId);
        executor.execute(new SysDeptDeleteCmd(deptId));
    }
}