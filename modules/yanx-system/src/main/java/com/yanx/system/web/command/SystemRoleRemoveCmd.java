package com.yanx.system.web.command;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.VoidCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.common.utils.Collect_;
import com.yanx.system.base.command.SysRoleDeleteCmd;
import com.yanx.system.base.domain.entity.SysRole;
import com.yanx.system.base.domain.manager.SysRoleManager;
import lombok.AllArgsConstructor;

import java.util.Set;

/**
 * 菜单权限 删除
 *
 * @author: gotanks
 * @create: 2023-2-9
 */
@AllArgsConstructor
public class SystemRoleRemoveCmd extends VoidCommand {
    private Set<Long> roleIds;

    @Override
    public void handler(Executor executor) {
        for (Long roleId : roleIds) {
            SysRoleManager sysRoleManager = executor.getReceiver(SysRoleManager.class);
            SysRole sysRole = sysRoleManager.getById(roleId);
            if (Collect_.isNotEmpty(sysRole.getUserRoles())) {
                throw new BusinessException("{}已分配,不能删除", sysRole.getRoleName());
            }
        }
        executor.execute(new SysRoleDeleteCmd(roleIds));
    }
}