package com.yanx.system.web.query;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.QryCommand;
import com.yanx.system.base.domain.entity.QSysMenu;
import com.yanx.system.base.domain.entity.rel.QSysRoleMenu;
import com.yanx.system.base.domain.entity.rel.QSysUserRole;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 根据用户ID获取权限
 *
 * @author gotanks
 * @date 2021-04-20 09:49:00
 */
@Slf4j

@AllArgsConstructor
public class PremsByUserIdQry extends QryCommand<Set<String>> {

    private Long userId;

    @Override
    public Set<String> execute(Executor executor) {
        QSysRoleMenu sysRoleMenu = QSysRoleMenu.sysRoleMenu;
        QSysUserRole sysUserRole = QSysUserRole.sysUserRole;
        QSysMenu sysMenu = QSysMenu.sysMenu;
        List<String> perms = queryFactory.select(sysMenu.perms)
                .from(sysRoleMenu)
                .join(sysMenu).on(sysRoleMenu.menuId.eq(sysMenu.id))
                .where(sysRoleMenu.roleId.in(
                        queryFactory.select(sysUserRole.roleId).from(sysUserRole).where(sysUserRole.userId.eq(userId))
                ))
                .fetch();
        log.info("用户：{}，获取权限数量：{}", userId, perms.size());
        return new HashSet<>(perms);
    }


}
