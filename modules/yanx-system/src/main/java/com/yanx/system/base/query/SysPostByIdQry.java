package com.yanx.system.base.query;

import com.querydsl.core.types.Projections;
import com.querydsl.core.types.QBean;
import com.yanx.common.command.Executor;
import com.yanx.common.core.command.QryCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.system.base.domain.entity.QSysPost;
import com.yanx.system.base.model.vo.SysPostVo;
import lombok.AllArgsConstructor;

/**
 * 根据ID查询岗位
 *
 * @author: gotanks
 * @create: 2023-5-23
 */
@AllArgsConstructor
public class SysPostByIdQry extends QryCommand<SysPostVo> {

    private Long id;

    @Override
    public SysPostVo execute(Executor executor) {
        if (id == null) {
            throw new BusinessException("岗位ID不能为空");
        }
        QSysPost sysPost = QSysPost.sysPost;
        return queryFactory.select(this.fields())
                .from(sysPost)
                .where(sysPost.deleted.eq(false), sysPost.id.eq(id))
                .fetchOne();
    }

    /**
     * 岗位VO映射
     *
     * @return QBean<SysPostVo>
     */
    public static QBean<SysPostVo> fields() {
        QSysPost sysPost = QSysPost.sysPost;
        return Projections.fields(
                SysPostVo.class,
                sysPost.postCode,
                sysPost.postName,
                sysPost.postSort,
                sysPost.status,
                sysPost.createTime,
                sysPost.id
        );
    }
}