package com.yanx.system.base.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yanx.common.core.dto.BaseDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;


/**
 * 菜单权限DTO
 *
 * @author: gotanks
 * @create: 2023-2-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "菜单权限入参模型")
public class SysRoleDto extends BaseDto<Long> {

    private static final long serialVersionUID = 1L;

    //兼容ruoyi
    @Schema(name = "角色ID")
    public void setRoleId(Long roleId) {
        this.setId(roleId);
    }

    @Schema(name = "角色名称")
    private String roleName;

    @Schema(name = "角色ID")
    private String roleKey;

    @Schema(name = "角色ID")
    private Integer roleSort;

    @Schema(name = "数据范围")
    private String dataScope;

    @Schema(name = "菜单树选择项是否关联显示")
    private boolean menuCheckStrictly;

    @Schema(name = "部门树选择项是否关联显示")
    private boolean deptCheckStrictly;

    @Schema(name = "角色状态")
    private String status;

    @Schema(name = "菜单组")
    private List<Long> menuIds;

    @Schema(name = "部门组（数据权限）")
    private List<Long> deptIds;

}
