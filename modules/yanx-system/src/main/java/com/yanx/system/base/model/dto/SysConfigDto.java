package com.yanx.system.base.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yanx.common.core.dto.BaseDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 参数配置DTO
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "参数配置入参模型")
public class SysConfigDto extends BaseDto<Long> {

    private static final long serialVersionUID = 1L;

    @Schema(name = "参数名称")
    private String configName;

    @Schema(name = "参数键名")
    private String configKey;

    @Schema(name = "参数键值")
    private String configValue;

    @Schema(name = "系统内置")
    private String configType;

    @Schema(name = "备注")
    private String remark;

}
