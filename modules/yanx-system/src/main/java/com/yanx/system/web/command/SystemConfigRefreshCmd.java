package com.yanx.system.web.command;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.VoidCommand;
import com.yanx.system.base.domain.manager.SysConfigManager;

/**
 * 重置系统配置缓存
 *
 * @author: gotanks
 * @create: 2023-2-9
 */
public class SystemConfigRefreshCmd extends VoidCommand {

    @Override
    public void handler(Executor executor) {
        SysConfigManager sysConfigManager = executor.getReceiver(SysConfigManager.class);
        sysConfigManager.resetConfigCache();
    }
}