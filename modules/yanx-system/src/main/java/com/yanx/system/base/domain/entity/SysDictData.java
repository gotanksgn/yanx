package com.yanx.system.base.domain.entity;


import cn.hutool.extra.spring.SpringUtil;
import com.yanx.codegen.annotation.CodeGenClass;
import com.yanx.codegen.annotation.CodeGenField;
import com.yanx.common.core.domain.entity.BaseTimeEntity;
import com.yanx.common.enums.DataStatus;
import com.yanx.system.base.domain.event.SysDictSaveEvent;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.domain.DomainEvents;

import javax.persistence.Entity;
import javax.persistence.PostRemove;
import javax.persistence.Table;

/**
 * 字典数据表 sys_dict_data
 *
 * @author gotanks
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "SYS_DICT_DATA")
@CodeGenClass(value = "字典数据", author = "gotanks")
public class SysDictData extends BaseTimeEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 字典排序
     */
    @CodeGenField("字典排序")
    private Long dictSort;

    /**
     * 字典标签
     */
    @CodeGenField("字典标签")
    private String dictLabel;

    /**
     * 字典键值
     */
    @CodeGenField("字典键值")
    private String dictValue;

    /**
     * 字典类型
     */
    @CodeGenField("字典类型")
    private String dictType;

    /**
     * 样式属性（其他样式扩展）
     */
    private String cssClass;

    /**
     * 表格字典样式
     */
    private String listClass;

    /**
     * 是否默认（Y是 N否）
     */
    @CodeGenField("是否默认")
    private String isDefault;

    /**
     * 状态（0正常 1停用）
     */
    @CodeGenField("状态")
    private DataStatus status;


    @DomainEvents
    Object domainEvents() {
        return new SysDictSaveEvent(dictType);
    }

    @PostRemove
    public void onPostRemove() {
        System.out.println("delete dict data success");
        SpringUtil.getApplicationContext().publishEvent(new SysDictSaveEvent(dictType));
    }
}
