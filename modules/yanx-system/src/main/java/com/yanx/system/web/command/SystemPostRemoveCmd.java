package com.yanx.system.web.command;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.VoidCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.system.base.command.SysPostDeleteCmd;
import com.yanx.system.base.domain.entity.SysPost;
import com.yanx.system.base.domain.manager.SysPostManager;
import lombok.AllArgsConstructor;

import java.util.Set;

/**
 * 岗位信息 删除
 *
 * @author: gotanks
 * @create: 2023-5-23
 */
@AllArgsConstructor
public class SystemPostRemoveCmd extends VoidCommand {
    private Set<Long> postIds;

    @Override
    public void handler(Executor executor) {
        SysPostManager sysPostManager = executor.getReceiver(SysPostManager.class);
        for (Long postId : postIds) {
            SysPost post = sysPostManager.getById(postId);
            if (sysPostManager.checkUserPostExist(postId)) {
                throw new BusinessException("{}已分配,不能删除", post.getPostName());
            }
        }
        executor.execute(new SysPostDeleteCmd(postIds));
    }
}