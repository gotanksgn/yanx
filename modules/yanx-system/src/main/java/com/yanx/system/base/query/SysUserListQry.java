package com.yanx.system.base.query;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.yanx.common.command.Executor;
import com.yanx.common.core.command.PageQryCommand;
import com.yanx.system.base.domain.entity.QSysUser;
import com.yanx.system.base.model.qo.SysUserQo;
import com.yanx.system.base.model.vo.SysUserVo;
import lombok.AllArgsConstructor;

import java.util.List;

/**
 * 用户分页查询
 *
 * @author: gotanks
 * @create: 2023-4-12
 */
@AllArgsConstructor
public class SysUserListQry extends PageQryCommand<List<SysUserVo>> {

    private SysUserQo sysUserQo;

    @Override
    public List<SysUserVo> execute(Executor executor) {
        QSysUser sysUser = QSysUser.sysUser;

        //查询条件
        BooleanBuilder condition = new BooleanBuilder();
        condition.and(sysUser.deleted.eq(false));
        if (sysUserQo != null) {
            //自定义查询条件
            if (sysUserQo.getUserId() != null && sysUserQo.getUserId() != 0) {
                condition.and(sysUser.id.eq(sysUserQo.getUserId()));
            }
            if (sysUserQo.getDeptId() != null && sysUserQo.getDeptId() != 0) {
                condition.and(sysUser.deptId.eq(sysUserQo.getDeptId()));
            }
            if (sysUserQo.getUserName() != null && !sysUserQo.getUserName().equals("")) {
                condition.and(sysUser.userName.contains(sysUserQo.getUserName()));
            }
            if (sysUserQo.getPhonenumber() != null && !sysUserQo.getPhonenumber().equals("")) {
                condition.and(sysUser.phonenumber.contains(sysUserQo.getPhonenumber()));
            }
            if (sysUserQo.getStatus() != null && !sysUserQo.getStatus().equals("")) {
                condition.and(sysUser.status.eq(sysUserQo.getStatus()));
            }
            if (sysUserQo.getBeginTime() != null) {
                condition.and(sysUser.createTime.gt(sysUserQo.getBeginTime()));
            }
            if (sysUserQo.getEndTime() != null) {
                condition.and(sysUser.createTime.lt(sysUserQo.getEndTime()));
            }
        }

        JPAQuery<SysUserVo> query = queryFactory.select(SysUserByIdQry.fields())
                .from(sysUser)
                .where(condition);

        return this.pageList(query);
    }

}