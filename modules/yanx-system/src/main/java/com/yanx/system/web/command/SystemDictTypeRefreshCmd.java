package com.yanx.system.web.command;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.VoidCommand;
import com.yanx.system.base.domain.manager.SysDictTypeManager;

/**
 * 重置字典缓存
 *
 * @author: gotanks
 * @create: 2023-6-9
 */
public class SystemDictTypeRefreshCmd extends VoidCommand {

    @Override
    public void handler(Executor executor) {
        SysDictTypeManager sysDictTypeManager = executor.getReceiver(SysDictTypeManager.class);
        sysDictTypeManager.resetDictCache();
    }
}