package com.yanx.system.web.command;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.VoidCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.common.utils.String_;
import com.yanx.system.base.command.SysMenuUpdateCmd;
import com.yanx.system.base.domain.manager.SysMenuManager;
import com.yanx.system.base.model.dto.SysMenuDto;
import com.yanx.system.web.infra.constant.UserConstants;
import lombok.AllArgsConstructor;

/**
 * 菜单权限 修改
 *
 * @author: gotanks
 * @create: 2023-2-9
 */
@AllArgsConstructor
public class SystemMenuEditCmd extends VoidCommand {
    private SysMenuDto menu;

    @Override
    public void handler(Executor executor) {
        SysMenuManager sysMenuManager = executor.getReceiver(SysMenuManager.class);
        if (sysMenuManager.checkMenuNameUnique(menu.getMenuName(), menu.getParentId(), menu.getId())) {
            throw new BusinessException("修改菜单'{}'失败，菜单名称已存在", menu.getMenuName());
        } else if (UserConstants.YES_FRAME.equals(menu.getIsFrame()) && !String_.startWithAny(menu.getPath(), "http://", "https://")) {
            throw new BusinessException("修改菜单'{}'失败，地址必须以http(s)://开头", menu.getMenuName());
        } else if (menu.getId().equals(menu.getParentId())) {
            throw new BusinessException("修改菜单'{}'失败，上级菜单不能选择自己", menu.getMenuName());
        }
        executor.execute(new SysMenuUpdateCmd(menu.getId(), menu));
    }
}