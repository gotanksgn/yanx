package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.system.base.domain.entity.SysDept;
import com.yanx.system.base.infra.mapper.SysDeptMapper;
import com.yanx.system.base.model.dto.SysDeptDto;

/**
 * 部门 修改命令
 *
 * @author: gotanks
 * @create: 2023-2-8
 */
public class SysDeptUpdateCmd extends CrudCommand<SysDeptDto> {

    /**
     * 修改
     *
     * @param id
    * @param sysDeptDto
     */
    public SysDeptUpdateCmd(Long id, SysDeptDto sysDeptDto) {
        super(id, sysDeptDto);
    }

    /**
     * 修改时实体类转换
     *
     * @param entity
     * @return
     */
    @Override
    public BaseEntity toEntity(BaseEntity entity) {
        return SysDeptMapper.INSTANCE.toSysDept(this.getDto(), (SysDept) entity);
    }

}