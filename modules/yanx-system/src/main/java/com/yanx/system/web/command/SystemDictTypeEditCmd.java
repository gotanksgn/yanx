package com.yanx.system.web.command;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.VoidCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.system.base.command.SysDictTypeUpdateCmd;
import com.yanx.system.base.domain.manager.SysDictTypeManager;
import com.yanx.system.base.model.dto.SysDictTypeDto;
import lombok.AllArgsConstructor;

/**
 * 字典 修改命令
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
@AllArgsConstructor
public class SystemDictTypeEditCmd extends VoidCommand {
    private SysDictTypeDto dictType;

    @Override
    public void handler(Executor executor) {
        SysDictTypeManager sysDictTypeManager = executor.getReceiver(SysDictTypeManager.class);
        if (sysDictTypeManager.checkDictTypeUnique(dictType.getDictType(), dictType.getId())) {
            throw new BusinessException("修改字典'{}'失败，字典类型已存在", dictType.getDictType());
        }

//        dictType.setUpdateBy(getDictTypename());
        executor.execute(new SysDictTypeUpdateCmd(dictType.getId(), dictType));
    }
}