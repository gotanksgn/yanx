package com.yanx.system.base.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yanx.common.core.dto.BaseDto;
import com.yanx.common.enums.DataStatus;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 字典类型DTO
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "字典类型入参模型")
public class SysDictTypeDto extends BaseDto<Long> {

    private static final long serialVersionUID = 1L;

    @Schema(name = "字典名称")
    private String dictName;

    @Schema(name = "字典类型")
    private String dictType;

    @Schema(name = "状态")
    private DataStatus status;

}
