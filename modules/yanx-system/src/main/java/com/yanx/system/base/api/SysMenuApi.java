package com.yanx.system.base.api;

import com.yanx.common.annotation.ApiResult;
import com.yanx.common.annotation.Pagination;
import com.yanx.common.core.api.BaseApi;
import com.yanx.system.base.command.SysMenuCreateCmd;
import com.yanx.system.base.command.SysMenuDeleteCmd;
import com.yanx.system.base.command.SysMenuUpdateCmd;
import com.yanx.system.base.model.dto.SysMenuDto;
import com.yanx.system.base.model.qo.SysMenuQo;
import com.yanx.system.base.model.vo.SysMenuVo;
import com.yanx.system.base.query.SysMenuByIdQry;
import com.yanx.system.base.query.SysMenuListQry;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

/**
 * 菜单权限 接口
 *
 * @author: gotanks
 * @create: 2023-4-12
 */
@Tag(name = "菜单权限", description = "菜单权限")
@RestController
@RequestMapping(value = "/api/sys-menu")
public class SysMenuApi extends BaseApi {

    @ApiResult
    @Operation(summary = "根据ID查询菜单权限")
    @GetMapping("/{id}")
    public SysMenuVo get(@PathVariable Long id) {
        return queryExecutor.execute(new SysMenuByIdQry(id));
    }

    @Pagination(total = true)
    @ApiResult
    @Operation(summary = "分页查询菜单权限")
    @GetMapping
    public List<SysMenuVo> getList(SysMenuQo sysMenuQo) {
        return queryExecutor.execute(new SysMenuListQry(sysMenuQo));
    }

    @ApiResult
    @Operation(summary = "新增菜单权限")
    @PostMapping
    public void create(@Valid @RequestBody SysMenuDto sysMenuDto) {
        commandExecutor.execute(new SysMenuCreateCmd(sysMenuDto));
    }

    @ApiResult
    @Operation(summary = "更新菜单权限")
    @PutMapping("/{id}")
    public void update(@PathVariable("id") Long id, @Valid @RequestBody SysMenuDto sysMenuDto) {
        commandExecutor.execute(new SysMenuUpdateCmd(id, sysMenuDto));
    }

    @ApiResult
    @Operation(summary = "删除菜单权限")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        commandExecutor.execute(new SysMenuDeleteCmd(id));
    }

    @ApiResult
    @Operation(summary = "批量删除菜单权限")
    @DeleteMapping
    public void batchDelete(@RequestBody Set<Long> ids) {
        commandExecutor.execute(new SysMenuDeleteCmd(ids));
    }
}