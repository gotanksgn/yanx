package com.yanx.system.base.query;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.yanx.common.command.Executor;
import com.yanx.common.core.command.PageQryCommand;
import com.yanx.system.base.domain.entity.QSysMenu;
import com.yanx.system.base.model.qo.SysMenuQo;
import com.yanx.system.base.model.vo.SysMenuVo;
import lombok.AllArgsConstructor;

import java.util.List;

/**
 * 菜单权限分页查询
 *
 * @author: gotanks
 * @create: 2023-4-12
 */
@AllArgsConstructor
public class SysMenuListQry extends PageQryCommand<List<SysMenuVo>> {

    private SysMenuQo sysMenuQo;

    @Override
    public List<SysMenuVo> execute(Executor executor) {
        QSysMenu sysMenu = QSysMenu.sysMenu;

        //查询条件
        BooleanBuilder condition = new BooleanBuilder();
        condition.and(sysMenu.deleted.eq(false));
        if (sysMenuQo != null) {
            //自定义查询条件
        }

        JPAQuery<SysMenuVo> query = queryFactory.select(SysMenuByIdQry.fields())
                .from(sysMenu)
                .where(condition);

        return this.pageList(query);
    }

}