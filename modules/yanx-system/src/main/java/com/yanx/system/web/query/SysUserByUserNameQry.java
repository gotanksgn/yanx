package com.yanx.system.web.query;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.PageQryCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.system.base.domain.entity.QSysUser;
import com.yanx.system.base.model.vo.SysUserVo;
import com.yanx.system.base.query.SysUserByIdQry;
import lombok.AllArgsConstructor;

/**
 * 根据用户账号查询用户信息
 *
 * @author: gotanks
 * @create: 2022-11-22
 */
@AllArgsConstructor
public class SysUserByUserNameQry extends PageQryCommand<SysUserVo> {

    private String userName;

    @Override
    public SysUserVo execute(Executor executor) {
        if (userName == null) {
            throw new BusinessException("用户账号不能为空");
        }
        QSysUser sysUser = QSysUser.sysUser;
        return queryFactory.select(SysUserByIdQry.fields())
                .from(sysUser)
                .where(sysUser.userName.eq(userName))
                .fetchOne();
    }

}