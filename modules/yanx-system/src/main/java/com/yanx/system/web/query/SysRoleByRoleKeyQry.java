package com.yanx.system.web.query;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.QryCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.system.base.domain.entity.QSysRole;
import com.yanx.system.base.model.vo.SysRoleVo;
import com.yanx.system.base.query.SysRoleByIdQry;
import lombok.AllArgsConstructor;

/**
 * 根据KEY查询菜单权限
 *
 * @author: gotanks
 * @create: 2023-6-5
 */
@AllArgsConstructor
public class SysRoleByRoleKeyQry extends QryCommand<SysRoleVo> {

    private String roleKey;

    @Override
    public SysRoleVo execute(Executor executor) {
        if (roleKey == null) {
            throw new BusinessException("菜单权限Key不能为空");
        }
        QSysRole sysRole = QSysRole.sysRole;
        return queryFactory.select(SysRoleByIdQry.fields())
                .from(sysRole)
                .where(sysRole.deleted.eq(false), sysRole.roleKey.eq(roleKey))
                .fetchOne();
    }

}