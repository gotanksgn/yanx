package com.yanx.system.web.infra.listener;

import com.yanx.system.base.domain.entity.SysConfig;
import com.yanx.system.base.domain.event.SysConfigDeleteEvent;
import com.yanx.system.base.domain.event.SysConfigSaveEvent;
import com.yanx.system.web.infra.cache.SysConfigCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 监听系统配置，更新缓存
 *
 * @author gotanks
 * @date 2023/06/07
 */
@Slf4j
@Component
public class SysConfigListener {

    @Async
    @TransactionalEventListener
    public void onSysConfigSave(SysConfigSaveEvent event) {
        SysConfig sysConfig = event.getSysConfig();
        String configKey = sysConfig.getConfigKey();
        String configValue = sysConfig.getConfigValue();
        log.info("更新系统配置缓存->{}->{}", configKey, configValue);
        SysConfigCache.INSTANCE.set(new String[]{configKey}, configValue);
    }

    @Async
    @TransactionalEventListener
    public void onSysConfigDelete(SysConfigDeleteEvent event) {
        SysConfig sysConfig = event.getSysConfig();
        String configKey = sysConfig.getConfigKey();
        log.info("删除系统配置缓存->{}", configKey);
        SysConfigCache.INSTANCE.delete(configKey);
    }
}
