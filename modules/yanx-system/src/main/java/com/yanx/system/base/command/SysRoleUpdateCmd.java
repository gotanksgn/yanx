package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.system.base.domain.entity.SysRole;
import com.yanx.system.base.infra.mapper.SysRoleMapper;
import com.yanx.system.base.model.dto.SysRoleDto;

/**
 * 菜单权限 修改命令
 *
 * @author: gotanks
 * @create: 2023-2-10
 */
public class SysRoleUpdateCmd extends CrudCommand<SysRoleDto> {

    /**
     * 修改
     *
     * @param id
    * @param sysRoleDto
     */
    public SysRoleUpdateCmd(Long id, SysRoleDto sysRoleDto) {
        super(id, sysRoleDto);
    }

    /**
     * 修改时实体类转换
     *
     * @param entity
     * @return
     */
    @Override
    public BaseEntity toEntity(BaseEntity entity) {
        return SysRoleMapper.INSTANCE.toSysRole(this.getDto(), (SysRole) entity);
    }

}