package com.yanx.system.web.command;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.VoidCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.system.base.command.SysRoleCreateCmd;
import com.yanx.system.base.domain.manager.SysRoleManager;
import com.yanx.system.base.model.dto.SysRoleDto;
import lombok.AllArgsConstructor;

/**
 * 角色信息 新增命令
 *
 * @author: gotanks
 * @create: 2023-5-22
 */
@AllArgsConstructor
public class SystemRoleAddCmd extends VoidCommand {
    private SysRoleDto role;

    @Override
    public void handler(Executor executor) {
        SysRoleManager sysRoleManager = executor.getReceiver(SysRoleManager.class);
        if (sysRoleManager.checkRoleNameUnique(role.getRoleName(), role.getId())) {
            throw new BusinessException("新增角色'{}'失败，角色名称已存在", role.getRoleName());
        }
        if (sysRoleManager.checkRoleKeyUnique(role.getRoleKey(), role.getId())) {
            throw new BusinessException("新增角色'{}'失败，角色权限已存在", role.getRoleName());
        }
        executor.execute(new SysRoleCreateCmd(role));
    }
}