package com.yanx.system.base.query;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.yanx.common.command.Executor;
import com.yanx.common.core.command.PageQryCommand;
import com.yanx.system.base.domain.entity.QSysDictType;
import com.yanx.system.base.model.qo.SysDictTypeQo;
import com.yanx.system.base.model.vo.SysDictTypeVo;
import lombok.AllArgsConstructor;

import java.util.List;

/**
 * 字典类型分页查询
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
@AllArgsConstructor
public class SysDictTypeListQry extends PageQryCommand<List<SysDictTypeVo>> {

    private SysDictTypeQo sysDictTypeQo;

    @Override
    public List<SysDictTypeVo> execute(Executor executor) {
        QSysDictType sysDictType = QSysDictType.sysDictType;

        //查询条件
        BooleanBuilder condition = new BooleanBuilder();
        condition.and(sysDictType.deleted.eq(false));
        if (sysDictTypeQo != null) {
            //自定义查询条件
            if (sysDictTypeQo.getDictType() != null && !sysDictTypeQo.getDictType().equals("")) {
                condition.and(sysDictType.dictType.contains(sysDictTypeQo.getDictType()));
            }
            if (sysDictTypeQo.getDictName() != null && !sysDictTypeQo.getDictName().equals("")) {
                condition.and(sysDictType.dictName.contains(sysDictTypeQo.getDictName()));
            }
            if (sysDictTypeQo.getStatus() != null && !sysDictTypeQo.getStatus().equals("")) {
                condition.and(sysDictType.status.eq(sysDictTypeQo.getStatus()));
            }
            if (sysDictTypeQo.getBeginTime() != null) {
                condition.and(sysDictType.createTime.gt(sysDictTypeQo.getBeginTime()));
            }
            if (sysDictTypeQo.getEndTime() != null) {
                condition.and(sysDictType.createTime.lt(sysDictTypeQo.getEndTime()));
            }
        }

        JPAQuery<SysDictTypeVo> query = queryFactory.select(SysDictTypeByIdQry.fields())
                .from(sysDictType)
                .where(condition);

        return this.pageList(query);
    }

}