package com.yanx.system.web.query;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.QryCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.system.base.domain.entity.QSysConfig;
import com.yanx.system.base.model.vo.SysConfigVo;
import com.yanx.system.base.query.SysConfigByIdQry;
import com.yanx.system.web.infra.cache.SysConfigCache;
import lombok.AllArgsConstructor;

/**
 * 根据ID查询参数配置
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@AllArgsConstructor
public class SysConfigByKeyQry extends QryCommand<String> {

    private String configKey;

    @Override
    public String execute(Executor executor) {
        if (configKey == null) {
            throw new BusinessException("参数配置key不能为空");
        }
        return SysConfigCache.INSTANCE.get(new String[]{configKey}, () -> {
            QSysConfig sysConfig = QSysConfig.sysConfig;
            SysConfigVo sysConfigVo1 = queryFactory.select(SysConfigByIdQry.fields())
                    .from(sysConfig)
                    .where(sysConfig.deleted.eq(false), sysConfig.configKey.eq(configKey))
                    .fetchOne();
            return sysConfigVo1.getConfigValue();
        });
    }

}