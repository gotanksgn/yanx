package com.yanx.system.base.api;

import com.yanx.common.annotation.ApiResult;
import com.yanx.common.annotation.Pagination;
import com.yanx.common.core.api.BaseApi;
import com.yanx.system.base.command.SysRoleCreateCmd;
import com.yanx.system.base.command.SysRoleDeleteCmd;
import com.yanx.system.base.command.SysRoleUpdateCmd;
import com.yanx.system.base.model.dto.SysRoleDto;
import com.yanx.system.base.model.qo.SysRoleQo;
import com.yanx.system.base.model.vo.SysRoleVo;
import com.yanx.system.base.query.SysRoleByIdQry;
import com.yanx.system.base.query.SysRoleListQry;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

/**
 * 菜单权限 接口
 *
 * @author: gotanks
 * @create: 2023-4-12
 */
@Tag(name = "菜单权限", description = "菜单权限")
@RestController
@RequestMapping(value = "/api/sys-role")
public class SysRoleApi extends BaseApi {

    @ApiResult
    @Operation(summary = "根据ID查询菜单权限")
    @GetMapping("/{id}")
    public SysRoleVo get(@PathVariable Long id) {
        return queryExecutor.execute(new SysRoleByIdQry(id));
    }

    @Pagination(total = true)
    @ApiResult
    @Operation(summary = "分页查询菜单权限")
    @GetMapping
    public List<SysRoleVo> getList(SysRoleQo sysRoleQo) {
        return queryExecutor.execute(new SysRoleListQry(sysRoleQo));
    }

    @ApiResult
    @Operation(summary = "新增菜单权限")
    @PostMapping
    public void create(@Valid @RequestBody SysRoleDto sysRoleDto) {
        commandExecutor.execute(new SysRoleCreateCmd(sysRoleDto));
    }

    @ApiResult
    @Operation(summary = "更新菜单权限")
    @PutMapping("/{id}")
    public void update(@PathVariable("id") Long id, @Valid @RequestBody SysRoleDto sysRoleDto) {
        commandExecutor.execute(new SysRoleUpdateCmd(id, sysRoleDto));
    }

    @ApiResult
    @Operation(summary = "删除菜单权限")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        commandExecutor.execute(new SysRoleDeleteCmd(id));
    }

    @ApiResult
    @Operation(summary = "批量删除菜单权限")
    @DeleteMapping
    public void batchDelete(@RequestBody Set<Long> ids) {
        commandExecutor.execute(new SysRoleDeleteCmd(ids));
    }
}