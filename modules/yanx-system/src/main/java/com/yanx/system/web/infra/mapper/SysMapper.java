//package com.yanx.system.web.infra.mapper;
//
//import com.yanx.system.base.domain.entity.SysUser;
//import com.yanx.system.client.dto.LoginUserVo;
//import org.mapstruct.*;
//import org.mapstruct.factory.Mappers;
//
///**
// * 用户 实体转换类
// *
// * @author: gotanks
// * @create: 2022-11-22
// */
//@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE,
//        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
//public interface SysMapper {
//
//    SysMapper INSTANCE = Mappers.getMapper(SysMapper.class);
//
//    @Mapping(target = "username", source = "userName")
//    @Mapping(target = "fullname", source = "nickName")
//    LoginUserVo toLoginUser(SysUser sysUser);
//
//    @AfterMapping
//    static void afterLoginUser(SysUser sysUser, @MappingTarget LoginUserVo loginUserVo) {
//        String status = sysUser.getStatus();
//        if ("1".equals(status)) {
//            loginUserVo.setDisabled(true);
//        }
//        if ("2".equals(status)) {
//            loginUserVo.setLocked(true);
//        }
//    }
//}
