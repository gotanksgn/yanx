package com.yanx.system.base.query;

import com.querydsl.core.types.Projections;
import com.querydsl.core.types.QBean;
import com.yanx.common.command.Executor;
import com.yanx.common.core.command.QryCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.system.base.domain.entity.QSysUser;
import com.yanx.system.base.model.vo.SysUserVo;
import lombok.AllArgsConstructor;

/**
 * 根据ID查询用户
 *
 * @author: gotanks
 * @create: 2023-4-12
 */
@AllArgsConstructor
public class SysUserByIdQry extends QryCommand<SysUserVo> {

    private Long id;

    @Override
    public SysUserVo execute(Executor executor) {
        if (id == null) {
            throw new BusinessException("用户ID不能为空");
        }
        QSysUser sysUser = QSysUser.sysUser;
        return queryFactory.select(this.fields())
                .from(sysUser)
                .where(sysUser.deleted.eq(false), sysUser.id.eq(id))
                .fetchOne();
    }

    /**
     * 用户VO映射
     *
     * @return QBean<SysUserVo>
     */
    public static QBean<SysUserVo> fields() {
        QSysUser sysUser = QSysUser.sysUser;
        return Projections.fields(
                SysUserVo.class,
                sysUser.userName,
                sysUser.nickName,
                sysUser.password,
                sysUser.userType,
                sysUser.status,
                sysUser.email,
                sysUser.phonenumber,
                sysUser.avatar,
                sysUser.sex,
                sysUser.loginIp,
                sysUser.loginDate,
                sysUser.deptId,
                sysUser.createTime,
                sysUser.id
        );
    }
}