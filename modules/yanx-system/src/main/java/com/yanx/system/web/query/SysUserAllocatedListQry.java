package com.yanx.system.web.query;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.QBean;
import com.querydsl.jpa.impl.JPAQuery;
import com.yanx.common.command.Executor;
import com.yanx.common.core.command.PageQryCommand;
import com.yanx.common.utils.String_;
import com.yanx.system.base.domain.entity.QSysDept;
import com.yanx.system.base.domain.entity.QSysRole;
import com.yanx.system.base.domain.entity.QSysUser;
import com.yanx.system.base.domain.entity.rel.QSysUserRole;
import com.yanx.system.base.model.qo.SysUserRoleQo;
import com.yanx.system.base.model.vo.SysUserVo;
import com.yanx.system.web.infra.utils.DataScopeUtil;
import lombok.AllArgsConstructor;

import java.util.List;

/**
 * 用户分页查询
 *
 * @author: gotanks
 * @create: 2023-2-6
 */
@AllArgsConstructor
public class SysUserAllocatedListQry extends PageQryCommand<List<SysUserVo>> {

    private SysUserRoleQo sysUserRoleQo;

    @Override
    public List<SysUserVo> execute(Executor executor) {
        QSysUser sysUser = QSysUser.sysUser;
        QSysDept sysDept = QSysDept.sysDept;
        QSysUserRole sysUserRole = QSysUserRole.sysUserRole;
        QSysRole sysRole = QSysRole.sysRole;

        //查询条件
        BooleanBuilder condition = DataScopeUtil.getUserScopeCondition();//数据范围权限
        condition.and(sysUser.deleted.eq(false));
        if (sysUserRoleQo != null) {
            //自定义查询条件
            condition.and(sysRole.id.eq(sysUserRoleQo.getRoleId()));
            if (String_.isNotBlank(sysUserRoleQo.getUserName())) {
                condition.and(sysUser.userName.eq(sysUserRoleQo.getUserName()));
            }
            if (String_.isNotBlank(sysUserRoleQo.getPhonenumber())) {
                condition.and(sysUser.phonenumber.eq(sysUserRoleQo.getPhonenumber()));
            }
        }

        JPAQuery<SysUserVo> query = queryFactory.select(fields())
                .from(sysUser)
                .leftJoin(sysDept).on(sysDept.id.eq(sysUser.deptId))
                .leftJoin(sysUserRole).on(sysUserRole.userId.eq(sysUser.id))
                .leftJoin(sysRole).on(sysRole.id.eq(sysUserRole.roleId))
                .where(condition);

        return this.pageList(query);
    }

    /**
     * 用户VO映射
     *
     * @return QBean<SysUserVo>
     */
    public static QBean<SysUserVo> fields() {
        QSysUser sysUser = QSysUser.sysUser;
        return Projections.fields(
                SysUserVo.class,
                sysUser.userName,
                sysUser.nickName,
                sysUser.userType,
                sysUser.status,
                sysUser.email,
                sysUser.phonenumber,
                sysUser.avatar,
                sysUser.sex,
                sysUser.loginIp,
                sysUser.loginDate,
                sysUser.deptId,
                sysUser.createTime,
                sysUser.id
        );
    }

}