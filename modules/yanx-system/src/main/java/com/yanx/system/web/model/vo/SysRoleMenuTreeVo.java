package com.yanx.system.web.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * 菜单权限树模型
 *
 * @author: gotanks
 * @create: 2023-2-9
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "菜单权限树模型")
public class SysRoleMenuTreeVo implements Serializable {

    private static final long serialVersionUID = 1L;
    private List<Long> checkedKeys;//菜单树信息
    private List<SysMenuTreeVo> menus;
}
