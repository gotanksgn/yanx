package com.yanx.system.base.domain.event;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SysDictDeleteEvent {

    private String dictType;

}
