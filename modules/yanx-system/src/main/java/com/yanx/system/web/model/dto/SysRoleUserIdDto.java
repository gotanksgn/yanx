package com.yanx.system.web.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;


/**
 * 菜单用户ID DTO
 *
 * @author: gotanks
 * @create: 2023-2-10
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "菜单用户ID")
public class SysRoleUserIdDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(name = "角色ID")
    private Long roleId;

    @Schema(name = "用户ID")
    private Long userId;

}
