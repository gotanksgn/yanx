package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.system.base.domain.entity.SysUser;
import com.yanx.system.base.infra.mapper.SysUserMapper;
import com.yanx.system.base.model.dto.SysUserDto;

/**
 * 用户 修改命令
 *
 * @author: gotanks
 * @create: 2023-2-6
 */
public class SysUserUpdateCmd extends CrudCommand<SysUserDto> {

    /**
     * 修改
     *
     * @param id
    * @param sysUserDto
     */
    public SysUserUpdateCmd(Long id, SysUserDto sysUserDto) {
        super(id, sysUserDto);
    }

    /**
     * 修改时实体类转换
     *
     * @param entity
     * @return
     */
    @Override
    public BaseEntity toEntity(BaseEntity entity) {
        return SysUserMapper.INSTANCE.toSysUser(this.getDto(), (SysUser) entity);
    }

}