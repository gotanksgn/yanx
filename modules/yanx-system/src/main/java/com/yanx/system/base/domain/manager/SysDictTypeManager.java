package com.yanx.system.base.domain.manager;

import com.yanx.common.core.domain.manager.BaseManager;
import com.yanx.system.base.domain.entity.SysDictType;
import com.yanx.system.base.domain.repository.SysDictTypeRepository;
import com.yanx.system.web.infra.cache.SysDictCache;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * 字典类型领域服务
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
@Service
public class SysDictTypeManager extends BaseManager<SysDictType, SysDictTypeRepository> {

    /**
     * 项目启动时，初始化数据字典到缓存
     */
    @PostConstruct
    public void init() {
        loadingDictCache();
    }

    public boolean checkDictTypeUnique(String dictType) {
        int num = repository.countByDictType(dictType);
        return num > 0;
    }

    public boolean checkDictTypeUnique(String dictType, Long id) {
        int num = repository.countByDictTypeAndIdNot(dictType, id);
        return num > 0;
    }

    public void resetDictCache() {
        clearDictCache();
        loadingDictCache();
    }

    private void loadingDictCache() {
        SysDictCache.INSTANCE.loadingAll();
    }

    private void clearDictCache() {
        SysDictCache.INSTANCE.deleteAll();
    }

}
