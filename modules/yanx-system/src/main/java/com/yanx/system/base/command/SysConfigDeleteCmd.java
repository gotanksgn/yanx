package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.system.base.domain.entity.SysConfig;
import com.yanx.system.base.infra.mapper.SysConfigMapper;
import com.yanx.system.base.model.dto.SysConfigDto;

import java.util.Collection;

/**
 * 参数配置 删除命令
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
public class SysConfigDeleteCmd extends CrudCommand<SysConfigDto> {

    /**
     * 删除
     *
     * @param id
     */
    public SysConfigDeleteCmd(Long id) {
        super(id);
    }

    /**
     * 批量删除
     *
     * @param ids
     */
    public SysConfigDeleteCmd(Collection<Long> ids) {
        super(ids);
    }

}