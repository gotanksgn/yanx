package com.yanx.system.web.api;

import com.yanx.common.annotation.ApiResult;
import com.yanx.common.annotation.OperationLog;
import com.yanx.common.core.api.BaseApi;
import com.yanx.common.enums.BusinessType;
import com.yanx.common.security.preauthorize.PreAuthorize;
import com.yanx.system.base.model.dto.SysMenuDto;
import com.yanx.system.base.model.qo.SysMenuQo;
import com.yanx.system.base.model.vo.SysMenuVo;
import com.yanx.system.base.query.SysMenuByIdQry;
import com.yanx.system.web.command.SystemMenuAddCmd;
import com.yanx.system.web.command.SystemMenuEditCmd;
import com.yanx.system.web.command.SystemMenuRemoveCmd;
import com.yanx.system.web.model.vo.SysMenuTreeVo;
import com.yanx.system.web.model.vo.SysRoleMenuTreeVo;
import com.yanx.system.web.query.SysMenuIdListByRoleIdQry;
import com.yanx.system.web.query.SysMenuListByUserIdQry;
import com.yanx.system.web.query.SysMenuTreeListQry;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 菜单权限 接口
 *
 * @author: gotanks
 * @create: 2023-2-9
 */
@Tag(name = "菜单权限", description = "菜单权限")
@RestController
@RequestMapping(value = "/system/menu")
public class SystemMenuApi extends BaseApi {


    @PreAuthorize(hasPermi = "system:menu:list")
    @ApiResult
    @Operation(summary = "获取菜单列表")
    @GetMapping("/list")
    public List<SysMenuVo> list(SysMenuQo sysMenuQo) {
        return queryExecutor.execute(new SysMenuListByUserIdQry(sysMenuQo));
    }

    @PreAuthorize(hasPermi = "system:menu:query")
    @ApiResult
    @Operation(summary = "根据菜单编号获取详细信息")
    @GetMapping(value = "/{id}")
    public SysMenuVo getInfo(@PathVariable Long id) {
        return queryExecutor.execute(new SysMenuByIdQry(id));
    }

    @ApiResult
    @Operation(summary = "获取菜单下拉树列表")
    @GetMapping("/treeselect")
    public List<SysMenuTreeVo> treeselect(SysMenuQo sysMenuQo) {
        return queryExecutor.execute(new SysMenuTreeListQry(sysMenuQo));
    }

    @ApiResult
    @Operation(summary = "加载对应角色菜单列表树")
    @GetMapping(value = "/roleMenuTreeselect/{roleId}")
    public SysRoleMenuTreeVo roleMenuTreeselect(@PathVariable("roleId") Long roleId) {
        SysRoleMenuTreeVo sysRoleMenuTreeVo = new SysRoleMenuTreeVo();
        sysRoleMenuTreeVo.setMenus(queryExecutor.execute(new SysMenuTreeListQry()));
        sysRoleMenuTreeVo.setCheckedKeys(queryExecutor.execute(new SysMenuIdListByRoleIdQry(roleId)));
        return sysRoleMenuTreeVo;
    }

    /**
     * 新增菜单
     */
    @PreAuthorize(hasPermi = "system:menu:add")
    @ApiResult
    @Operation(summary = "新增菜单")
    @PostMapping
    @OperationLog(title = "菜单管理", businessType = BusinessType.INSERT)
    public void add(@Validated @RequestBody SysMenuDto menu) {
        commandExecutor.execute(new SystemMenuAddCmd(menu));
    }

    /**
     * 修改菜单
     */
    @PreAuthorize(hasPermi = "system:menu:edit")
    @ApiResult
    @Operation(summary = "修改菜单")
    @PutMapping
    @OperationLog(title = "菜单管理", businessType = BusinessType.UPDATE)
    public void edit(@Validated @RequestBody SysMenuDto menu) {
        commandExecutor.execute(new SystemMenuEditCmd(menu));
    }

    /**
     * 删除菜单
     */
    @PreAuthorize(hasPermi = "system:menu:remove")
    @ApiResult
    @Operation(summary = "删除菜单")
    @DeleteMapping("/{menuId}")
    @OperationLog(title = "菜单管理", businessType = BusinessType.DELETE)
    public void remove(@PathVariable("menuId") Long menuId) {
        commandExecutor.execute(new SystemMenuRemoveCmd(menuId));
    }
}