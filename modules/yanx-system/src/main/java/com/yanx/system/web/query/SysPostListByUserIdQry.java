package com.yanx.system.web.query;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.yanx.common.command.Executor;
import com.yanx.common.core.command.PageQryCommand;
import com.yanx.system.base.domain.entity.QSysPost;
import com.yanx.system.base.domain.entity.rel.QSysUserPost;
import com.yanx.system.base.model.vo.SysPostVo;
import com.yanx.system.base.query.SysPostByIdQry;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 根据用户查询岗位集合
 *
 * @author: gotanks
 * @create: 2023-6-7
 */
@NoArgsConstructor
@AllArgsConstructor
public class SysPostListByUserIdQry extends PageQryCommand<List<SysPostVo>> {

    private Long userId;

    @Override
    public List<SysPostVo> execute(Executor executor) {
        QSysPost sysPost = QSysPost.sysPost;
        QSysUserPost sysUserPost = QSysUserPost.sysUserPost;

        //查询条件
        BooleanBuilder condition = new BooleanBuilder();
        condition.and(sysPost.deleted.eq(false));
        JPAQuery<SysPostVo> query = queryFactory.select(SysPostByIdQry.fields())
                .from(sysPost)
                .leftJoin(sysUserPost).on(sysUserPost.postId.eq(sysPost.id))
                .where(condition, sysUserPost.userId.eq(userId));

        return this.pageList(query);
    }

}