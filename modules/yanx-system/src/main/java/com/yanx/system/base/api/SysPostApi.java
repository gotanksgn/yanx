package com.yanx.system.base.api;

import com.yanx.common.annotation.ApiResult;
import com.yanx.common.annotation.Pagination;
import com.yanx.common.core.api.BaseApi;
import com.yanx.system.base.command.SysPostCreateCmd;
import com.yanx.system.base.command.SysPostDeleteCmd;
import com.yanx.system.base.command.SysPostUpdateCmd;
import com.yanx.system.base.model.dto.SysPostDto;
import com.yanx.system.base.model.qo.SysPostQo;
import com.yanx.system.base.model.vo.SysPostVo;
import com.yanx.system.base.query.SysPostByIdQry;
import com.yanx.system.base.query.SysPostListQry;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

/**
 * 岗位 接口
 *
 * @author: gotanks
 * @create: 2023-5-23
 */
@Tag(name = "岗位", description = "岗位")
@RestController
@RequestMapping(value = "/api/sys-post")
public class SysPostApi extends BaseApi {

    @ApiResult
    @Operation(summary = "根据ID查询岗位")
    @GetMapping("/{id}")
    public SysPostVo get(@PathVariable Long id) {
        return queryExecutor.execute(new SysPostByIdQry(id));
    }

    @Pagination(total = true)
    @ApiResult
    @Operation(summary = "分页查询岗位")
    @GetMapping
    public List<SysPostVo> getList(SysPostQo sysPostQo) {
        return queryExecutor.execute(new SysPostListQry(sysPostQo));
    }

    @ApiResult
    @Operation(summary = "新增岗位")
    @PostMapping
    public void create(@Valid @RequestBody SysPostDto sysPostDto) {
        commandExecutor.execute(new SysPostCreateCmd(sysPostDto));
    }

    @ApiResult
    @Operation(summary = "更新岗位")
    @PutMapping("/{id}")
    public void update(@PathVariable("id") Long id, @Valid @RequestBody SysPostDto sysPostDto) {
        commandExecutor.execute(new SysPostUpdateCmd(id, sysPostDto));
    }

    @ApiResult
    @Operation(summary = "删除岗位")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        commandExecutor.execute(new SysPostDeleteCmd(id));
    }

    @ApiResult
    @Operation(summary = "批量删除岗位")
    @DeleteMapping
    public void batchDelete(@RequestBody Set<Long> ids) {
        commandExecutor.execute(new SysPostDeleteCmd(ids));
    }
}