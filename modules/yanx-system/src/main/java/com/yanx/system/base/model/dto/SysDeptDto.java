package com.yanx.system.base.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yanx.common.core.dto.BaseDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 部门DTO
 *
 * @author: gotanks
 * @create: 2023-2-8
 */
@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "部门入参模型")
public class SysDeptDto extends BaseDto<Long> {

    private static final long serialVersionUID = 1L;

    @Schema(name = "祖先节点")
    private String ancestors;
    
    @Schema(name = "父部门ID")
    private Long parentId;

    @Schema(name = "部门名称")
    private String deptName;

    @Schema(name = "显示顺序")
    private Integer orderNum;

    @Schema(name = "负责人")
    private String leader;

    @Schema(name = "联系电话")
    private String phone;

    @Schema(name = "邮箱")
    private String email;

    @Schema(name = "部门状态")
    private String status;

}
