package com.yanx.system.web.command;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.VoidCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.system.base.command.SysConfigCreateCmd;
import com.yanx.system.base.domain.manager.SysConfigManager;
import com.yanx.system.base.model.dto.SysConfigDto;
import lombok.AllArgsConstructor;

/**
 * 系统配置 新增命令
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@AllArgsConstructor
public class SystemConfigAddCmd extends VoidCommand {
    private SysConfigDto config;

    @Override
    public void handler(Executor executor) {
        SysConfigManager sysConfigManager = executor.getReceiver(SysConfigManager.class);
        if (sysConfigManager.checkConfigKeyUnique(config.getConfigKey())) {
            throw new BusinessException("新增参数'{}'失败，参数键名已存在", config.getConfigKey());
        }

//        config.setCreateBy(getConfigname());
        executor.execute(new SysConfigCreateCmd(config));
    }
}