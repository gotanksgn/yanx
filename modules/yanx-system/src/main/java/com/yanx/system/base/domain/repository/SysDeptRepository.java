package com.yanx.system.base.domain.repository;

import com.yanx.common.core.domain.repository.BaseRepository;
import com.yanx.system.base.domain.entity.SysDept;
import org.springframework.data.jpa.repository.Query;

import java.io.Serializable;
import java.util.List;

/**
 * 部门仓库
 *
 * @author: gotanks
 * @create: 2023-2-8
 */
public interface SysDeptRepository extends BaseRepository<SysDept, Serializable> {

    int countByDeptNameAndParentId(String deptName, Long parentId);

    int countByDeptNameAndParentIdAndIdNot(String deptName, Long parentId, Long deptId);

    @Query("select count(t) from SysDept t where t.deleted = false and t.status = '0' and find_in_set(?1, t.ancestors) > 0")
    int countNormalChildren(Long deptId);

    @Query("select count(t) from SysDept t where t.deleted = false and find_in_set(?1, t.ancestors) > 0")
    int countChildren(Long deptId);

    @Query("select count(t) from SysUser t where t.deleted = false and t.deptId = ?1")
    int checkDeptExistUser(Long deptId);

    List<SysDept> findAllByParentId(Long deptId);

    List<SysDept> findAllByIdIn(List<Long> parentDeptIds);
}
