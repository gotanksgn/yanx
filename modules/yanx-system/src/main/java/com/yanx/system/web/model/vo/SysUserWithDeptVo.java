package com.yanx.system.web.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yanx.system.base.model.vo.SysDeptVo;
import com.yanx.system.base.model.vo.SysUserVo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户VO
 *
 * @author: gotanks
 * @create: 2023-2-6
 */
@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "用户视图模型")
public class SysUserWithDeptVo extends SysUserVo {

    private static final long serialVersionUID = 1L;

    @Schema(name = "部门")
    private SysDeptVo dept = new SysDeptVo();

}
