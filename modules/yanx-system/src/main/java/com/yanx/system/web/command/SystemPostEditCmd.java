package com.yanx.system.web.command;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.VoidCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.system.base.command.SysPostUpdateCmd;
import com.yanx.system.base.domain.manager.SysPostManager;
import com.yanx.system.base.model.dto.SysPostDto;
import lombok.AllArgsConstructor;

/**
 * 岗位信息 修改命令
 *
 * @author: gotanks
 * @create: 2023-5-23
 */
@AllArgsConstructor
public class SystemPostEditCmd extends VoidCommand {
    private SysPostDto post;

    @Override
    public void handler(Executor executor) {
        SysPostManager sysPostManager = executor.getReceiver(SysPostManager.class);
        if (sysPostManager.checkPostNameUnique(post.getPostName(), post.getId())) {
            throw new BusinessException("修改岗位'{}'失败，岗位名称已存在", post.getPostName());
        }
        if (sysPostManager.checkPostCodeUnique(post.getPostCode(), post.getId())) {
            throw new BusinessException("修改岗位'{}'失败，岗位编码已存在", post.getPostName());
        }

//        post.setUpdateBy(getPostname());
        executor.execute(new SysPostUpdateCmd(post.getId(), post));
    }
}