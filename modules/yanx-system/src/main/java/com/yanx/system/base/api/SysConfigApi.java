package com.yanx.system.base.api;

import com.yanx.common.annotation.ApiResult;
import com.yanx.common.annotation.Pagination;
import com.yanx.common.core.api.BaseApi;
import com.yanx.system.base.command.SysConfigCreateCmd;
import com.yanx.system.base.command.SysConfigDeleteCmd;
import com.yanx.system.base.command.SysConfigUpdateCmd;
import com.yanx.system.base.model.dto.SysConfigDto;
import com.yanx.system.base.model.qo.SysConfigQo;
import com.yanx.system.base.model.vo.SysConfigVo;
import com.yanx.system.base.query.SysConfigByIdQry;
import com.yanx.system.base.query.SysConfigListQry;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

/**
 * 参数配置 接口
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@Tag(name = "参数配置", description = "参数配置")
@RestController
@RequestMapping(value = "/api/sys-config")
public class SysConfigApi extends BaseApi {

    @ApiResult
    @Operation(summary = "根据ID查询参数配置")
    @GetMapping("/{id}")
    public SysConfigVo get(@PathVariable Long id) {
        return queryExecutor.execute(new SysConfigByIdQry(id));
    }

    @Pagination(total = true)
    @ApiResult
    @Operation(summary = "分页查询参数配置")
    @GetMapping
    public List<SysConfigVo> getList(SysConfigQo sysConfigQo) {
        return queryExecutor.execute(new SysConfigListQry(sysConfigQo));
    }

    @ApiResult
    @Operation(summary = "新增参数配置")
    @PostMapping
    public void create(@Valid @RequestBody SysConfigDto sysConfigDto) {
        commandExecutor.execute(new SysConfigCreateCmd(sysConfigDto));
    }

    @ApiResult
    @Operation(summary = "更新参数配置")
    @PutMapping("/{id}")
    public void update(@PathVariable("id") Long id, @Valid @RequestBody SysConfigDto sysConfigDto) {
        commandExecutor.execute(new SysConfigUpdateCmd(id, sysConfigDto));
    }

    @ApiResult
    @Operation(summary = "删除参数配置")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        commandExecutor.execute(new SysConfigDeleteCmd(id));
    }

    @ApiResult
    @Operation(summary = "批量删除参数配置")
    @DeleteMapping
    public void batchDelete(@RequestBody Set<Long> ids) {
        commandExecutor.execute(new SysConfigDeleteCmd(ids));
    }
}