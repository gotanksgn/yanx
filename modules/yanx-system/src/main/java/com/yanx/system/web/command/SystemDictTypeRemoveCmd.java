package com.yanx.system.web.command;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.VoidCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.common.utils.Collect_;
import com.yanx.system.base.command.SysDictTypeDeleteCmd;
import com.yanx.system.base.domain.manager.SysDictTypeManager;
import com.yanx.system.base.model.vo.SysDictDataVo;
import com.yanx.system.web.query.SysDictDataListByTypeCacheQry;
import lombok.AllArgsConstructor;

import java.util.List;

/**
 * 字典 删除命令
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
@AllArgsConstructor
public class SystemDictTypeRemoveCmd extends VoidCommand {

    private Long[] dictTypeIds;

    @Override
    public void handler(Executor executor) {
        SysDictTypeManager sysDictTypeManager = executor.getReceiver(SysDictTypeManager.class);
        for (Long dictTypeId : dictTypeIds) {
            sysDictTypeManager.findById(dictTypeId).ifPresent(dictType -> {
                List<SysDictDataVo> sysDictDataVos = executor.execute(new SysDictDataListByTypeCacheQry(dictType.getDictType()));
                if (Collect_.isNotEmpty(sysDictDataVos)) {
                    throw new BusinessException("{}已分配,不能删除", dictType.getDictName());
                }
                executor.execute(new SysDictTypeDeleteCmd(dictTypeId));
            });
        }
    }
}