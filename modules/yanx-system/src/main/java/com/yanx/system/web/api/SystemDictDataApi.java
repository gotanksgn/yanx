package com.yanx.system.web.api;

import com.yanx.common.annotation.ApiResult;
import com.yanx.common.annotation.OperationLog;
import com.yanx.common.annotation.Pagination;
import com.yanx.common.core.api.BaseApi;
import com.yanx.common.enums.BusinessType;
import com.yanx.common.security.preauthorize.PreAuthorize;
import com.yanx.system.base.command.SysDictDataCreateCmd;
import com.yanx.system.base.command.SysDictDataDeleteCmd;
import com.yanx.system.base.command.SysDictDataUpdateCmd;
import com.yanx.system.base.model.dto.SysDictDataDto;
import com.yanx.system.base.model.qo.SysDictDataQo;
import com.yanx.system.base.model.vo.SysDictDataVo;
import com.yanx.system.base.query.SysDictDataByIdQry;
import com.yanx.system.base.query.SysDictDataListQry;
import com.yanx.system.web.query.SysDictDataListByTypeCacheQry;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 数据字典 接口
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
@Tag(name = "数据字典", description = "数据字典")
@RestController
@RequestMapping(value = "/system/dict/data")
public class SystemDictDataApi extends BaseApi {

    @PreAuthorize(hasPermi = "system:dict:list")
    @Pagination(total = true)
    @ApiResult
    @Operation(summary = "获取数据字典列表")
    @GetMapping("/list")
    public List<SysDictDataVo> list(SysDictDataQo dictData) {
        return queryExecutor.execute(new SysDictDataListQry(dictData));
    }

//    @OperationLog(title = "字典类型", businessType = BusinessType.EXPORT)
//    @PreAuthorize(hasPermi = "system:dict:export")
//    @ApiResult
//    @Operation(summary = "查询部门列表（排除节点）")
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, SysDictData dictData) {
//        List<SysDictData> list = dictDataService.selectDictDataList(dictData);
//        ExcelUtil<SysDictData> util = new ExcelUtil<SysDictData>(SysDictData.class);
//        util.exportExcel(response, list, "字典类型");
//    }

    @PreAuthorize(hasPermi = "system:dict:query")
    @ApiResult
    @Operation(summary = "查询字典数据详细")
    @GetMapping(value = "/{dictCode}")
    public SysDictDataVo getInfo(@PathVariable Long dictCode) {
        return queryExecutor.execute(new SysDictDataByIdQry(dictCode));
    }

    @ApiResult
    @Operation(summary = "根据字典类型查询字典数据信息")
    @GetMapping(value = "/type/{dictType}")
    public List<SysDictDataVo> dictType(@PathVariable String dictType) {
        return queryExecutor.execute(new SysDictDataListByTypeCacheQry(dictType));
    }

    /**
     * 新增字典类型
     */
    @PreAuthorize(hasPermi = "system:dict:add")
    @OperationLog(title = "字典类型", businessType = BusinessType.INSERT)
    @ApiResult
    @Operation(summary = "新增字典类型")
    @PostMapping
    public void add(@Validated @RequestBody SysDictDataDto dict) {
        commandExecutor.execute(new SysDictDataCreateCmd(dict));
    }

    @PreAuthorize(hasPermi = "system:dict:edit")
    @OperationLog(title = "字典类型", businessType = BusinessType.UPDATE)
    @ApiResult
    @Operation(summary = "修改字典类型")
    @PutMapping
    public void edit(@Validated @RequestBody SysDictDataDto dict) {
        commandExecutor.execute(new SysDictDataUpdateCmd(dict.getId(), dict));
    }

    @PreAuthorize(hasPermi = "system:dict:remove")
    @OperationLog(title = "字典类型", businessType = BusinessType.DELETE)
    @ApiResult
    @Operation(summary = "删除字典类型")
    @DeleteMapping("/{dictCodes}")
    public void remove(@PathVariable Long[] dictCodes) {
        commandExecutor.execute(new SysDictDataDeleteCmd(Arrays.asList(dictCodes)));
    }

}