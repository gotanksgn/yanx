package com.yanx.system.web.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yanx.common.core.vo.BaseTreeVo;
import com.yanx.system.base.model.vo.SysMenuVo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.stream.Collectors;


/**
 * 菜单权限树模型
 *
 * @author: gotanks
 * @create: 2023-2-9
 */
@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "菜单权限树模型")
public class SysMenuTreeVo extends BaseTreeVo<Long> {

    private static final long serialVersionUID = 1L;

    /**
     * 节点名称
     */
    private String label;
    
    public SysMenuTreeVo(SysMenuVo menu) {
        this.setId(menu.getId());
        this.setLabel(menu.getMenuName());
        this.setChildren(menu.getChildren().stream().map(t -> new SysMenuTreeVo((SysMenuVo) t)).collect(Collectors.toList()));

    }

}
