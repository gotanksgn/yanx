package com.yanx.system.base.domain.entity.rel;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 用户和角色关联 sys_user_role
 *
 * @author gotanks
 */
@Data
@Entity
@Table(name = "SYS_USER_ROLE")
@IdClass(value = SysUserRole.class)
public class SysUserRole implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 用户ID
     */
    @Id
    @JoinColumn(foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    private Long userId;

    /**
     * 角色ID
     */
    @Id
    @JoinColumn(foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    private Long roleId;

}
