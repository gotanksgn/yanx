package com.yanx.system.web.command;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.VoidCommand;
import com.yanx.system.base.domain.manager.SysRoleManager;
import lombok.AllArgsConstructor;

/**
 * 菜单权限 删除
 *
 * @author: gotanks
 * @create: 2023-5-23
 */
@AllArgsConstructor
public class SystemUserRoleRemoveCmd extends VoidCommand {
    private Long roleId;
    private Long[] userIds;

    @Override
    public void handler(Executor executor) {
        SysRoleManager sysRoleManager = executor.getReceiver(SysRoleManager.class);
        sysRoleManager.removeUserRoles(roleId, userIds);
    }
}