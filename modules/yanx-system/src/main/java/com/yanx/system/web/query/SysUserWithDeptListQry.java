package com.yanx.system.web.query;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.QBean;
import com.querydsl.jpa.impl.JPAQuery;
import com.yanx.common.command.Executor;
import com.yanx.common.core.command.PageQryCommand;
import com.yanx.system.base.domain.entity.QSysDept;
import com.yanx.system.base.domain.entity.QSysUser;
import com.yanx.system.base.model.qo.SysUserQo;
import com.yanx.system.base.model.vo.SysDeptVo;
import com.yanx.system.web.infra.utils.DataScopeUtil;
import com.yanx.system.web.model.vo.SysUserWithDeptVo;
import lombok.AllArgsConstructor;

import java.util.List;

/**
 * 用户分页查询
 *
 * @author: gotanks
 * @create: 2023-2-6
 */
@AllArgsConstructor
public class SysUserWithDeptListQry extends PageQryCommand<List<SysUserWithDeptVo>> {

    private SysUserQo sysUserQo;

    @Override
    public List<SysUserWithDeptVo> execute(Executor executor) {
        QSysUser sysUser = QSysUser.sysUser;
        QSysDept sysDept = QSysDept.sysDept;

        //查询条件
        BooleanBuilder condition = DataScopeUtil.getUserScopeCondition();//数据范围权限
        condition.and(sysUser.deleted.eq(false));
        if (sysUserQo != null) {
            //自定义查询条件
            if (sysUserQo.getUserId() != null && sysUserQo.getUserId() != 0) {
                condition.and(sysUser.id.eq(sysUserQo.getUserId()));
            }
            if (sysUserQo.getDeptId() != null && sysUserQo.getDeptId() != 0) {
                condition.and(sysUser.deptId.eq(sysUserQo.getDeptId()));
            }
            if (sysUserQo.getUserName() != null && !sysUserQo.getUserName().equals("")) {
                condition.and(sysUser.userName.contains(sysUserQo.getUserName()));
            }
            if (sysUserQo.getPhonenumber() != null && !sysUserQo.getPhonenumber().equals("")) {
                condition.and(sysUser.phonenumber.contains(sysUserQo.getPhonenumber()));
            }
            if (sysUserQo.getStatus() != null && !sysUserQo.getStatus().equals("")) {
                condition.and(sysUser.status.eq(sysUserQo.getStatus()));
            }
            if (sysUserQo.getBeginTime() != null) {
                condition.and(sysUser.createTime.gt(sysUserQo.getBeginTime()));
            }
            if (sysUserQo.getEndTime() != null) {
                condition.and(sysUser.createTime.lt(sysUserQo.getEndTime()));
            }
        }

        JPAQuery<SysUserWithDeptVo> query = queryFactory.select(fields())
                .from(sysUser)
                .leftJoin(sysDept).on(sysDept.id.eq(sysUser.deptId))
                .where(condition);

        return this.pageList(query);
    }

    /**
     * 用户VO映射
     *
     * @return QBean<SysUserVo>
     */
    public static QBean<SysUserWithDeptVo> fields() {
        QSysUser sysUser = QSysUser.sysUser;
        QSysDept sysDept = QSysDept.sysDept;
        return Projections.fields(
                SysUserWithDeptVo.class,
                sysUser.userName,
                sysUser.nickName,
                sysUser.userType,
                sysUser.status,
                sysUser.email,
                sysUser.phonenumber,
                sysUser.avatar,
                sysUser.sex,
                sysUser.loginIp,
                sysUser.loginDate,
                sysUser.deptId,
                sysUser.createTime,
                sysUser.id,
                Projections.bean(
                        SysDeptVo.class,
                        sysDept.deptName,
                        sysDept.leader
                ).as("dept")
        );
    }

}