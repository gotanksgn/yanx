package com.yanx.system.base.query;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.yanx.common.command.Executor;
import com.yanx.common.core.command.PageQryCommand;
import com.yanx.common.utils.String_;
import com.yanx.system.base.domain.entity.QSysPost;
import com.yanx.system.base.model.qo.SysPostQo;
import com.yanx.system.base.model.vo.SysPostVo;
import lombok.AllArgsConstructor;

import java.util.List;

/**
 * 岗位分页查询
 *
 * @author: gotanks
 * @create: 2023-5-23
 */
@AllArgsConstructor
public class SysPostListQry extends PageQryCommand<List<SysPostVo>> {

    private SysPostQo sysPostQo;

    @Override
    public List<SysPostVo> execute(Executor executor) {
        QSysPost sysPost = QSysPost.sysPost;

        //查询条件
        BooleanBuilder condition = new BooleanBuilder();
        condition.and(sysPost.deleted.eq(false));
        if (sysPostQo != null) {
            //自定义查询条件
            if (String_.isNotBlank(sysPostQo.getPostCode())) {
                condition.and(sysPost.postCode.contains(sysPostQo.getPostCode()));
            }
            if (String_.isNotBlank(sysPostQo.getPostName())) {
                condition.and(sysPost.postName.contains(sysPostQo.getPostName()));
            }
            if (String_.isNotBlank(sysPostQo.getStatus())) {
                condition.and(sysPost.status.eq(sysPostQo.getStatus()));
            }
        }

        JPAQuery<SysPostVo> query = queryFactory.select(SysPostByIdQry.fields())
                .from(sysPost)
                .where(condition);

        return this.pageList(query);
    }

}