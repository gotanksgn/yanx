package com.yanx.system.web.query;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.PageQryCommand;
import com.yanx.system.base.model.vo.SysDictDataVo;
import com.yanx.system.web.infra.cache.SysDictCache;
import lombok.AllArgsConstructor;

import java.util.List;

/**
 * 根据字典类型查询字典数据(缓存)
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
@AllArgsConstructor
public class SysDictDataListByTypeCacheQry extends PageQryCommand<List<SysDictDataVo>> {

    private String dictType;

    @Override
    public List<SysDictDataVo> execute(Executor executor) {
        return SysDictCache.INSTANCE.get(new String[]{dictType}, () -> executor.execute(new SysDictDataListByTypeQry(dictType)));
    }

}