package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.system.base.infra.mapper.SysPostMapper;
import com.yanx.system.base.model.dto.SysPostDto;

/**
 * 岗位 新增命令
 *
 * @author: gotanks
 * @create: 2023-5-23
 */
public class SysPostCreateCmd extends CrudCommand<SysPostDto> {

    /**
     * 新增
     *
     * @param sysPostDto
     */
    public SysPostCreateCmd(SysPostDto sysPostDto) {
        super(sysPostDto);
    }

    /**
     * 新增时实体类转换
     *
     * @return
     */
    @Override
    public BaseEntity toEntity() {
        return SysPostMapper.INSTANCE.toSysPost(this.getDto());
    }

}