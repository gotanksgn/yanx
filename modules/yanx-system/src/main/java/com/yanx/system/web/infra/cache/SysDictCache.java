package com.yanx.system.web.infra.cache;

import cn.hutool.extra.spring.SpringUtil;
import com.yanx.common.command.executor.QueryExecutor;
import com.yanx.common.utils.String_;
import com.yanx.framework.redis.RedisCacheManager;
import com.yanx.system.base.model.vo.SysDictDataVo;
import com.yanx.system.base.model.vo.SysDictTypeVo;
import com.yanx.system.base.query.SysDictTypeListQry;
import com.yanx.system.web.query.SysDictDataListByTypeQry;

import java.util.List;

/**
 * 数据字典缓存
 */
public enum SysDictCache implements RedisCacheManager<List<SysDictDataVo>> {

    INSTANCE;

    @Override
    public String key(Object... keyParts) {
        return String_.format("sys_dict:{}", keyParts);
    }

//    @Override
//    public Integer timeout() {
//        return 3600;
//    }

    public void loadingAll() {
        QueryExecutor queryExecutor = SpringUtil.getBean(QueryExecutor.class);
        List<SysDictTypeVo> sysDictTypeVos = queryExecutor.execute(new SysDictTypeListQry(null));
        for (SysDictTypeVo dictType : sysDictTypeVos) {
            String type = dictType.getDictType();
            List<SysDictDataVo> sysDictDataVos = queryExecutor.execute(new SysDictDataListByTypeQry(type));
            SysDictCache.INSTANCE.set(new String[]{type}, sysDictDataVos);
        }
    }
}
