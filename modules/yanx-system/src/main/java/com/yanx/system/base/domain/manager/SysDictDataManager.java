package com.yanx.system.base.domain.manager;

import com.yanx.common.core.domain.manager.BaseManager;
import com.yanx.system.base.domain.entity.SysDictData;
import com.yanx.system.base.domain.repository.SysDictDataRepository;
import org.springframework.stereotype.Service;

/**
 * 字典数据领域服务
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
@Service
public class SysDictDataManager extends BaseManager<SysDictData, SysDictDataRepository> {

}
