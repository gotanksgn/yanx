package com.yanx.system.web.command;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.VoidCommand;
import com.yanx.system.base.domain.manager.SysUserManager;
import lombok.AllArgsConstructor;

/**
 * 菜单权限 新增
 *
 * @author: gotanks
 * @create: 2023-5-23
 */
@AllArgsConstructor
public class SystemUserRoleAddCmd extends VoidCommand {
    private Long userId;
    private Long[] roleIds;

    @Override
    public void handler(Executor executor) {
        SysUserManager sysUserManager = executor.getReceiver(SysUserManager.class);
        sysUserManager.addUserRoles(userId, roleIds);
    }
}