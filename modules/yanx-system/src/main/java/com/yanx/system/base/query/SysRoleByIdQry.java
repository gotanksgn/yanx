package com.yanx.system.base.query;

import com.querydsl.core.types.Projections;
import com.querydsl.core.types.QBean;
import com.yanx.common.command.Executor;
import com.yanx.common.core.command.QryCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.system.base.domain.entity.QSysRole;
import com.yanx.system.base.model.vo.SysRoleVo;
import lombok.AllArgsConstructor;

/**
 * 根据ID查询菜单权限
 *
 * @author: gotanks
 * @create: 2023-4-12
 */
@AllArgsConstructor
public class SysRoleByIdQry extends QryCommand<SysRoleVo> {

    private Long id;

    @Override
    public SysRoleVo execute(Executor executor) {
        if (id == null) {
            throw new BusinessException("菜单权限ID不能为空");
        }
        QSysRole sysRole = QSysRole.sysRole;
        return queryFactory.select(this.fields())
                .from(sysRole)
                .where(sysRole.deleted.eq(false), sysRole.id.eq(id))
                .fetchOne();
    }

    /**
     * 菜单权限VO映射
     *
     * @return QBean<SysRoleVo>
     */
    public static QBean<SysRoleVo> fields() {
        QSysRole sysRole = QSysRole.sysRole;
        return Projections.fields(
                SysRoleVo.class,
                sysRole.roleName,
                sysRole.roleKey,
                sysRole.roleSort,
                sysRole.dataScope,
                sysRole.menuCheckStrictly,
                sysRole.deptCheckStrictly,
                sysRole.status,
                sysRole.createTime,
                sysRole.id
        );
    }
}