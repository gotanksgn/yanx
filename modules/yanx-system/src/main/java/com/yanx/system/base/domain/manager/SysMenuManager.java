package com.yanx.system.base.domain.manager;

import com.yanx.common.core.domain.manager.BaseManager;
import com.yanx.system.base.domain.entity.SysMenu;
import com.yanx.system.base.domain.repository.SysMenuRepository;
import org.springframework.stereotype.Service;

/**
 * 菜单权限领域服务
 *
 * @author: gotanks
 * @create: 2023-2-9
 */
@Service
public class SysMenuManager extends BaseManager<SysMenu, SysMenuRepository> {

    /**
     * 检验菜单名称是否唯一
     *
     * @param menuName
     * @param parentId
     * @return
     */
    public boolean checkMenuNameUnique(String menuName, Long parentId) {
        int num = repository.countByMenuNameAndParentId(menuName, parentId);
        return num > 0;
    }

    /**
     * 检验菜单名称是否唯一
     *
     * @param menuName
     * @param parentId
     * @param menuId
     * @return
     */
    public boolean checkMenuNameUnique(String menuName, Long parentId, Long menuId) {
        int num = repository.countByMenuNameAndParentIdAndIdNot(menuName, parentId, menuId);
        return num > 0;
    }

    /**
     * 是否存在子菜单
     *
     * @param menuId
     * @return
     */
    public boolean hasChildByMenuId(Long menuId) {
        int num = repository.countByParentId(menuId);
        return num > 0;
    }

    /**
     * 菜单是否已分配
     *
     * @param menuId
     * @return
     */
    public boolean checkMenuExistRole(Long menuId) {
        int num = repository.countRoleByMenuId(menuId);
        return num > 0;
    }
}
