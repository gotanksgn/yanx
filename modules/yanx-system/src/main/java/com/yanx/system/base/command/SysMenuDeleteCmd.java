package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.system.base.model.dto.SysMenuDto;

import java.util.Collection;

/**
 * 菜单权限 删除命令
 *
 * @author: gotanks
 * @create: 2023-2-9
 */
public class SysMenuDeleteCmd extends CrudCommand<SysMenuDto> {

    /**
     * 删除
     *
     * @param id
     */
    public SysMenuDeleteCmd(Long id) {
        super(id);
    }

    /**
     * 批量删除
     *
     * @param ids
     */
    public SysMenuDeleteCmd(Collection<Long> ids) {
        super(ids);
    }

}