package com.yanx.system.web.command;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.VoidCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.system.base.command.SysConfigUpdateCmd;
import com.yanx.system.base.domain.manager.SysConfigManager;
import com.yanx.system.base.model.dto.SysConfigDto;
import lombok.AllArgsConstructor;

/**
 * 系统配置 修改命令
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@AllArgsConstructor
public class SystemConfigEditCmd extends VoidCommand {
    private SysConfigDto config;

    @Override
    public void handler(Executor executor) {
        SysConfigManager sysConfigManager = executor.getReceiver(SysConfigManager.class);
        if (sysConfigManager.checkConfigKeyUnique(config.getConfigKey(), config.getId())) {
            throw new BusinessException("修改参数'{}'失败，参数键名已存在", config.getConfigKey());
        }

//        config.setUpdateBy(getConfigname());
        executor.execute(new SysConfigUpdateCmd(config.getId(), config));
    }
}