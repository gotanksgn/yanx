package com.yanx.system.base.domain.entity.rel;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 用户和岗位关联 sys_user_post
 *
 * @author gotanks
 */
@Data
@Entity
@Table(name = "SYS_USER_POST")
@IdClass(value = SysUserPost.class)
public class SysUserPost implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 用户ID
     */
    @Id
    @JoinColumn(foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    private Long userId;

    /**
     * 岗位ID
     */
    @Id
    @JoinColumn(foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT))
    private Long postId;

}
