package com.yanx.system.base.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yanx.common.core.dto.BaseDto;
import com.yanx.common.enums.DataStatus;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 字典数据DTO
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "字典数据入参模型")
public class SysDictDataDto extends BaseDto<Long> {

    private static final long serialVersionUID = 1L;

    @Schema(name = "字典排序")
    private Long dictSort;

    @Schema(name = "字典标签")
    private String dictLabel;

    @Schema(name = "字典键值")
    private String dictValue;

    @Schema(name = "字典类型")
    private String dictType;

    @Schema(name = "是否默认")
    private String isDefault;

    @Schema(name = "状态")
    private DataStatus status;

}
