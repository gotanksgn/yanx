package com.yanx.system.base.domain.entity;

import com.yanx.codegen.annotation.CodeGenClass;
import com.yanx.codegen.annotation.CodeGenField;
import com.yanx.common.core.domain.entity.BaseTimeEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 菜单权限表 sys_menu
 *
 * @author gotanks
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "SYS_MENU")
@CodeGenClass(value = "菜单权限", author = "gotanks")
public class SysMenu extends BaseTimeEntity {
    private static final long serialVersionUID = 1L;

    @CodeGenField("菜单名称")
    private String menuName;

    @CodeGenField("父菜单名称")
    private String parentName;

    @CodeGenField("父菜单ID")
    private Long parentId;

    @CodeGenField("显示顺序")
    private Integer orderNum;

    @CodeGenField("路由地址")
    private String path;

    @CodeGenField("组件路径")
    private String component;

    @CodeGenField("路由参数")
    private String query;

    /**
     * （0是 1否）
     */
    @CodeGenField("是否为外链")
    private String isFrame;

    /**
     * （0缓存 1不缓存）
     */
    @CodeGenField("是否缓存")
    private String isCache;

    /**
     * （M目录 C菜单 F按钮）
     */
    @CodeGenField("类型")
    private String menuType;

    /**
     * （0显示 1隐藏）
     */
    @CodeGenField("显示状态")
    private String visible;

    /**
     * （0正常 1停用）
     */
    @CodeGenField("菜单状态")
    private String status;

    @CodeGenField("权限字符串")
    private String perms;

    @CodeGenField("菜单图标")
    private String icon;

}
