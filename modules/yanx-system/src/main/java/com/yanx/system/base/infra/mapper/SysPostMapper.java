package com.yanx.system.base.infra.mapper;

import com.yanx.system.base.model.dto.SysPostDto;
import com.yanx.system.base.domain.entity.SysPost;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
 * 岗位 实体转换类
 *
 * @author: gotanks
 * @create: 2023-5-23
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface SysPostMapper {

    SysPostMapper INSTANCE = Mappers.getMapper(SysPostMapper.class);

    SysPost toSysPost(SysPostDto dto);

    SysPost toSysPost(SysPostDto dto, @MappingTarget SysPost entity);

}
