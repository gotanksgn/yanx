package com.yanx.system.web.command;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.VoidCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.system.base.command.SysMenuDeleteCmd;
import com.yanx.system.base.domain.manager.SysMenuManager;
import lombok.AllArgsConstructor;

/**
 * 菜单权限 删除
 *
 * @author: gotanks
 * @create: 2023-2-9
 */
@AllArgsConstructor
public class SystemMenuRemoveCmd extends VoidCommand {
    private Long menuId;

    @Override
    public void handler(Executor executor) {
        SysMenuManager sysMenuManager = executor.getReceiver(SysMenuManager.class);
        if (sysMenuManager.hasChildByMenuId(menuId)) {
            throw new BusinessException("存在子菜单,不允许删除");
        } else if (sysMenuManager.checkMenuExistRole(menuId)) {
            throw new BusinessException("菜单已分配,不允许删除");
        }
        executor.execute(new SysMenuDeleteCmd(menuId));
    }
}