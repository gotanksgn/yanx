package com.yanx.system.web.command;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.VoidCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.common.utils.String_;
import com.yanx.system.base.command.SysConfigDeleteCmd;
import com.yanx.system.base.domain.manager.SysConfigManager;
import com.yanx.system.web.infra.constant.UserConstants;
import lombok.AllArgsConstructor;

/**
 * 系统配置 删除命令
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@AllArgsConstructor
public class SystemConfigRemoveCmd extends VoidCommand {

    private Long[] configIds;

    @Override
    public void handler(Executor executor) {
        SysConfigManager sysConfigManager = executor.getReceiver(SysConfigManager.class);
        for (Long configId : configIds) {
            sysConfigManager.findById(configId).ifPresent(config -> {
                if (String_.equals(UserConstants.YES, config.getConfigType())) {
                    throw new BusinessException("内置参数【{}】不能删除 ", config.getConfigKey());
                }
                executor.execute(new SysConfigDeleteCmd(configId));
            });
        }
    }
}