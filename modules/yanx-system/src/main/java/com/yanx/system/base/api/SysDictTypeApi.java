package com.yanx.system.base.api;

import com.yanx.common.annotation.ApiResult;
import com.yanx.common.annotation.Pagination;
import com.yanx.common.core.api.BaseApi;
import com.yanx.system.base.command.SysDictTypeCreateCmd;
import com.yanx.system.base.command.SysDictTypeDeleteCmd;
import com.yanx.system.base.command.SysDictTypeUpdateCmd;
import com.yanx.system.base.model.dto.SysDictTypeDto;
import com.yanx.system.base.model.qo.SysDictTypeQo;
import com.yanx.system.base.model.vo.SysDictTypeVo;
import com.yanx.system.base.query.SysDictTypeByIdQry;
import com.yanx.system.base.query.SysDictTypeListQry;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

/**
 * 字典类型 接口
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
@Tag(name = "字典类型", description = "字典类型")
@RestController
@RequestMapping(value = "/api/sys-dict-type")
public class SysDictTypeApi extends BaseApi {

    @ApiResult
    @Operation(summary = "根据ID查询字典类型")
    @GetMapping("/{id}")
    public SysDictTypeVo get(@PathVariable Long id) {
        return queryExecutor.execute(new SysDictTypeByIdQry(id));
    }

    @Pagination(total = true)
    @ApiResult
    @Operation(summary = "分页查询字典类型")
    @GetMapping
    public List<SysDictTypeVo> getList(SysDictTypeQo sysDictTypeQo) {
        return queryExecutor.execute(new SysDictTypeListQry(sysDictTypeQo));
    }

    @ApiResult
    @Operation(summary = "新增字典类型")
    @PostMapping
    public void create(@Valid @RequestBody SysDictTypeDto sysDictTypeDto) {
        commandExecutor.execute(new SysDictTypeCreateCmd(sysDictTypeDto));
    }

    @ApiResult
    @Operation(summary = "更新字典类型")
    @PutMapping("/{id}")
    public void update(@PathVariable("id") Long id, @Valid @RequestBody SysDictTypeDto sysDictTypeDto) {
        commandExecutor.execute(new SysDictTypeUpdateCmd(id, sysDictTypeDto));
    }

    @ApiResult
    @Operation(summary = "删除字典类型")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        commandExecutor.execute(new SysDictTypeDeleteCmd(id));
    }

    @ApiResult
    @Operation(summary = "批量删除字典类型")
    @DeleteMapping
    public void batchDelete(@RequestBody Set<Long> ids) {
        commandExecutor.execute(new SysDictTypeDeleteCmd(ids));
    }
}