package com.yanx.system.base.model.qo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yanx.common.utils.DateTime_;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * 菜单权限QO
 *
 * @author: gotanks
 * @create: 2023-2-10
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "菜单权限查询模型")
public class SysRoleQo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(name = "角色ID")
    private Long roleId;

    @Schema(name = "角色ID")
    private String roleName;

    @Schema(name = "角色ID")
    private String status;

    @Schema(name = "角色ID")
    private String roleKey;

    @Schema(name = "请求参数")
    private Map<String, Object> params = new HashMap<>();

    public LocalDateTime getBeginTime() {
        Object beginTime = params.get("beginTime");
        if (beginTime != null) {
            return DateTime_.parseDateTime(beginTime.toString());
        }
        return null;
    }

    public LocalDateTime getEndTime() {
        Object endTime = params.get("endTime");
        if (endTime != null) {
            return DateTime_.parseDateTime(endTime.toString());
        }
        return null;
    }
}
