package com.yanx.system.base.domain.repository;

import com.yanx.common.core.domain.repository.BaseRepository;
import com.yanx.system.base.domain.entity.SysConfig;

import java.io.Serializable;

/**
 * 参数配置仓库
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
public interface SysConfigRepository extends BaseRepository<SysConfig, Serializable> {

    int countByConfigKey(String configKey);

    int countByConfigKeyAndIdNot(String configKey, Long id);
}
