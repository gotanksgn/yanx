package com.yanx.system.base.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yanx.common.core.dto.BaseDto;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 岗位DTO
 *
 * @author: gotanks
 * @create: 2023-5-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "岗位入参模型")
public class SysPostDto extends BaseDto<Long> {

    private static final long serialVersionUID = 1L;

    @Schema(name = "岗位编码")
    private String postCode;

    @Schema(name = "岗位名称")
    private String postName;

    @Schema(name = "岗位排序")
    private Integer postSort;

    @Schema(name = "状态")
    private String status;

}
