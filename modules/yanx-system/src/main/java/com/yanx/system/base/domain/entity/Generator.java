package com.yanx.system.base.domain.entity;

import com.yanx.codegen.CodeGenerator;

/**
 * 代码生成工具类
 * 使用方法：
 * 1、在对应的业务包下创建domain.entity包
 * 2、在domain.entity包中创建Jpa的Entity类，类和属性上加上@CodeGenClass和@CodeGenField注解，具体见示例
 * 3、将Generator.java类拷贝到domain.entity包下，运行此类，即可自动生成代码
 *
 * @author gotanks
 * @date 2021-09-26
 */
public class Generator {
    private static final String scanPackage = Generator.class.getPackage().getName();

    public static void main(String[] args) {

        CodeGenerator.generatorWithEntity(scanPackage, new Class[]{SysDictType.class, SysDictData.class}, true);
    }
}
