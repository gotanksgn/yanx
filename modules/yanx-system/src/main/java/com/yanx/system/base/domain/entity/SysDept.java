package com.yanx.system.base.domain.entity;

import com.yanx.codegen.annotation.CodeGenClass;
import com.yanx.codegen.annotation.CodeGenField;
import com.yanx.common.core.domain.entity.BaseTreeTimeEntity;
import com.yanx.system.base.domain.event.SysDeptSaveEvent;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.domain.DomainEvents;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 部门表 sys_dept
 *
 * @author gotanks
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "SYS_DEPT")
@CodeGenClass(value = "部门", author = "gotanks")
public class SysDept extends BaseTreeTimeEntity {
    private static final long serialVersionUID = 1L;

    @CodeGenField("部门名称")
    private String deptName;

    @CodeGenField("显示顺序")
    private Integer orderNum;

    @CodeGenField("负责人")
    private String leader;

    @CodeGenField("联系电话")
    private String phone;

    @CodeGenField("邮箱")
    private String email;

    /**
     * 部门状态:0正常,1停用
     */
    @CodeGenField("部门状态")
    private String status;


    @DomainEvents
    Object domainEvents() {
        return new SysDeptSaveEvent(this);
    }
}
