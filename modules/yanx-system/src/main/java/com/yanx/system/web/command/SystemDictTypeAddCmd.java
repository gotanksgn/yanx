package com.yanx.system.web.command;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.VoidCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.system.base.command.SysDictTypeCreateCmd;
import com.yanx.system.base.domain.manager.SysDictTypeManager;
import com.yanx.system.base.model.dto.SysDictTypeDto;
import lombok.AllArgsConstructor;

/**
 * 字典类型 新增命令
 *
 * @author: gotanks
 * @create: 2023-6-8
 */
@AllArgsConstructor
public class SystemDictTypeAddCmd extends VoidCommand {
    private SysDictTypeDto dictType;

    @Override
    public void handler(Executor executor) {
        SysDictTypeManager sysDictTypeManager = executor.getReceiver(SysDictTypeManager.class);
        if (sysDictTypeManager.checkDictTypeUnique(dictType.getDictType())) {
            throw new BusinessException("新增字典'{}'失败，字典类型已存在", dictType.getDictType());
        }

//        dictType.setCreateBy(getDictTypename());
        executor.execute(new SysDictTypeCreateCmd(dictType));
    }
}