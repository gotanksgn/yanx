package com.yanx.system.web.api;

import com.yanx.common.annotation.ApiResult;
import com.yanx.common.annotation.OperationLog;
import com.yanx.common.annotation.Pagination;
import com.yanx.common.core.api.BaseApi;
import com.yanx.common.enums.BusinessType;
import com.yanx.common.security.preauthorize.PreAuthorize;
import com.yanx.system.base.model.dto.SysPostDto;
import com.yanx.system.base.model.qo.SysPostQo;
import com.yanx.system.base.model.vo.SysPostVo;
import com.yanx.system.base.query.SysPostByIdQry;
import com.yanx.system.base.query.SysPostListQry;
import com.yanx.system.web.command.SystemPostAddCmd;
import com.yanx.system.web.command.SystemPostEditCmd;
import com.yanx.system.web.command.SystemPostRemoveCmd;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * 系统岗位管理 接口
 *
 * @author: gotanks
 * @create: 2023-5-23
 */
@Tag(name = "岗位管理", description = "岗位管理")
@RestController
@RequestMapping(value = "/system/post")
public class SystemPostApi extends BaseApi {

    /**
     * 获取岗位列表
     */
    @PreAuthorize(hasPermi = "system:post:list")
    @Pagination(total = true)
    @ApiResult
    @Operation(summary = "获取岗位列表")
    @GetMapping("/list")
    public List<SysPostVo> list(SysPostQo post) {
        return queryExecutor.execute(new SysPostListQry(post));
    }

//    @Log(title = "岗位管理", businessType = BusinessType.EXPORT)
//    @org.springframework.security.access.prepost.PreAuthorize("@ss.hasPermi('system:post:export')")
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, SysPost post) {
//        List<SysPost> list = postService.selectPostList(post);
//        ExcelUtil<SysPost> util = new ExcelUtil<SysPost>(SysPost.class);
//        util.exportExcel(response, list, "岗位数据");
//    }

    /**
     * 根据岗位编号获取详细信息
     */
    @PreAuthorize(hasPermi = "system:post:query")
    @ApiResult
    @Operation(summary = "根据岗位编号获取详细信息")
    @GetMapping(value = "/{postId}")
    public SysPostVo getInfo(@PathVariable Long postId) {
        return queryExecutor.execute(new SysPostByIdQry(postId));
    }

    /**
     * 新增岗位
     */
    @PreAuthorize(hasPermi = "system:post:add")
    @OperationLog(title = "岗位管理", businessType = BusinessType.INSERT)
    @ApiResult
    @Operation(summary = "新增岗位")
    @PostMapping
    public void add(@Validated @RequestBody SysPostDto post) {
        commandExecutor.execute(new SystemPostAddCmd(post));
    }

    /**
     * 修改岗位
     */
    @PreAuthorize(hasPermi = "system:post:edit")
    @OperationLog(title = "岗位管理", businessType = BusinessType.UPDATE)
    @ApiResult
    @Operation(summary = "修改岗位")
    @PutMapping
    public void edit(@Validated @RequestBody SysPostDto post) {
        commandExecutor.execute(new SystemPostEditCmd(post));
    }

    /**
     * 删除岗位
     */
    @PreAuthorize(hasPermi = "system:post:remove")
    @OperationLog(title = "岗位管理", businessType = BusinessType.DELETE)
    @ApiResult
    @Operation(summary = "删除岗位")
    @DeleteMapping("/{postIds}")
    public void remove(@PathVariable Set<Long> postIds) {
        commandExecutor.execute(new SystemPostRemoveCmd(postIds));
    }

    /**
     * 获取岗位选择框列表
     */
    @ApiResult
    @Operation(summary = "获取岗位选择框列表")
    @GetMapping("/optionselect")
    public List<SysPostVo> optionselect() {
        return queryExecutor.execute(new SysPostListQry(new SysPostQo()));
    }
}