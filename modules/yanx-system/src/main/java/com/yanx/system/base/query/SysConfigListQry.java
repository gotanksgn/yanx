package com.yanx.system.base.query;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.yanx.common.command.Executor;
import com.yanx.common.core.command.PageQryCommand;
import com.yanx.system.base.domain.entity.QSysConfig;
import com.yanx.system.base.model.qo.SysConfigQo;
import com.yanx.system.base.model.vo.SysConfigVo;
import lombok.AllArgsConstructor;

import java.util.List;

/**
 * 参数配置分页查询
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@AllArgsConstructor
public class SysConfigListQry extends PageQryCommand<List<SysConfigVo>> {

    private SysConfigQo sysConfigQo;

    @Override
    public List<SysConfigVo> execute(Executor executor) {
        QSysConfig sysConfig = QSysConfig.sysConfig;

        //查询条件
        BooleanBuilder condition = new BooleanBuilder();
        condition.and(sysConfig.deleted.eq(false));
        if (sysConfigQo != null) {
            //自定义查询条件
            if (sysConfigQo.getConfigName() != null && !sysConfigQo.getConfigName().equals("")) {
                condition.and(sysConfig.configName.contains(sysConfigQo.getConfigName()));
            }
            if (sysConfigQo.getConfigType() != null && !sysConfigQo.getConfigType().equals("")) {
                condition.and(sysConfig.configType.eq(sysConfigQo.getConfigType()));
            }
            if (sysConfigQo.getConfigKey() != null && !sysConfigQo.getConfigKey().equals("")) {
                condition.and(sysConfig.configKey.contains(sysConfigQo.getConfigKey()));
            }
            if (sysConfigQo.getBeginTime() != null) {
                condition.and(sysConfig.createTime.gt(sysConfigQo.getBeginTime()));
            }
            if (sysConfigQo.getEndTime() != null) {
                condition.and(sysConfig.createTime.lt(sysConfigQo.getEndTime()));
            }
        }

        JPAQuery<SysConfigVo> query = queryFactory.select(SysConfigByIdQry.fields())
                .from(sysConfig)
                .where(condition);

        return this.pageList(query);
    }

}