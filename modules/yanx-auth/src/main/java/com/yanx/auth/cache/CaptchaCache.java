package com.yanx.auth.cache;

import com.yanx.common.utils.String_;
import com.yanx.framework.redis.RedisCacheManager;

import java.util.concurrent.TimeUnit;

/**
 * 验证码缓存
 */
public enum CaptchaCache implements RedisCacheManager<String> {

    INSTANCE;

    @Override
    public String key(Object... keyParts) {
        return String_.format("captcha_codes:{}", keyParts);
    }

    @Override
    public Integer timeout() {
        return 2;
    }

    @Override
    public TimeUnit timeUnit() {
        return TimeUnit.MINUTES;
    }
}
