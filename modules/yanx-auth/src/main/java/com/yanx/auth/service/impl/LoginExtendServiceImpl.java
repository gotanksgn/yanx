package com.yanx.auth.service.impl;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.yanx.auth.cache.CaptchaCache;
import com.yanx.common.exception.user.CaptchaException;
import com.yanx.common.exception.user.CaptchaExpireException;
import com.yanx.common.security.LoginInforRecordUtils;
import com.yanx.common.security.service.LoginExtendService;
import com.yanx.common.utils.Message_;
import com.yanx.system.client.SystemRpc;
import com.yanx.system.client.dto.LoginUserVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 用户验证处理
 *
 * @author gotanks
 */
@Slf4j
@Service
public class LoginExtendServiceImpl implements LoginExtendService {

    @DubboReference(version = "1.0")
    private SystemRpc systemRpc;

    @Override
    public LoginUser getLoginUserByUsername(String username) {
        LoginUserVo loginUserVo = systemRpc.getLoginUserByUsername(username);
        LoginUser loginUser = new LoginUser();
        if (loginUserVo == null) {
            log.info("登录用户：{} 不存在.", username);
            loginUser.setAccountNonExpired(false);
        } else if (loginUserVo.isDisabled()) {
            log.info("登录用户：{} 已被停用.", username);
            loginUser.setEnabled(false);
        } else if (loginUserVo.isLocked()) {
            log.info("登录用户：{} 已被锁定.", username);
            loginUser.setAccountNonLocked(false);
        } else {
            Map<String, Object> user = loginUserVo.getUser();
            loginUser.setUsername(user.get("userName").toString());
            loginUser.setPassword(user.get("password").toString());
            loginUser.setUserId(Long.parseLong(user.get("id").toString()));
            loginUser.setUser(user);
            loginUser.setRoles(loginUserVo.getRoles());
            loginUser.setPermissions(loginUserVo.getPermissions());
//            loginUser.put("deptId", loginUserVo.getDeptId());
//            loginUser.put("fullname", loginUserVo.getFullname());
        }
        return loginUser;
    }

    @Override
    public void validateCaptcha(String username, String code, String uuid) {
        boolean captchaEnabled = systemRpc.captchaEnabled();
        if (!captchaEnabled) {
            return;
        }
        String captcha = CaptchaCache.INSTANCE.get(uuid);
        CaptchaCache.INSTANCE.delete(uuid);
        if (captcha == null) {
            LoginInforRecordUtils.error(username, Message_.message("user.jcaptcha.expire"));
//            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.jcaptcha.expire")));
            throw new CaptchaExpireException();
        }
        if (!code.equalsIgnoreCase(captcha)) {
            LoginInforRecordUtils.error(username, Message_.message("user.jcaptcha.error"));
//            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.jcaptcha.error")));
            throw new CaptchaException();
        }
    }
}
