package com.yanx.auth.api;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.yanx.auth.service.AuthService;
import com.yanx.common.annotation.ApiResult;
import com.yanx.common.core.api.BaseApi;
import com.yanx.common.security.SecurityUtils;
import com.yanx.common.security.model.LoginDto;
import com.yanx.common.security.model.LoginVo;
import com.yanx.common.security.model.RegisterDto;
import com.yanx.common.security.service.LoginService;
import com.yanx.system.client.dto.RouterVo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Tag(name = "登录API")
@RequestMapping
public class AuthApi extends BaseApi {

    @Autowired
    private LoginService loginService;

    @Autowired
    private AuthService authService;

    @Operation(summary = "登录并创建token")
    @PostMapping(value = "/login")
    public LoginVo login(@RequestBody LoginDto loginDto) {
        return loginService.login(loginDto);
    }

    @Operation(summary = "注册")
    @PostMapping("/register")
    public void register(@RequestBody RegisterDto user) {
        authService.register(user);
    }

    /**
     * 获取用户信息
     *
     * @return 用户信息
     */
    @Operation(summary = "获取用户信息")
    @GetMapping("/getInfo")
    public Map<String, Object> getInfo() {
        //用户信息
        Map<String, Object> result = new HashMap<>();
        LoginUser loginUser = SecurityUtils.getLoginUser();
        result.put("user", loginUser.getUser());
        result.put("roles", loginUser.getRoles());
        result.put("permissions", loginUser.getPermissions());
        return result;
    }

    /**
     * 获取路由信息
     *
     * @return 路由信息
     */
    @ApiResult
    @Operation(summary = "获取路由信息")
    @GetMapping("/getRouters")
    public List<RouterVo> getRouters() {
        return authService.getRouters();
    }

    /**
     * 生成验证码
     */
    @Operation(summary = "生成验证码")
    @GetMapping("/captchaImage")
    public Map<String, Object> getCode() {
        return authService.getCode();
    }
}
