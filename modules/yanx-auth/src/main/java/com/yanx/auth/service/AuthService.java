package com.yanx.auth.service;

import com.yanx.common.security.model.RegisterDto;
import com.yanx.system.client.dto.RouterVo;

import java.util.List;
import java.util.Map;

/**
 * 登录应用服务接口
 *
 * @author gotanks
 * @date 2021-02-26 13:41:00
 */
public interface AuthService {

    /**
     * 获取路由信息
     */
    List<RouterVo> getRouters();

    /**
     * 获取验证码
     *
     * @return
     */
    Map<String, Object> getCode();

    /**
     * 注册
     *
     * @param user
     */
    void register(RegisterDto user);
}
