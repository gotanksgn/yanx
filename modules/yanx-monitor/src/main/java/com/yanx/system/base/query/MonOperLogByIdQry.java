package com.yanx.system.base.query;

import com.querydsl.core.types.Projections;
import com.querydsl.core.types.QBean;
import com.yanx.common.command.Executor;
import com.yanx.common.core.command.QryCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.system.base.domain.entity.QMonOperLog;
import com.yanx.system.base.model.vo.MonOperLogVo;
import lombok.AllArgsConstructor;

/**
 * 根据ID查询操作日志
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@AllArgsConstructor
public class MonOperLogByIdQry extends QryCommand<MonOperLogVo> {

    private Long id;

    @Override
    public MonOperLogVo execute(Executor executor) {
        if (id == null) {
            throw new BusinessException("操作日志ID不能为空");
        }
        QMonOperLog monOperLog = QMonOperLog.monOperLog;
        return queryFactory.select(this.fields())
                .from(monOperLog)
                .where(monOperLog.deleted.eq(false), monOperLog.id.eq(id))
                .fetchOne();
    }

    /**
     * 操作日志VO映射
     *
     * @return QBean<MonOperLogVo>
     */
    public static QBean<MonOperLogVo> fields() {
        QMonOperLog monOperLog = QMonOperLog.monOperLog;
        return Projections.fields(
            MonOperLogVo.class,
            monOperLog.title,
            monOperLog.businessType,
            monOperLog.method,
            monOperLog.requestMethod,
            monOperLog.operatorType,
            monOperLog.operName,
            monOperLog.deptName,
            monOperLog.operUrl,
            monOperLog.operIp,
            monOperLog.operLocation,
            monOperLog.operParam,
            monOperLog.jsonResult,
            monOperLog.status,
            monOperLog.errorMsg,
            monOperLog.operTime,
            monOperLog.id
        );
    }
}