package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.system.base.infra.mapper.MonLoginInforMapper;
import com.yanx.system.base.model.dto.MonLoginInforDto;

/**
 * 系统访问记录 新增命令
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
public class MonLoginInforCreateCmd extends CrudCommand<MonLoginInforDto> {

    /**
     * 新增
     *
     * @param monLoginInforDto
     */
    public MonLoginInforCreateCmd(MonLoginInforDto monLoginInforDto) {
        super(monLoginInforDto);
    }

    /**
     * 新增时实体类转换
     *
     * @return
     */
    @Override
    public BaseEntity toEntity() {
        return MonLoginInforMapper.INSTANCE.toMonLoginInfor(this.getDto());
    }

}