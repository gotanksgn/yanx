package com.yanx.system.base.api;

import com.yanx.common.annotation.ApiResult;
import com.yanx.common.annotation.Pagination;
import com.yanx.common.core.api.BaseApi;
import com.yanx.system.base.command.MonOperLogCreateCmd;
import com.yanx.system.base.command.MonOperLogDeleteCmd;
import com.yanx.system.base.command.MonOperLogUpdateCmd;
import com.yanx.system.base.model.dto.MonOperLogDto;
import com.yanx.system.base.model.qo.MonOperLogQo;
import com.yanx.system.base.model.vo.MonOperLogVo;
import com.yanx.system.base.query.MonOperLogByIdQry;
import com.yanx.system.base.query.MonOperLogListQry;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

/**
 * 操作日志 接口
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@Tag(name = "操作日志", description = "操作日志")
@RestController
@RequestMapping(value = "/api/mon-oper-log")
public class MonOperLogApi extends BaseApi {

    @ApiResult
    @Operation(summary = "根据ID查询操作日志")
    @GetMapping("/{id}")
    public MonOperLogVo get(@PathVariable Long id) {
        return queryExecutor.execute(new MonOperLogByIdQry(id));
    }

    @Pagination(total = true)
    @ApiResult
    @Operation(summary = "分页查询操作日志")
    @GetMapping
    public List<MonOperLogVo> getList(MonOperLogQo monOperLogQo) {
        return queryExecutor.execute(new MonOperLogListQry(monOperLogQo));
    }

    @ApiResult
    @Operation(summary = "新增操作日志")
    @PostMapping
    public void create(@Valid @RequestBody MonOperLogDto monOperLogDto) {
        commandExecutor.execute(new MonOperLogCreateCmd(monOperLogDto));
    }

    @ApiResult
    @Operation(summary = "更新操作日志")
    @PutMapping("/{id}")
    public void update(@PathVariable("id") Long id, @Valid @RequestBody MonOperLogDto monOperLogDto) {
        commandExecutor.execute(new MonOperLogUpdateCmd(id, monOperLogDto));
    }

    @ApiResult
    @Operation(summary = "删除操作日志")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        commandExecutor.execute(new MonOperLogDeleteCmd(id));
    }

    @ApiResult
    @Operation(summary = "批量删除操作日志")
    @DeleteMapping
    public void batchDelete(@RequestBody Set<Long> ids) {
        commandExecutor.execute(new MonOperLogDeleteCmd(ids));
    }
}