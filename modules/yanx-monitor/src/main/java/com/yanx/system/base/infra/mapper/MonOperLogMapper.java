package com.yanx.system.base.infra.mapper;

import com.yanx.system.base.model.dto.MonOperLogDto;
import com.yanx.system.base.domain.entity.MonOperLog;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
 * 操作日志 实体转换类
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface MonOperLogMapper {

    MonOperLogMapper INSTANCE = Mappers.getMapper(MonOperLogMapper.class);

    MonOperLog toMonOperLog(MonOperLogDto dto);

    MonOperLog toMonOperLog(MonOperLogDto dto, @MappingTarget MonOperLog entity);

}
