package com.yanx.system.base.domain.manager;

import com.yanx.common.core.domain.manager.BaseManager;
import com.yanx.system.base.domain.entity.MonOperLog;
import com.yanx.system.base.domain.repository.MonOperLogRepository;
import org.springframework.stereotype.Service;

/**
 * 操作日志领域服务
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@Service
public class MonOperLogManager extends BaseManager<MonOperLog, MonOperLogRepository> {
    /**
     * 清空操作日志表
     */
    public void clean() {
        repository.trancate();
    }
}
