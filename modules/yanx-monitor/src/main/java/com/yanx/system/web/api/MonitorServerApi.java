package com.yanx.system.web.api;

import com.yanx.common.annotation.ApiResult;
import com.yanx.common.security.preauthorize.PreAuthorize;
import com.yanx.framework.web.domain.Server;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 服务器监控
 *
 * @author gotanks
 */
@Tag(name = "服务器监控", description = "服务器监控")
@RestController
@RequestMapping("/monitor/server")
public class MonitorServerApi {
    @PreAuthorize(hasPermi = "monitor:server:list")
    @ApiResult
    @Operation(summary = "获取服务器监控信息")
    @GetMapping
    public Server getInfo() throws Exception {
        Server server = new Server();
        server.copyTo();
        return server;
    }
}
