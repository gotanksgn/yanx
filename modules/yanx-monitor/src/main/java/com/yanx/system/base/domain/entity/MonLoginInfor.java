package com.yanx.system.base.domain.entity;

import com.yanx.codegen.annotation.CodeGenClass;
import com.yanx.codegen.annotation.CodeGenField;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.common.enums.DataStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * 系统访问记录表 sys_logininfor
 *
 * @author gotanks
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "SYS_LOGININFOR")
@CodeGenClass(value = "系统访问记录", author = "gotanks")
public class MonLoginInfor extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 用户账号
     */
    @CodeGenField("用户账号")
    private String userName;

    /**
     * 登录状态 0成功 1失败
     */
    @CodeGenField("登录状态")
    private DataStatus status;

    /**
     * 登录IP地址
     */
    @CodeGenField("登录地址")
    private String ipaddr;

    /**
     * 登录地点
     */
    @CodeGenField("登录地点")
    private String loginLocation;

    /**
     * 浏览器类型
     */
    @CodeGenField("浏览器")
    private String browser;

    /**
     * 操作系统
     */
    @CodeGenField("操作系统")
    private String os;

    /**
     * 提示消息
     */
    @CodeGenField("提示消息")
    private String msg;

    /**
     * 访问时间
     */
    @Column(updatable = false)
    @CreationTimestamp
    @CodeGenField("访问时间")
    private LocalDateTime loginTime;

}
