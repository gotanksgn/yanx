package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.system.base.domain.entity.MonOperLog;
import com.yanx.system.base.infra.mapper.MonOperLogMapper;
import com.yanx.system.base.model.dto.MonOperLogDto;

/**
 * 操作日志 修改命令
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
public class MonOperLogUpdateCmd extends CrudCommand<MonOperLogDto> {

    /**
     * 修改
     *
     * @param id
    * @param monOperLogDto
     */
    public MonOperLogUpdateCmd(Long id, MonOperLogDto monOperLogDto) {
        super(id, monOperLogDto);
    }

    /**
     * 修改时实体类转换
     *
     * @param entity
     * @return
     */
    @Override
    public BaseEntity toEntity(BaseEntity entity) {
        return MonOperLogMapper.INSTANCE.toMonOperLog(this.getDto(), (MonOperLog) entity);
    }

}