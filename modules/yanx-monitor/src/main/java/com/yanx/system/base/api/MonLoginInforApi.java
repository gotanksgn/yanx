package com.yanx.system.base.api;

import com.yanx.common.annotation.ApiResult;
import com.yanx.common.annotation.Pagination;
import com.yanx.common.core.api.BaseApi;
import com.yanx.system.base.command.MonLoginInforCreateCmd;
import com.yanx.system.base.command.MonLoginInforDeleteCmd;
import com.yanx.system.base.command.MonLoginInforUpdateCmd;
import com.yanx.system.base.model.dto.MonLoginInforDto;
import com.yanx.system.base.model.qo.MonLoginInforQo;
import com.yanx.system.base.model.vo.MonLoginInforVo;
import com.yanx.system.base.query.MonLoginInforByIdQry;
import com.yanx.system.base.query.MonLoginInforListQry;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

/**
 * 系统访问记录 接口
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@Tag(name = "系统访问记录", description = "系统访问记录")
@RestController
@RequestMapping(value = "/api/mon-login-infor")
public class MonLoginInforApi extends BaseApi {

    @ApiResult
    @Operation(summary = "根据ID查询系统访问记录")
    @GetMapping("/{id}")
    public MonLoginInforVo get(@PathVariable Long id) {
        return queryExecutor.execute(new MonLoginInforByIdQry(id));
    }

    @Pagination(total = true)
    @ApiResult
    @Operation(summary = "分页查询系统访问记录")
    @GetMapping
    public List<MonLoginInforVo> getList(MonLoginInforQo monLoginInforQo) {
        return queryExecutor.execute(new MonLoginInforListQry(monLoginInforQo));
    }

    @ApiResult
    @Operation(summary = "新增系统访问记录")
    @PostMapping
    public void create(@Valid @RequestBody MonLoginInforDto monLoginInforDto) {
        commandExecutor.execute(new MonLoginInforCreateCmd(monLoginInforDto));
    }

    @ApiResult
    @Operation(summary = "更新系统访问记录")
    @PutMapping("/{id}")
    public void update(@PathVariable("id") Long id, @Valid @RequestBody MonLoginInforDto monLoginInforDto) {
        commandExecutor.execute(new MonLoginInforUpdateCmd(id, monLoginInforDto));
    }

    @ApiResult
    @Operation(summary = "删除系统访问记录")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        commandExecutor.execute(new MonLoginInforDeleteCmd(id));
    }

    @ApiResult
    @Operation(summary = "批量删除系统访问记录")
    @DeleteMapping
    public void batchDelete(@RequestBody Set<Long> ids) {
        commandExecutor.execute(new MonLoginInforDeleteCmd(ids));
    }
}