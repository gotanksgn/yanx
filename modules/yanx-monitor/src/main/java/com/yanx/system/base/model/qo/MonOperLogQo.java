package com.yanx.system.base.model.qo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yanx.common.enums.BusinessType;
import com.yanx.common.enums.DataStatus;
import com.yanx.common.utils.DateTime_;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * 操作日志QO
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "操作日志查询模型")
public class MonOperLogQo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(name = "操作模块")
    private String title;

    @Schema(name = "业务类型")
    private BusinessType businessType;

    @Schema(name = "业务类型数组")
    private BusinessType[] businessTypes;

    @Schema(name = "操作人员")
    private String operName;

    @Schema(name = "状态")
    private DataStatus status;

    @Schema(name = "请求参数")
    private Map<String, Object> params = new HashMap<>();

    public LocalDateTime getBeginTime() {
        Object beginTime = params.get("beginTime");
        if (beginTime != null) {
            return DateTime_.parseDateTime(beginTime.toString());
        }
        return null;
    }

    public LocalDateTime getEndTime() {
        Object endTime = params.get("endTime");
        if (endTime != null) {
            return DateTime_.parseDateTime(endTime.toString());
        }
        return null;
    }
}
