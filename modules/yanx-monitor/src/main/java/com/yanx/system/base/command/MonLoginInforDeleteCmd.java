package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.system.base.domain.entity.MonLoginInfor;
import com.yanx.system.base.infra.mapper.MonLoginInforMapper;
import com.yanx.system.base.model.dto.MonLoginInforDto;

import java.util.Collection;

/**
 * 系统访问记录 删除命令
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
public class MonLoginInforDeleteCmd extends CrudCommand<MonLoginInforDto> {

    /**
     * 删除
     *
     * @param id
     */
    public MonLoginInforDeleteCmd(Long id) {
        super(id);
    }

    /**
     * 批量删除
     *
     * @param ids
     */
    public MonLoginInforDeleteCmd(Collection<Long> ids) {
        super(ids);
    }

}