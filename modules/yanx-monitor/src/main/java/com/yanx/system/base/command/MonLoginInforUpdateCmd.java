package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.system.base.domain.entity.MonLoginInfor;
import com.yanx.system.base.infra.mapper.MonLoginInforMapper;
import com.yanx.system.base.model.dto.MonLoginInforDto;

/**
 * 系统访问记录 修改命令
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
public class MonLoginInforUpdateCmd extends CrudCommand<MonLoginInforDto> {

    /**
     * 修改
     *
     * @param id
    * @param monLoginInforDto
     */
    public MonLoginInforUpdateCmd(Long id, MonLoginInforDto monLoginInforDto) {
        super(id, monLoginInforDto);
    }

    /**
     * 修改时实体类转换
     *
     * @param entity
     * @return
     */
    @Override
    public BaseEntity toEntity(BaseEntity entity) {
        return MonLoginInforMapper.INSTANCE.toMonLoginInfor(this.getDto(), (MonLoginInfor) entity);
    }

}