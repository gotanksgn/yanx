package com.yanx.system.web.api;

import com.yanx.common.annotation.ApiResult;
import com.yanx.common.annotation.OperationLog;
import com.yanx.common.annotation.Pagination;
import com.yanx.common.core.api.BaseApi;
import com.yanx.common.enums.BusinessType;
import com.yanx.common.security.preauthorize.PreAuthorize;
import com.yanx.system.base.command.MonLoginInforDeleteCmd;
import com.yanx.system.base.model.qo.MonLoginInforQo;
import com.yanx.system.base.model.vo.MonLoginInforVo;
import com.yanx.system.base.query.MonLoginInforListQry;
import com.yanx.system.web.command.MonLoginInforCleanCmd;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 系统访问记录 接口
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@Tag(name = "系统访问记录", description = "系统访问记录")
@RestController
@RequestMapping(value = "/monitor/logininfor")
public class MonitorLoginInforApi extends BaseApi {


    @PreAuthorize(hasPermi = "monitor:logininfor:list")
    @Pagination(total = true)
    @ApiResult
    @Operation(summary = "获取系统访问记录列表")
    @GetMapping("/list")
    public List<MonLoginInforVo> list(MonLoginInforQo monLoginInforQo) {
        return queryExecutor.execute(new MonLoginInforListQry(monLoginInforQo));
    }

//    @Log(title = "登录日志", businessType = BusinessType.EXPORT)
//    @PreAuthorize("@ss.hasPermi('monitor:logininfor:export')")
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, SysLogininfor logininfor) {
//        List<SysLogininfor> list = logininforService.selectLogininforList(logininfor);
//        ExcelUtil<SysLogininfor> util = new ExcelUtil<SysLogininfor>(SysLogininfor.class);
//        util.exportExcel(response, list, "登录日志");
//    }

    @PreAuthorize(hasPermi = "monitor:logininfor:remove")
    @ApiResult
    @Operation(summary = "删除系统访问记录")
    @OperationLog(title = "登录日志", businessType = BusinessType.DELETE)
    @DeleteMapping("/{infoIds}")
    public void remove(@PathVariable Long[] infoIds) {
        commandExecutor.execute(new MonLoginInforDeleteCmd(Arrays.asList(infoIds)));
    }

    @PreAuthorize(hasPermi = "monitor:logininfor:remove")
    @ApiResult
    @Operation(summary = "清空系统访问记录")
    @OperationLog(title = "登录日志", businessType = BusinessType.CLEAN)
    @DeleteMapping("/clean")
    public void clean() {
        commandExecutor.execute(new MonLoginInforCleanCmd());
    }

//    @PreAuthorize(hasPermi = "monitor:logininfor:unlock")
//    @ApiResult
//    @Operation(summary = "账户解锁")
//    @OperationLog(title = "账户解锁", businessType = BusinessType.OTHER)
//    @GetMapping("/unlock/{userName}")
//    public void unlock(@PathVariable("userName") String userName) {
//        passwordService.clearLoginRecordCache(userName);
//    }
}