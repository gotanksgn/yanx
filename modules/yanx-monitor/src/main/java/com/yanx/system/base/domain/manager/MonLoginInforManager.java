package com.yanx.system.base.domain.manager;

import com.yanx.common.core.domain.manager.BaseManager;
import com.yanx.system.base.domain.entity.MonLoginInfor;
import com.yanx.system.base.domain.repository.MonLoginInforRepository;
import org.springframework.stereotype.Service;

/**
 * 系统访问记录领域服务
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@Service
public class MonLoginInforManager extends BaseManager<MonLoginInfor, MonLoginInforRepository> {
    /**
     * 清空系统访问记录
     */
    public void clean() {
        repository.trancate();
    }
}
