package com.yanx.system.base.domain.repository;

import com.yanx.common.core.domain.repository.BaseRepository;
import com.yanx.system.base.domain.entity.MonLoginInfor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.io.Serializable;

/**
 * 系统访问记录仓库
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
public interface MonLoginInforRepository extends BaseRepository<MonLoginInfor, Serializable> {

    @Modifying
    @Query(value = "truncate table SYS_LOGININFOR", nativeQuery = true)
    void trancate();
}
