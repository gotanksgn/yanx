package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.system.base.domain.entity.MonOperLog;
import com.yanx.system.base.infra.mapper.MonOperLogMapper;
import com.yanx.system.base.model.dto.MonOperLogDto;

import java.util.Collection;

/**
 * 操作日志 删除命令
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
public class MonOperLogDeleteCmd extends CrudCommand<MonOperLogDto> {

    /**
     * 删除
     *
     * @param id
     */
    public MonOperLogDeleteCmd(Long id) {
        super(id);
    }

    /**
     * 批量删除
     *
     * @param ids
     */
    public MonOperLogDeleteCmd(Collection<Long> ids) {
        super(ids);
    }

}