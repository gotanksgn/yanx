package com.yanx.system.web.command;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.VoidCommand;
import com.yanx.system.base.domain.manager.MonLoginInforManager;
import lombok.AllArgsConstructor;

/**
 * 清空登录日志
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@AllArgsConstructor
public class MonLoginInforCleanCmd extends VoidCommand {

    @Override
    public void handler(Executor executor) {
        MonLoginInforManager manager = executor.getReceiver(MonLoginInforManager.class);
        manager.clean();
    }
}