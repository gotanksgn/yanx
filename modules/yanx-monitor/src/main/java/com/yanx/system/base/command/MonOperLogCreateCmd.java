package com.yanx.system.base.command;

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.system.base.infra.mapper.MonOperLogMapper;
import com.yanx.system.base.model.dto.MonOperLogDto;

/**
 * 操作日志 新增命令
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
public class MonOperLogCreateCmd extends CrudCommand<MonOperLogDto> {

    /**
     * 新增
     *
     * @param monOperLogDto
     */
    public MonOperLogCreateCmd(MonOperLogDto monOperLogDto) {
        super(monOperLogDto);
    }

    /**
     * 新增时实体类转换
     *
     * @return
     */
    @Override
    public BaseEntity toEntity() {
        return MonOperLogMapper.INSTANCE.toMonOperLog(this.getDto());
    }

}