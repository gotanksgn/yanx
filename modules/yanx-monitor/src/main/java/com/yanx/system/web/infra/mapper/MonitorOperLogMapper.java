package com.yanx.system.web.infra.mapper;

import com.yanx.common.core.dto.OperationLogDto;
import com.yanx.system.base.model.dto.MonOperLogDto;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
 * 操作日志 实体转换类
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface MonitorOperLogMapper {

    MonitorOperLogMapper INSTANCE = Mappers.getMapper(MonitorOperLogMapper.class);

    MonOperLogDto toMonOperLogDto(OperationLogDto dto);

}
