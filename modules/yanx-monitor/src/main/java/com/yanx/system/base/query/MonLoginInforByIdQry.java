package com.yanx.system.base.query;

import com.querydsl.core.types.Projections;
import com.querydsl.core.types.QBean;
import com.yanx.common.command.Executor;
import com.yanx.common.core.command.QryCommand;
import com.yanx.common.exception.BusinessException;
import com.yanx.system.base.domain.entity.QMonLoginInfor;
import com.yanx.system.base.model.vo.MonLoginInforVo;
import lombok.AllArgsConstructor;

/**
 * 根据ID查询系统访问记录
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@AllArgsConstructor
public class MonLoginInforByIdQry extends QryCommand<MonLoginInforVo> {

    private Long id;

    @Override
    public MonLoginInforVo execute(Executor executor) {
        if (id == null) {
            throw new BusinessException("系统访问记录ID不能为空");
        }
        QMonLoginInfor monLoginInfor = QMonLoginInfor.monLoginInfor;
        return queryFactory.select(this.fields())
                .from(monLoginInfor)
                .where(monLoginInfor.deleted.eq(false), monLoginInfor.id.eq(id))
                .fetchOne();
    }

    /**
     * 系统访问记录VO映射
     *
     * @return QBean<MonLoginInforVo>
     */
    public static QBean<MonLoginInforVo> fields() {
        QMonLoginInfor monLoginInfor = QMonLoginInfor.monLoginInfor;
        return Projections.fields(
            MonLoginInforVo.class,
            monLoginInfor.userName,
            monLoginInfor.status,
            monLoginInfor.ipaddr,
            monLoginInfor.loginLocation,
            monLoginInfor.browser,
            monLoginInfor.os,
            monLoginInfor.msg,
            monLoginInfor.loginTime,
            monLoginInfor.id
        );
    }
}