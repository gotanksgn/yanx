package com.yanx.system.base.model.qo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yanx.common.enums.DataStatus;
import com.yanx.common.utils.DateTime_;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * 系统访问记录QO
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "系统访问记录查询模型")
public class MonLoginInforQo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(name = "用户账号")
    private String userName;

    @Schema(name = "登录状态")
    private DataStatus status;

    @Schema(name = "登录地址")
    private String ipaddr;

    @Schema(name = "请求参数")
    private Map<String, Object> params = new HashMap<>();

    public LocalDateTime getBeginTime() {
        Object beginTime = params.get("beginTime");
        if (beginTime != null) {
            return DateTime_.parseDateTime(beginTime.toString());
        }
        return null;
    }

    public LocalDateTime getEndTime() {
        Object endTime = params.get("endTime");
        if (endTime != null) {
            return DateTime_.parseDateTime(endTime.toString());
        }
        return null;
    }
}
