package com.yanx.system.base.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yanx.common.core.dto.BaseDto;
import com.yanx.common.enums.BusinessType;
import com.yanx.common.enums.DataStatus;
import com.yanx.common.enums.OperatorType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 操作日志DTO
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "操作日志入参模型")
public class MonOperLogDto extends BaseDto<Long> {

    private static final long serialVersionUID = 1L;

    @Schema(name = "操作模块")
    private String title;

    @Schema(name = "业务类型")
    private BusinessType businessType;

    @Schema(name = "请求方法")
    private String method;

    @Schema(name = "请求方式")
    private String requestMethod;

    @Schema(name = "操作类别")
    private OperatorType operatorType;

    @Schema(name = "操作人员")
    private String operName;

    @Schema(name = "部门名称")
    private String deptName;

    @Schema(name = "请求地址")
    private String operUrl;

    @Schema(name = "操作地址")
    private String operIp;

    @Schema(name = "操作地点")
    private String operLocation;

    @Schema(name = "请求参数")
    private String operParam;

    @Schema(name = "返回参数")
    private String jsonResult;

    @Schema(name = "状态")
    private DataStatus status;

    @Schema(name = "错误消息")
    private String errorMsg;

    @Schema(name = "操作时间")
    private LocalDateTime operTime;

}
