package com.yanx.system.base.domain.entity;

import com.yanx.codegen.annotation.CodeGenClass;
import com.yanx.codegen.annotation.CodeGenField;
import com.yanx.common.core.domain.entity.BaseEntity;
import com.yanx.common.enums.BusinessType;
import com.yanx.common.enums.DataStatus;
import com.yanx.common.enums.OperatorType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;


/**
 * 操作日志表 SYS_OPER_LOG
 *
 * @author gotanks
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "SYS_OPER_LOG")
@CodeGenClass(value = "操作日志", author = "gotanks")
public class MonOperLog extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 操作模块
     */
    @CodeGenField("操作模块")
    private String title;

    /**
     * 业务类型（0=其它,1=新增,2=修改,3=删除,4=授权,5=导出,6=导入,7=强退,8=生成代码,9=清空数据）
     */
    @CodeGenField("业务类型")
    private BusinessType businessType;

    /**
     * 请求方法
     */
    @CodeGenField("请求方法")
    private String method;

    /**
     * 请求方式
     */
    @CodeGenField("请求方式")
    private String requestMethod;

    /**
     * 操作类别（0其它 1后台用户 2手机端用户）
     */
    @CodeGenField("操作类别")
    private OperatorType operatorType;

    /**
     * 操作人员
     */
    @CodeGenField("操作人员")
    private String operName;

    /**
     * 部门名称
     */
    @CodeGenField("部门名称")
    private String deptName;

    /**
     * 请求url
     */
    @CodeGenField("请求地址")
    private String operUrl;

    /**
     * 操作地址
     */
    @CodeGenField("操作地址")
    private String operIp;

    /**
     * 操作地点
     */
    @CodeGenField("操作地点")
    private String operLocation;

    /**
     * 请求参数
     */
    @Column(length = 2000)
    @CodeGenField("请求参数")
    private String operParam;

    /**
     * 返回参数
     */
    @Column(length = 2000)
    @CodeGenField("返回参数")
    private String jsonResult;

    /**
     * 操作状态（0正常 1异常）
     */
    @CodeGenField("状态")
    private DataStatus status;

    /**
     * 错误消息
     */
    @Column(length = 2000)
    @CodeGenField("错误消息")
    private String errorMsg;

    /**
     * 操作时间
     */
    @Column(updatable = false)
    @CreationTimestamp
    @CodeGenField("操作时间")
    private LocalDateTime operTime;

}
