package com.yanx.system.base.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yanx.common.core.dto.BaseDto;
import com.yanx.common.enums.DataStatus;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 系统访问记录DTO
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(name = "系统访问记录入参模型")
public class MonLoginInforDto extends BaseDto<Long> {

    private static final long serialVersionUID = 1L;

    @Schema(name = "用户账号")
    private String userName;

    @Schema(name = "登录状态")
    private DataStatus status;

    @Schema(name = "登录地址")
    private String ipaddr;

    @Schema(name = "登录地点")
    private String loginLocation;

    @Schema(name = "浏览器")
    private String browser;

    @Schema(name = "操作系统")
    private String os;

    @Schema(name = "提示消息")
    private String msg;

    @Schema(name = "访问时间")
    private LocalDateTime loginTime;

}
