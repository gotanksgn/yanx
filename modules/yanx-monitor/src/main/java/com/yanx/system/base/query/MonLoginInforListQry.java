package com.yanx.system.base.query;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.yanx.common.command.Executor;
import com.yanx.common.core.command.PageQryCommand;
import com.yanx.system.base.domain.entity.QMonLoginInfor;
import com.yanx.system.base.model.qo.MonLoginInforQo;
import com.yanx.system.base.model.vo.MonLoginInforVo;
import lombok.AllArgsConstructor;

import java.util.List;

/**
 * 系统访问记录分页查询
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@AllArgsConstructor
public class MonLoginInforListQry extends PageQryCommand<List<MonLoginInforVo>> {

    private MonLoginInforQo monLoginInforQo;

    @Override
    public List<MonLoginInforVo> execute(Executor executor) {
        QMonLoginInfor monLoginInfor = QMonLoginInfor.monLoginInfor;

        //查询条件
        BooleanBuilder condition = new BooleanBuilder();
        condition.and(monLoginInfor.deleted.eq(false));
        if (monLoginInforQo != null) {
            //自定义查询条件
            if (monLoginInforQo.getIpaddr() != null && !monLoginInforQo.getIpaddr().equals("")) {
                condition.and(monLoginInfor.ipaddr.contains(monLoginInforQo.getIpaddr()));
            }
            if (monLoginInforQo.getStatus() != null) {
                condition.and(monLoginInfor.status.eq(monLoginInforQo.getStatus()));
            }
            if (monLoginInforQo.getUserName() != null && !monLoginInforQo.getUserName().equals("")) {
                condition.and(monLoginInfor.userName.contains(monLoginInforQo.getUserName()));
            }
            if (monLoginInforQo.getBeginTime() != null) {
                condition.and(monLoginInfor.loginTime.gt(monLoginInforQo.getBeginTime()));
            }
            if (monLoginInforQo.getEndTime() != null) {
                condition.and(monLoginInfor.loginTime.lt(monLoginInforQo.getEndTime()));
            }
        }

        JPAQuery<MonLoginInforVo> query = queryFactory.select(MonLoginInforByIdQry.fields())
                .from(monLoginInfor)
                .where(condition)
                .orderBy(monLoginInfor.id.desc());

        return this.pageList(query);
    }

}