package com.yanx.system.web.command;

import com.yanx.common.command.Executor;
import com.yanx.common.core.command.VoidCommand;
import com.yanx.system.base.domain.manager.MonOperLogManager;
import lombok.AllArgsConstructor;

/**
 * 清空操作日志
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@AllArgsConstructor
public class MonOperLogCleanCmd extends VoidCommand {

    @Override
    public void handler(Executor executor) {
        MonOperLogManager monOperLogManager = executor.getReceiver(MonOperLogManager.class);
        monOperLogManager.clean();
    }
}