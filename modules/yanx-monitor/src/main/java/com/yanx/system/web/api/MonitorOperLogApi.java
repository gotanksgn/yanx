package com.yanx.system.web.api;

import com.yanx.common.annotation.ApiResult;
import com.yanx.common.annotation.OperationLog;
import com.yanx.common.annotation.Pagination;
import com.yanx.common.core.api.BaseApi;
import com.yanx.common.enums.BusinessType;
import com.yanx.common.security.preauthorize.PreAuthorize;
import com.yanx.system.base.command.MonOperLogDeleteCmd;
import com.yanx.system.base.model.qo.MonOperLogQo;
import com.yanx.system.base.model.vo.MonOperLogVo;
import com.yanx.system.base.query.MonOperLogListQry;
import com.yanx.system.web.command.MonOperLogCleanCmd;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 操作日志 接口
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@Tag(name = "操作日志", description = "操作日志")
@RestController
@RequestMapping(value = "/monitor/operlog")
public class MonitorOperLogApi extends BaseApi {

    @PreAuthorize(hasPermi = "monitor:operlog:list")
    @Pagination(total = true)
    @ApiResult
    @Operation(summary = "获取操作日志列表")
    @GetMapping("/list")
    public List<MonOperLogVo> list(MonOperLogQo monOperLogQo) {
        return queryExecutor.execute(new MonOperLogListQry(monOperLogQo));
    }

//    @OperationLog(title = "操作日志", businessType = BusinessType.EXPORT)
//    @PreAuthorize(hasPermi = "monitor:operlog:export")
//    @ApiResult
//    @Operation(summary = "导出操作日志列表")
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, SysOperLog operLog) {
//        List<SysOperLog> list = operLogService.selectOperLogList(operLog);
//        ExcelUtil<SysOperLog> util = new ExcelUtil<SysOperLog>(SysOperLog.class);
//        util.exportExcel(response, list, "操作日志");
//    }

    @OperationLog(title = "操作日志", businessType = BusinessType.DELETE)
    @PreAuthorize(hasPermi = "monitor:operlog:remove")
    @ApiResult
    @Operation(summary = "删除操作日志列表")
    @DeleteMapping("/{operIds}")
    public void remove(@PathVariable Long[] operIds) {
        commandExecutor.execute(new MonOperLogDeleteCmd(Arrays.asList(operIds)));
    }

    @OperationLog(title = "操作日志", businessType = BusinessType.CLEAN)
    @PreAuthorize(hasPermi = "monitor:operlog:remove")
    @ApiResult
    @Operation(summary = "清空操作日志列表")
    @DeleteMapping("/clean")
    public void clean() {
        commandExecutor.execute(new MonOperLogCleanCmd());
    }
}