package com.yanx.system.base.domain.repository;

import com.yanx.common.core.domain.repository.BaseRepository;
import com.yanx.system.base.domain.entity.MonOperLog;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.io.Serializable;

/**
 * 操作日志仓库
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
public interface MonOperLogRepository extends BaseRepository<MonOperLog, Serializable> {
    @Modifying
    @Query(value = "truncate table SYS_OPER_LOG", nativeQuery = true)
    void trancate();
}
