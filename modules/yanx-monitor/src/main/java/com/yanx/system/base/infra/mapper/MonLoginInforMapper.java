package com.yanx.system.base.infra.mapper;

import com.yanx.system.base.model.dto.MonLoginInforDto;
import com.yanx.system.base.domain.entity.MonLoginInfor;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
 * 系统访问记录 实体转换类
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface MonLoginInforMapper {

    MonLoginInforMapper INSTANCE = Mappers.getMapper(MonLoginInforMapper.class);

    MonLoginInfor toMonLoginInfor(MonLoginInforDto dto);

    MonLoginInfor toMonLoginInfor(MonLoginInforDto dto, @MappingTarget MonLoginInfor entity);

}
