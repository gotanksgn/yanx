package com.yanx.system.base.query;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.yanx.common.command.Executor;
import com.yanx.common.core.command.PageQryCommand;
import com.yanx.system.base.domain.entity.QMonOperLog;
import com.yanx.system.base.model.qo.MonOperLogQo;
import com.yanx.system.base.model.vo.MonOperLogVo;
import lombok.AllArgsConstructor;

import java.util.List;

/**
 * 操作日志分页查询
 *
 * @author: gotanks
 * @create: 2023-6-2
 */
@AllArgsConstructor
public class MonOperLogListQry extends PageQryCommand<List<MonOperLogVo>> {

    private MonOperLogQo monOperLogQo;

    @Override
    public List<MonOperLogVo> execute(Executor executor) {
        QMonOperLog monOperLog = QMonOperLog.monOperLog;

        //查询条件
        BooleanBuilder condition = new BooleanBuilder();
        condition.and(monOperLog.deleted.eq(false));
        if (monOperLogQo != null) {
            //自定义查询条件
            if (monOperLogQo.getTitle() != null && !monOperLogQo.getTitle().equals("")) {
                condition.and(monOperLog.title.contains(monOperLogQo.getTitle()));
            }
            if (monOperLogQo.getBusinessType() != null) {
                condition.and(monOperLog.businessType.eq(monOperLogQo.getBusinessType()));
            }
            if (monOperLogQo.getBusinessTypes() != null && monOperLogQo.getBusinessTypes().length > 0) {
                condition.and(monOperLog.businessType.in(monOperLogQo.getBusinessTypes()));
            }
            if (monOperLogQo.getStatus() != null) {
                condition.and(monOperLog.status.eq(monOperLogQo.getStatus()));
            }
            if (monOperLogQo.getOperName() != null && !monOperLogQo.getOperName().equals("")) {
                condition.and(monOperLog.operName.contains(monOperLogQo.getOperName()));
            }
            if (monOperLogQo.getBeginTime() != null) {
                condition.and(monOperLog.operTime.gt(monOperLogQo.getBeginTime()));
            }
            if (monOperLogQo.getEndTime() != null) {
                condition.and(monOperLog.operTime.lt(monOperLogQo.getEndTime()));
            }
        }

        JPAQuery<MonOperLogVo> query = queryFactory.select(MonOperLogByIdQry.fields())
                .from(monOperLog)
                .where(condition)
                .orderBy(monOperLog.id.desc());

        return this.pageList(query);
    }

}