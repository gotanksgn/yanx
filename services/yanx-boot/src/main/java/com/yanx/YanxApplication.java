package com.yanx;

import com.yanx.common.core.BaseApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YanxApplication extends BaseApplication {

    public static void main(String[] args) {
        startApplication(YanxApplication.class, args);
    }

}
