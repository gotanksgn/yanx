package ${queryPackage};

import com.querydsl.core.types.Projections;
import com.querydsl.core.types.QBean;
import com.yanx.common.command.Executor;
import com.yanx.common.core.command.QryCommand;
import com.yanx.common.exception.BusinessException;
import ${entityPackage}.Q${className};
import ${voPackage}.${className}Vo;
import lombok.AllArgsConstructor;

/**
<#if labelName!="">
 * 根据ID查询${labelName}
 *
</#if>
 * @author: ${author}
 * @create: ${.now?date}
 */
@AllArgsConstructor
public class ${className}ByIdQry extends QryCommand<${className}Vo> {

    private <#if (idType=="String")>String<#else>Long</#if> id;

    @Override
    public ${className}Vo execute(Executor executor) {
        if (id == null) {
            throw new BusinessException("${labelName}ID不能为空");
        }
        Q${className} ${className?uncap_first} = Q${className}.${className?uncap_first};
        return queryFactory.select(this.fields())
                .from(${className?uncap_first})
                .where(${className?uncap_first}.id.eq(id))
                .fetchOne();
    }

    /**
     * ${labelName}VO映射
     *
     * @return QBean<${className}Vo>
     */
    public static QBean<${className}Vo> fields() {
        Q${className} ${className?uncap_first} = Q${className}.${className?uncap_first};
        return Projections.fields(
            ${className}Vo.class,
            <#list voFields as field>
            ${className?uncap_first}.${field.fieldName},
            </#list>
            ${className?uncap_first}.id
        );
    }
}