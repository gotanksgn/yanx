package ${utilsPackage};

import com.querydsl.core.types.Projections;
import com.querydsl.core.types.QBean;
import ${entityPackage}.*;
import ${voPackage}.*;

/**
<#if labelName!="">
 * QueryDSL VO映射类
 *
</#if>
 * @author: ${author}
 * @create: ${.now?date}
 */
public class ProjectionsUtil {

<#list classList as clz>
    /**
     * ${clz.labelName}VO映射
     *
     * @return QBean<${clz.className}Vo>
     */
    public static QBean<${clz.className}Vo> ${clz.className?uncap_first}() {
        Q${clz.className} ${clz.className?uncap_first} = Q${clz.className}.${clz.className?uncap_first};
        return Projections.fields(
                ${clz.className}Vo.class,
            <#list clz.voFields as field>
                ${clz.className?uncap_first}.${field.fieldName},
            </#list>
                ${clz.className?uncap_first}.id
        );
    }

</#list>
}