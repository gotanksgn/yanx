package ${mapperPackage};

import ${dtoPackage}.${className}Dto;
import ${entityPackage}.${className};
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
<#if labelName!="">
 * ${labelName} 实体转换类
 *
</#if>
 * @author: ${author}
 * @create: ${.now?date}
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface ${className}Mapper {

    ${className}Mapper INSTANCE = Mappers.getMapper(${className}Mapper.class);

    ${className} to${className}(${className}Dto dto);

    ${className} to${className}(${className}Dto dto, @MappingTarget ${className} entity);

}
