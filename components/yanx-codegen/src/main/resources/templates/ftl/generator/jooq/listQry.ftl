package ${queryPackage};

import command.com.yx.common.Executor;
import command.core.com.yx.common.CommonQry;
import tools.com.yx.common.String_;
import ${qoPackage}.${className}Qo;
import ${voPackage}.${className}Vo;
import ${basePackage}.infra.jooq.Tables;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jooq.Record;
import org.jooq.SelectQuery;

import java.util.List;

/**
<#if labelName!="">
 * ${labelName}分页查询
 *
</#if>
 * @author: ${author}
 * @create: ${.now?date}
 */
@NoArgsConstructor
@AllArgsConstructor
public class ${className}ListQry extends CommonQry<List<${className}Vo>> {

    private ${className}Qo ${className?uncap_first}Qo;

    @Override
    public List<${className}Vo> execute(Executor executor) {
        SelectQuery<Record> query = create.selectQuery();
        query.addFrom(Tables.${tableName});
        query.addConditions(Tables.${tableName}.DELETED.eq(false));

        if (${className?uncap_first}Qo != null) {
            <#list qoFields as field>
            if (String_.isNotBlank(${className?uncap_first}Qo.get${field.fieldName?cap_first}())) {
                query.addConditions(Tables.${tableName}.${field.fieldName?replace("([a-z])([A-Z]+)","$1_$2","r")?upper_case}.eq(${className?uncap_first}Qo.get${field.fieldName?cap_first}()));
            }
            </#list>
        }

        this.paginate(query);

        return query.fetchInto(${className}Vo.class);
    }

}