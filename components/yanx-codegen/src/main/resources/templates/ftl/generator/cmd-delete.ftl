package ${commandPackage};

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import ${entityPackage}.${className};
import ${mapperPackage}.${className}Mapper;
import ${dtoPackage}.${className}Dto;

import java.util.Collection;

/**
<#if labelName!="">
 * ${labelName} 删除命令
 *
</#if>
 * @author: ${author}
 * @create: ${.now?date}
 */
public class ${className}DeleteCmd extends CrudCommand<${className}Dto> {

    /**
     * 删除
     *
     * @param id
     */
    public ${className}DeleteCmd(<#if (idType=="String")>String<#else>Long</#if> id) {
        super(id);
    }

    /**
     * 批量删除
     *
     * @param ids
     */
    public ${className}DeleteCmd(Collection<<#if (idType=="String")>String<#else>Long</#if>> ids) {
        super(ids);
    }

}