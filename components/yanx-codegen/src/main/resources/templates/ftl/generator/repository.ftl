package ${repositoryPackage};

import com.yanx.common.core.domain.repository.BaseRepository;
import ${entityPackage}.${className};

import java.io.Serializable;

/**
<#if labelName!="">
 * ${labelName}仓库
 *
</#if>
 * @author: ${author}
 * @create: ${.now?date}
 */
public interface ${className}Repository extends BaseRepository<${className}, Serializable> {

}
