package ${qoPackage};

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
<#list qoFields as field>
    <#if field.fieldType=="BigDecimal">
import java.math.BigDecimal;
        <#break>
    </#if>
</#list>
<#list qoFields as field>
    <#if field.fieldType=="LocalDate">
import java.time.${field.fieldType};
        <#break>
    </#if>
</#list>
<#list qoFields as field>
    <#if field.fieldType=="LocalDateTime">
import java.time.${field.fieldType};
        <#break>
    </#if>
</#list>
<#list qoFields as field>
    <#if field.fieldType=="LocalTime">
import java.time.${field.fieldType};
        <#break>
    </#if>
</#list>
<#list qoFields as field>
    <#if field.fieldType=="Date">
import java.util.Date;
        <#break>
    </#if>
</#list>

/**
<#if labelName!="">
 * ${labelName}QO
 *
</#if>
 * @author: ${author}
 * @create: ${.now?date}
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
<#if labelName!="">
@Schema(title = "${labelName}查询模型")
</#if>
public class ${className}Qo implements Serializable {

    private static final long serialVersionUID = 1L;

<#list qoFields as field>
    <#if field.fieldRemark!="">
    @Schema(title = "${field.fieldRemark}")
    </#if>
    private ${field.fieldType} ${field.fieldName};

</#list>
}
