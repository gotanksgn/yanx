package ${voPackage};

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yanx.common.core.vo.BasePageVo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

<#list voFields as field>
    <#if field.fieldType=="BigDecimal">
import java.math.BigDecimal;
        <#break>
    </#if>
</#list>
<#list voFields as field>
    <#if field.fieldType=="LocalDate">
import java.time.${field.fieldType};
        <#break>
    </#if>
</#list>
<#list voFields as field>
    <#if field.fieldType=="LocalDateTime">
import java.time.${field.fieldType};
        <#break>
    </#if>
</#list>
<#list voFields as field>
    <#if field.fieldType=="LocalTime">
import java.time.${field.fieldType};
        <#break>
    </#if>
</#list>
<#list voFields as field>
    <#if field.fieldType=="Date">
import java.util.Date;
        <#break>
    </#if>
</#list>

/**
<#if labelName!="">
 * ${labelName}VO
 *
</#if>
 * @author: ${author}
 * @create: ${.now?date}
 */
@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
<#if labelName!="">
@Schema(title = "${labelName}视图模型")
</#if>
public class ${className}Vo extends BasePageVo<<#if (idType=="String")>String<#else>Long</#if>> {

    private static final long serialVersionUID = 1L;

<#list voFields as field>
    <#if field.fieldRemark!="">
    @Schema(title = "${field.fieldRemark}")
    </#if>
    private ${field.fieldType} ${field.fieldName};
    
</#list>
}
