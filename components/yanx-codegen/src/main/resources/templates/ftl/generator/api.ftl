package ${apiPackage};

import com.yanx.common.annotation.ApiResult;
import com.yanx.common.annotation.Pagination;
import com.yanx.common.core.api.BaseApi;
import ${commandPackage}.${className}CreateCmd;
import ${commandPackage}.${className}DeleteCmd;
import ${commandPackage}.${className}UpdateCmd;
import ${dtoPackage}.${className}Dto;
import ${qoPackage}.${className}Qo;
import ${voPackage}.${className}Vo;
import ${queryPackage}.${className}ByIdQry;
import ${queryPackage}.${className}ListQry;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

/**
<#if labelName!="">
 * ${labelName} 接口
 *
</#if>
 * @author: ${author}
 * @create: ${.now?date}
 */
@Tag(name = "${labelName}", description = "${labelName}")
@RestController
@RequestMapping(value = "/api/${className?replace("([a-z])([A-Z]+)","$1-$2","r")?lower_case}")
public class ${className}Api extends BaseApi {

    @ApiResult
    @Operation(summary = "根据ID查询${labelName}")
    @GetMapping("/{id}")
    public ${className}Vo get(@PathVariable <#if (idType=="String")>String<#else>Long</#if> id) {
        return queryExecutor.execute(new ${className}ByIdQry(id));
    }

    @Pagination(total = true)
    @ApiResult
    @Operation(summary = "分页查询${labelName}")
    @GetMapping
    public List<${className}Vo> getList(${className}Qo ${className?uncap_first}Qo) {
        return queryExecutor.execute(new ${className}ListQry(${className?uncap_first}Qo));
    }

    @ApiResult
    @Operation(summary = "新增${labelName}")
    @PostMapping
    public void create(@Valid @RequestBody ${className}Dto ${className?uncap_first}Dto) {
        domainExecutor.execute(new ${className}CreateCmd(${className?uncap_first}Dto));
    }

    @ApiResult
    @Operation(summary = "更新${labelName}")
    @PutMapping("/{id}")
    public void update(@PathVariable("id") <#if (idType=="String")>String<#else>Long</#if> id, @Valid @RequestBody ${className}Dto ${className?uncap_first}Dto) {
        domainExecutor.execute(new ${className}UpdateCmd(id, ${className?uncap_first}Dto));
    }

    @ApiResult
    @Operation(summary = "删除${labelName}")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") <#if (idType=="String")>String<#else>Long</#if> id) {
        domainExecutor.execute(new ${className}DeleteCmd(id));
    }

    @ApiResult
    @Operation(summary = "批量删除${labelName}")
    @DeleteMapping
    public void batchDelete(@RequestBody Set<<#if (idType=="String")>String<#else>Long</#if>> ids) {
        domainExecutor.execute(new ${className}DeleteCmd(ids));
    }
}