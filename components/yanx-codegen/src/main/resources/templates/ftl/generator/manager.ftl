package ${managerPackage};

import com.yanx.common.core.domain.manager.BaseManager;
import ${entityPackage}.${className};
import ${repositoryPackage}.${className}Repository;
import org.springframework.stereotype.Service;

/**
<#if labelName!="">
 * ${labelName}领域服务
 *
</#if>
 * @author: ${author}
 * @create: ${.now?date}
 */
@Service
public class ${className}Manager extends BaseManager<${className}, ${className}Repository> {

}
