package ${queryPackage};

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.yanx.common.command.Executor;
import com.yanx.common.core.command.PageQryCommand;
import ${entityPackage}.Q${className};
import ${qoPackage}.${className}Qo;
import ${voPackage}.${className}Vo;
import lombok.AllArgsConstructor;

import java.util.List;

/**
<#if labelName!="">
 * ${labelName}分页查询
 *
</#if>
 * @author: ${author}
 * @create: ${.now?date}
 */
@AllArgsConstructor
public class ${className}ListQry extends PageQryCommand<List<${className}Vo>> {

    private ${className}Qo ${className?uncap_first}Qo;

    @Override
    public List<${className}Vo> execute(Executor executor) {
        Q${className} ${className?uncap_first} = Q${className}.${className?uncap_first};

        //查询条件
        BooleanBuilder condition = new BooleanBuilder();
        if (${className?uncap_first}Qo != null) {
            <#list qoFields as field>
            if (String_.isNotBlank(${className?uncap_first}Qo.get${field.fieldName?cap_first}())) {
                builder.and(${className?uncap_first}.${field.fieldName?cap_first}.eq(${className?uncap_first}Qo.get${field.fieldName?cap_first}()));
            }
            </#list>
            //自定义查询条件
        }

        JPAQuery<${className}Vo> query = queryFactory.select(${className}ByIdQry.fields())
                .from(${className?uncap_first})
                .where(condition);

        return this.pageList(query);
    }

}