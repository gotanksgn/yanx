package ${queryPackage};

import command.com.yx.common.Executor;
import command.core.com.yx.common.CommonQry;
import exception.com.yx.common.BusinessException;
import ${voPackage}.${className}Vo;
import ${basePackage}.infra.jooq.Tables;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
<#if labelName!="">
 * 根据ID查询${labelName}
 *
</#if>
 * @author: ${author}
 * @create: ${.now?date}
 */
@NoArgsConstructor
@AllArgsConstructor
public class ${className}ByIdQry extends CommonQry<${className}Vo> {

    private <#if (idType=="String")>String<#else>Long</#if> id;

    @Override
    public ${className}Vo execute(Executor executor) {
        if (id == null) {
            throw new BusinessException("${labelName}ID不能为空");
        }
        return create.selectFrom(Tables.${tableName})
                .where(Tables.${tableName}.DELETED.eq(false))
                .and(Tables.${tableName}.ID.eq(id))
                .fetchOneInto(${className}Vo.class);
    }

}