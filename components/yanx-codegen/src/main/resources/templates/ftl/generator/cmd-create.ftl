package ${commandPackage};

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import ${mapperPackage}.${className}Mapper;
import ${dtoPackage}.${className}Dto;

/**
<#if labelName!="">
 * ${labelName} 新增命令
 *
</#if>
 * @author: ${author}
 * @create: ${.now?date}
 */
public class ${className}CreateCmd extends CrudCommand<${className}Dto> {

    /**
     * 新增
     *
     * @param ${className?uncap_first}Dto
     */
    public ${className}CreateCmd(${className}Dto ${className?uncap_first}Dto) {
        super(${className?uncap_first}Dto);
    }

    /**
     * 新增时实体类转换
     *
     * @return
     */
    @Override
    public BaseEntity toEntity() {
        return ${className}Mapper.INSTANCE.to${className}(this.getDto());
    }

}