package ${commandPackage};

import com.yanx.common.core.command.CrudCommand;
import com.yanx.common.core.domain.entity.BaseEntity;
import ${entityPackage}.${className};
import ${mapperPackage}.${className}Mapper;
import ${dtoPackage}.${className}Dto;

/**
<#if labelName!="">
 * ${labelName} 修改命令
 *
</#if>
 * @author: ${author}
 * @create: ${.now?date}
 */
public class ${className}UpdateCmd extends CrudCommand<${className}Dto> {

    /**
     * 修改
     *
     * @param id
    * @param ${className?uncap_first}Dto
     */
    public ${className}UpdateCmd(<#if (idType=="String")>String<#else>Long</#if> id, ${className}Dto ${className?uncap_first}Dto) {
        super(id, ${className?uncap_first}Dto);
    }

    /**
     * 修改时实体类转换
     *
     * @param entity
     * @return
     */
    @Override
    public BaseEntity toEntity(BaseEntity entity) {
        return ${className}Mapper.INSTANCE.to${className}(this.getDto(), (${className}) entity);
    }

}