package com.yanx.codegen;

/**
 * 自动生成类的类型
 *
 * @author gotanks
 */
public enum CodeGenType {
    /**
     * 领域仓库，通过SpringDataJpa与数据库交互
     */
    Repository,

    /**
     * 领域服务，业务逻辑层
     */
    Manager,

    /**
     * 命令，用于调度领域服务
     */
    Cmd,

    /**
     * 控制层，相当于Controller
     */
    Api,

    /**
     * 模型转换类，基于Mapstruct
     */
    Mapper,

    /**
     * 查询命令（根据ID查询Vo、根据Qo查询List）
     */
    Qry,

    /**
     * 传输模型，入参
     */
    Dto,

    /**
     * 视图模型，回参
     */
    Vo,

    /**
     * 查询模型，查询入参
     */
    Qo,
    ;

    /**
     * 所有类型
     *
     * @return
     */
    public static CodeGenType[] ALL() {
        return CodeGenType.values();
    }

    /**
     * 领域类型，包括Repository、Manager
     *
     * @return
     */
    public static CodeGenType[] DOMAIN() {
        return new CodeGenType[]{Repository, Manager};
    }

    /**
     * 模型类型，包括Qo、Dto、Vo
     *
     * @return
     */
    public static CodeGenType[] MODEL() {
        return new CodeGenType[]{Qo, Dto, Vo};
    }

    /**
     * 命令类型，包括Cmd、ByIdQry、ListQry、Mapper，以及DOMAIN和MODEL包含的所有类型
     *
     * @return
     */
    public static CodeGenType[] COMMAND() {
        return new CodeGenType[]{Cmd, Qry, Mapper, Qo, Dto, Vo, Repository, Manager};
    }
}
