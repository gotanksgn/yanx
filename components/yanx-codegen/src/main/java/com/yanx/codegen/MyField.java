package com.yanx.codegen;

import lombok.Data;

/**
 * @author gotanks
 * @date 2021-08-21 22:48:00
 */
@Data
public class MyField {
    //字段名
    private String fieldName = "";

    //字段类型
    private String fieldType = "";

    //字段注释
    private String fieldRemark = "";

}
