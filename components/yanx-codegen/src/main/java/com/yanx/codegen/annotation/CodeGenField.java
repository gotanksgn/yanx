package com.yanx.codegen.annotation;

import java.lang.annotation.*;

/**
 * 代码生成注解
 *
 * @author gotanks
 * @date 2021-08-22 21:53:00
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CodeGenField {

    String value();

    boolean dto() default true;

    boolean vo() default true;

    boolean qo() default false;

}
