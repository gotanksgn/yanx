package com.yanx.codegen.annotation;

import java.lang.annotation.*;

/**
 * 代码生成注解
 *
 * @author gotanks
 * @date 2021-08-22 21:53:00
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CodeGenClass {

    String value();

    Class idType() default Long.class;

    String author() default "";

}
