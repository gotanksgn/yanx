package com.yanx.codegen;

import cn.hutool.core.bean.BeanUtil;
import com.yanx.common.utils.String_;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author gotanks
 * @date 2021-08-21 22:46:00
 */
@Data
public class MyClass {
    //类名
    private String className = "";

    //表名
    private String tableName = "";

    //类中文名称
    private String labelName = "";

    //模块名称
    private String moduleName = "";

    //主键的类型
    private String idType = "";

    //创建人
    private String author = "";

    //字段
    private List<MyField> dtoFields = new ArrayList<>();
    private List<MyField> voFields = new ArrayList<>();
    private List<MyField> qoFields = new ArrayList<>();

    //包名
    private String basePackage;
    private String entityPackage;
    private String repositoryPackage;
    private String managerPackage;
    private String commandPackage;
    private String apiPackage;
    private String dtoPackage;
    private String voPackage;
    private String qoPackage;
    private String mapperPackage;
    private String utilsPackage;
    private String queryPackage;

    //基础路径
    private String basePath;

    public void setBasePackage(String basePackage) {
        this.basePackage = basePackage.replace(".domain.entity", "");
        this.entityPackage = basePackage;
        this.repositoryPackage = this.getBasePackage() + ".domain.repository";
        this.managerPackage = this.getBasePackage() + ".domain.manager";
        this.commandPackage = this.getBasePackage() + ".command";
        this.apiPackage = this.getBasePackage() + ".api";
        this.dtoPackage = this.getBasePackage() + ".model.dto";
        this.voPackage = this.getBasePackage() + ".model.vo";
        this.qoPackage = this.getBasePackage() + ".model.qo";
        this.mapperPackage = this.getBasePackage() + ".infra.mapper";
        this.utilsPackage = this.getBasePackage() + ".infra.utils";
        this.queryPackage = this.getBasePackage() + ".query";
        //设置module
        if (this.moduleName == null) {
            List<String> split = String_.split(this.basePackage, ".");
            this.moduleName = split.get(split.size() - 1);
        }
    }

    public String getBasePath() {
        return basePath.replace("build/classes/java/main/", "src/main/java/");
    }

    public String getRepositoryPath() {
        return this.getBasePath() + this.getRepositoryPackage().replace(".", "/");
    }

    public String getManagerPath() {
        return this.getBasePath() + this.getManagerPackage().replace(".", "/");
    }

    public String getCommandPath() {
        return this.getBasePath() + this.getCommandPackage().replace(".", "/");
    }

    public String getApiPath() {
        return this.getBasePath() + this.getApiPackage().replace(".", "/");
    }

    public String getDtoPath() {
        return this.getBasePath() + this.getDtoPackage().replace(".", "/");
    }

    public String getVoPath() {
        return this.getBasePath() + this.getVoPackage().replace(".", "/");
    }

    public String getQoPath() {
        return this.getBasePath() + this.getQoPackage().replace(".", "/");
    }

    public String getMapperPath() {
        return this.getBasePath() + this.getMapperPackage().replace(".", "/");
    }

    public String getUtilsPath() {
        return this.getBasePath() + this.getUtilsPackage().replace(".", "/");
    }

    public String getQueryPath() {
        return this.getBasePath() + this.getQueryPackage().replace(".", "/");
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = BeanUtil.beanToMap(this);
        return map;
    }
}

