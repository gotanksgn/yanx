package com.yanx.framework.filter;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.FormContentFilter;

/**
 * 针对DELETE,PUT和PATCH这三种HTTP method分析其FORM表单参数，将其暴露为Servlet请求参数
 *
 * @author gotanks
 * @since 2020-02-06
 */
@Component
public class MyFormContentFilter extends FormContentFilter {
}
