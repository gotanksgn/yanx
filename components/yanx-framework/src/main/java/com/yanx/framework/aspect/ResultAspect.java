package com.yanx.framework.aspect;

import com.yanx.common.annotation.ApiResult;
import com.yanx.common.result.IResult;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ResultAspect {

    /**
     * 定义请求切入点
     */
    @Pointcut(value = "@annotation(apiResult)")
    public void serviceStatistics(ApiResult apiResult) {
    }

    /**
     * 异常通知
     *
     * @param apiResult
     */
    @AfterThrowing(value = "serviceStatistics(apiResult)")
    private void doAfterThrowing(ApiResult apiResult) {
        IResult.setResultClass(apiResult.resultClass());
    }

}
