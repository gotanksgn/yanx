package com.yanx.framework.aspect;

import com.yanx.common.annotation.ErrorNotice;
import com.yanx.framework.service.ErrorNoticeService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
public class ErrorNoticeAspect {

    @Autowired(required = false)
    private ErrorNoticeService errorNoticeService;

    /**
     * 定义请求切入点
     */
    @Pointcut(value = "@annotation(errorNotice)")
    public void serviceStatistics(ErrorNotice errorNotice) {
    }

    /**
     * 异常通知
     */
    @AfterThrowing(value = "serviceStatistics(errorNotice)", throwing = "e", argNames = "errorNotice,e")
    private void doAfterThrowing(ErrorNotice errorNotice, Throwable e) {
        if (errorNoticeService != null) {
            log.info("[发送异常信息][{}]{}", errorNotice.value(), e.getMessage());
            errorNoticeService.send(errorNotice.value(), e.getMessage());
        }
    }

}
