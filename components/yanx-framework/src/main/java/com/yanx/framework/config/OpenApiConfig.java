package com.yanx.framework.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * OpenApi配置类
 *
 * @author gotanks
 */
@Configuration
public class OpenApiConfig {

    @Value("${springdoc.info.title:YanX Api}")
    private String title;

    @Value("${springdoc.info.description:YanX Api}")
    private String description;

    @Value("${springdoc.info.version:1.0.0}")
    private String version;

    @Bean
    public OpenAPI springShopOpenAPI() {
        return new OpenAPI()
                .info(new Info().title(title)
                        .description(description)
                        .version(version)
                        .license(
                                new License()
                                        .name("Apache 2.0")
                                        .url("http://springdoc.org")
                        ));
    }

}
