package com.yanx.framework.service;

/**
 * 通知接口
 *
 * @author gotanks
 * @create 2024/7/16
 */
public interface ErrorNoticeService {

    /**
     * 发送通知信息
     *
     * @param type
     * @param msg
     */
    void send(String type, String msg);
}
