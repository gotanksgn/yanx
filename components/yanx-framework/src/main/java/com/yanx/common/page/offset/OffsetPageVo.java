package com.yanx.common.page.offset;

import com.yanx.common.page.PageConstant;
import com.yanx.common.page.PageVo;
import com.yanx.common.utils.ThreadLocal_;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OffsetPageVo extends PageVo {
    private Integer page;
    private Integer size;
    private Long total;

    public OffsetPageVo() {
        this.setPage(ThreadLocal_.get(PageConstant.PARAM_OFFSET_PAGE));
        this.setSize(ThreadLocal_.get(PageConstant.PARAM_OFFSET_SIZE));
        this.setTotal(ThreadLocal_.get(PageConstant.TOTAL_NUMBER));
    }
}
