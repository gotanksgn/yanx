package com.yanx.common.annotation;

import java.lang.annotation.*;

/**
 * 报错通知
 *
 * @author gotanks
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ErrorNotice {
    
    String value() default "";

}
