package com.yanx.common.page;

import com.yanx.common.page.cursor.CursorPageVo;
import com.yanx.common.page.offset.OffsetPageVo;
import com.yanx.common.utils.String_;
import com.yanx.common.utils.ThreadLocal_;

public class PageVo {

    public static PageVo build() {
        PageVo pageVo;
        if (String_.equals(PageHelper.getPageType(), PageConstant.Type.CURSOR)) {
            pageVo = new CursorPageVo();
        } else {
            pageVo = new OffsetPageVo();
        }
        ThreadLocal_.remove();
        return pageVo;
    }
}
