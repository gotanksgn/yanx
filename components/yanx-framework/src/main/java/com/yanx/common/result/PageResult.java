package com.yanx.common.result;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.NumberUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yanx.common.page.PageVo;
import com.yanx.common.page.offset.OffsetPageVo;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * 返回统一的数据格式
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PageResult<T> extends Result<T> implements IResult<T> {

    private static final long serialVersionUID = 1L;

    /**
     * 分页数据
     */
    private PageVo paging;


    @Override
    public void initPaging() {
        this.setPaging(PageVo.build());
    }

    private OffsetPageVo offsetPageVo() {
        return (OffsetPageVo) this.getPaging();
    }

    public Integer getPageNo() {
        return this.offsetPageVo().getPage();
    }

    public Integer getPageSize() {
        return this.offsetPageVo().getSize();
    }

    public Long getTotalNum() {
        return this.offsetPageVo().getTotal();
    }

    public Integer getTotalPage() {
        if (getTotalNum() == null || getPageSize() == null) {
            return null;
        }
        BigDecimal div = NumberUtil.div(getTotalNum(), getPageSize());
        return Convert.toInt(Math.ceil(div.doubleValue()));
    }

//    //兼容
//    public T getDatas() {
//        return this.getData();
//    }

    //兼容ruoyi
    public Long getTotal() {
        return this.offsetPageVo().getTotal();
    }

    public T getRows() {
        return this.getData();
    }

}
