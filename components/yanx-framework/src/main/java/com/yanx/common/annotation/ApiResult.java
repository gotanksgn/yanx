package com.yanx.common.annotation;

import com.yanx.common.result.IResult;
import com.yanx.common.result.Result;

import java.lang.annotation.*;

/**
 * 封装返回值
 *
 * @author gotanks
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ApiResult {
    String value() default "";

    boolean encode() default false;

    /**
     * 成功编码，默认为0
     *
     * @return
     */
    int successCode() default 0;

    /**
     * 成功信息，默认为success
     *
     * @return
     */
    String successMsg() default "success";

    /**
     * 自定义返回封装类
     *
     * @return
     */
    Class<? extends IResult> resultClass() default Result.class;
}
