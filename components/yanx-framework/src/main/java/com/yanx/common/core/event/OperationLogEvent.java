package com.yanx.common.core.event;

import com.yanx.common.core.dto.OperationLogDto;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.context.ApplicationEvent;

/**
 * 扩展事件
 *
 * @author gotanks
 * @create 2022/9/13
 */
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class OperationLogEvent extends ApplicationEvent {

    private static final long serialVersionUID = -3473945007366904091L;

    public OperationLogEvent(OperationLogDto source) {
        super(source);
    }
}
