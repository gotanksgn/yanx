package com.yanx.common.annotation;

import com.yanx.common.page.PageConstant;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * 分页开关
 *
 * @author gotanks
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Pagination {
    @AliasFor("type")
    String value() default PageConstant.Type.OFFSET;

    @AliasFor("value")
    String type() default PageConstant.Type.OFFSET;

    /**
     * 排序
     *
     * @return
     */
    String orderBy() default PageConstant.OrderBy.ASC;

    /**
     * 查询总数
     *
     * @return
     */
    boolean total() default false;

    /**
     * 每页数量
     *
     * @return
     */
    int limit() default 10;
}
