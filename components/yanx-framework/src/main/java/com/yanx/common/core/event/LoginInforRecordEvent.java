package com.yanx.common.core.event;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.context.ApplicationEvent;

/**
 * 扩展事件
 *
 * @author gotanks
 * @create 2022/9/13
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class LoginInforRecordEvent extends ApplicationEvent {

    private static final long serialVersionUID = 5711390521540326603L;

    public LoginInforRecordEvent(int source) {
        super(source);
        this.status = source;
    }

    private String username;

    private int status;

    private String message;

}
