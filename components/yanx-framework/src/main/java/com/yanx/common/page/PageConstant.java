package com.yanx.common.page;

import lombok.Data;

@Data
public class PageConstant {

    public static class Type {
        public final static String OFFSET = "offset";
        public final static String CURSOR = "cursor";
    }

    public static class OrderBy {
        public final static String ASC = "asc";
        public final static String DESC = "desc";
    }

    public final static String PREFIX = "page_";
    public final static String TYPE = "page_type";
    public final static String PARAM_ORDERBY = "page_param_orderby";
    public final static String PARAM_ORDERBY_FIELD = "page_param_orderby_field";

    public final static String PARAM_OFFSET_PAGE = "page_param_offset_page";
    public final static String PARAM_OFFSET_SIZE = "page_param_offset_size";

    public final static String PARAM_CURSOR_BEFORE = "page_param_cursor_before";
    public final static String PARAM_CURSOR_AFTER = "page_param_cursor_after";
    public final static String PARAM_CURSOR_LIMIT = "page_param_cursor_limit";

    public final static String CURSOR_BEFORE = "page_cursor_before";
    public final static String CURSOR_AFTER = "page_cursor_after";
    public final static String CURSOR_LINK = "page_link";
    public final static String CURSOR_HASNEXT = "page_hasnext";

    public final static String TOTAL_SWITCH = "page_total_switch";
    public final static String TOTAL_NUMBER = "page_total_number";

}
