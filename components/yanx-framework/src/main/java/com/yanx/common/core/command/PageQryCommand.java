package com.yanx.common.core.command;

import com.querydsl.jpa.impl.JPAQuery;
import com.yanx.common.page.PageConstant;
import com.yanx.common.page.PageHelper;
import com.yanx.common.utils.String_;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 增删改命令基类
 *
 * @author gotanks
 * @date 2021-04-19 18:40:00
 */
@Setter
@Getter
public abstract class PageQryCommand<T> extends QryCommand<T> {

    /**
     * 分页查询，返回List
     *
     * @param query
     * @param <E>
     * @return List<E>
     */
    protected <E> List<E> pageList(JPAQuery<E> query) {
        return pageQuery(query).fetch();
    }

    /**
     * 分页查询，返回JPAQuery
     *
     * @param query
     * @param <E>
     * @return JPAQuery<E>
     */
    protected <E> JPAQuery<E> pageQuery(JPAQuery<E> query) {
        if (PageHelper.pageable()) {
            //查询总数
            if (PageHelper.Count.countTotal()) {
                PageHelper.Count.setTotal((long) query.fetch().size());
            }

            //排序
//            query.orderBy("id");
//            Field<Object> field = DSL.field(PageHelper.OrderBy.field());
//            if (PageHelper.OrderBy.asc()) {
//                query.addOrderBy(field.asc());
//            } else if (PageHelper.OrderBy.desc()) {
//                query.addOrderBy(field.desc());
//            }
            //cursor分页
            if (String_.equals(PageHelper.getPageType(), PageConstant.Type.CURSOR)) {
//                if (PageHelper.Cursor.getAfter() > 0) {
//                    query.addSeekAfter(DSL.val(PageHelper.Cursor.getAfter()));
//                }
//                query.addLimit(PageHelper.Cursor.getLimit());
            }
            //offset分页
            else {
                query.offset(PageHelper.Offset.getOffset());
                query.limit(PageHelper.Offset.getSize());
            }
        }
        return query;
    }
}
