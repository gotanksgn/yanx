package org.hibernate.dialect;

/**
 * @author Gail Badner
 */
public class MySQL57DialectWithoutFK extends MySQL57Dialect {

    @Override
    public String getAddForeignKeyConstraintString(
            String constraintName,
            String[] foreignKey,
            String referencedTable,
            String[] primaryKey,
            boolean referencesPrimaryKey) {
        return " ";
//		final String cols = StringHelper.join( ", ", foreignKey );
//		final String referencedCols = StringHelper.join( ", ", primaryKey );
//		return String.format(
//				" add constraint %s foreign key (%s) references %s (%s)",
//				constraintName,
//				cols,
//				referencedTable,
//				referencedCols
//		);
    }

    @Override
    public String getDropForeignKeyString() {
        return " ";
    }
}
