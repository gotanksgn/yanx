package com.yanx.common.command.executor;

import org.springframework.beans.factory.annotation.Autowired;

public class ExecutorHolder {
    /**
     * {@link DomainExecutor}
     */
    @Deprecated
    @Autowired(required = false)
    protected CommandExecutor commandExecutor;

    @Autowired
    protected CommonExecutor commonExecutor;

    @Autowired
    protected DomainExecutor domainExecutor;

    @Autowired
    protected QueryExecutor queryExecutor;

}
