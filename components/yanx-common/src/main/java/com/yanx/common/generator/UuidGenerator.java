package com.yanx.common.generator;

import cn.hutool.core.util.IdUtil;
import com.yanx.common.core.domain.entity.IBaseEntity;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.UUIDGenerator;

import java.io.Serializable;

public class UuidGenerator extends UUIDGenerator {

    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
        Serializable oriId = ((IBaseEntity) object).getId();
        if (oriId != null) {
            return oriId;
        }
        Object id = IdUtil.fastSimpleUUID();
        if (id != null) {
            return (Serializable) id;
        }
        return super.generate(session, object);
    }

}
