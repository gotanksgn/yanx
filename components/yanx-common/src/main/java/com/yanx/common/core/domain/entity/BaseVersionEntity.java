package com.yanx.common.core.domain.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

/**
 * 基类
 *
 * @author gotanks
 */
@Data
@EqualsAndHashCode(callSuper = true)
@MappedSuperclass
public class BaseVersionEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 乐观锁
     */
    @Column
    @Version
    private Long version;

}