package com.yanx.common.enums;


import com.yanx.common.exception.IExceptionEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @description: 登录错误枚举
 * @author: gotanks
 * @create: 2019-12-19 11:58
 **/
@Getter
@AllArgsConstructor
public enum AuthError implements IExceptionEnum {

    /**
     * 登录错误枚举
     */
    E2001("验证码错误"),
    E2002("验证码已失效"),
    E2003("用户不存在/密码错误"),
    E2004("密码输入错误{}次，帐户锁定{}分钟"),
    E2005("非法参数异常"),
    E2006("密码错误"),
    E2007("登录失败"),
    ;

    private String msg;

}
