package com.yanx.common.exception;

import lombok.Getter;

/**
 * 基础异常
 *
 * @author gotanks
 */
@Getter
public class BaseException extends RuntimeException {
    
    private static final long serialVersionUID = 1L;

    /**
     * 错误码
     */
    private Integer code;

    /**
     * 错误消息
     */
    private String message;

    public BaseException(Throwable cause) {
        this(null, cause.getMessage(), cause);
    }

    public BaseException(Integer code, String message) {
        this(code, message, null);
    }

//    public BaseException(String message) {
//        this(null, message, null);
//    }
//
//    public BaseException(String message, Throwable cause) {
//        this(null, message, cause);
//    }

    public BaseException(Integer code, Throwable cause) {
        this(code, cause.getMessage(), cause);
    }

    public BaseException(Integer code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
        this.message = message;
    }

}
