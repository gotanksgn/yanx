package com.yanx.common.core.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = true)
public class BasePageVo<ID extends Serializable> extends BaseVo<ID> {
    private static final long serialVersionUID = 1L;
}
