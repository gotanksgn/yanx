package com.yanx.common.core.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class IdVo<ID extends Serializable> extends BaseVo<ID> {

    public static <T extends Serializable> IdVo of(T id) {
        IdVo<T> idVo = new IdVo<>();
        idVo.setId(id);
        return idVo;
    }
}
