package com.yanx.common.core.domain.entity;

import java.io.Serializable;

/**
 * @author gotanks
 * @create 2022/4/28
 */
public interface IBaseLogicEntity<ID extends Serializable> extends IBaseEntity<ID> {

    default Boolean getDeleted() {
        return false;
    }

    default void setDeleted(Boolean deleted) {
    }

}
