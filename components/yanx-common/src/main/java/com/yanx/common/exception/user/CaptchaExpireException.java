package com.yanx.common.exception.user;

import com.yanx.common.enums.AuthError;
import com.yanx.common.exception.BusinessException;

/**
 * 验证码失效异常类
 *
 * @author ruoyi
 */
public class CaptchaExpireException extends BusinessException {
    private static final long serialVersionUID = 1L;

    public CaptchaExpireException() {
        super(AuthError.E2002);
    }
}
