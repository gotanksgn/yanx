package com.yanx.common.enums;


import com.yanx.common.exception.IExceptionEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @description: 鉴权校验结果枚举
 * @author: gotanks
 * @create: 2019-12-19 11:58
 **/
@Getter
@AllArgsConstructor
public enum TokenError implements IExceptionEnum {

    /**
     * 鉴权结果枚举
     */
    E1001("Token已过期"),
    E1002("Token格式错误"),
    E1003("Token不存在或无效Token"),
    E1004("签名失败"),
    E1005("非法参数异常"),
    E1006("密码错误"),
    E1007("登录失败"),
    ;

    private String msg;

}
