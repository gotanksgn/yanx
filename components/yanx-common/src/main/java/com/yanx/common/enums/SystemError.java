package com.yanx.common.enums;

import com.yanx.common.exception.IExceptionEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @description: 用户校验结果枚举
 * @author: gotanks
 * @create: 2020-01-13 18:14
 **/
@Getter
@AllArgsConstructor
public enum SystemError implements IExceptionEnum {

    /**
     * 定义系统异常
     */
    E990("自定义异常"),
    E403("权限异常"),
    E996("权限异常"),
    E997("票据异常"),
    E998("请求异常"),
    E999("系统错误");

    private String msg;

}
