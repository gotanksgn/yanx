package com.yanx.common.command;

/**
 * The command invoker for internal usage.
 *
 * @author gotanks
 */
public interface Executor {

    /**
     * get receiver
     *
     * @param clazz
     * @return
     */
    <R extends Receiver> R getReceiver(Class<R> clazz);

    /**
     * execute a command
     */
    <T> T execute(Command<T> command);

}
