package com.yanx.common.command.executor;

import com.yanx.common.command.Executor;
import com.yanx.common.command.Receiver;
import com.yanx.common.utils.String_;

import java.util.Map;

abstract class BaseExecutor implements Executor {

    protected Map<String, ? extends Receiver> receiverMap;

    @Override
    public <R extends Receiver> R getReceiver(Class<R> clazz) {
        Receiver receiver = receiverMap.get(String_.lowerFirst(clazz.getSimpleName()));
        if (receiver == null) {
            receiver = receiverMap.get(clazz.getSimpleName());
        }
        if (receiver == null) {
            for (Receiver service : receiverMap.values()) {
                if (clazz.equals(service.getClass()) || clazz.equals(service.getClass().getSuperclass())) {
                    receiver = service;
                    break;
                }
            }
        }
        return (R) receiver;
    }
}
