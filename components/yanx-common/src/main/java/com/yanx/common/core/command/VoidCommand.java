package com.yanx.common.core.command;

import com.yanx.common.command.Executor;
import com.yanx.common.command.UndoCommand;

/**
 * 返回值为空的命令基类
 *
 * @author gotanks
 * @date 2021-04-19 18:40:00
 */
public abstract class VoidCommand implements UndoCommand<Void> {

    @Override
    public Void execute(Executor executor) {
        handler(executor);
        return null;
    }

    public abstract void handler(Executor executor);
}
