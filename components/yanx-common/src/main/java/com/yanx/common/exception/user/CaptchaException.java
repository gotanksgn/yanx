package com.yanx.common.exception.user;

import com.yanx.common.enums.AuthError;
import com.yanx.common.exception.BusinessException;

/**
 * 验证码错误异常类
 *
 * @author ruoyi
 */
public class CaptchaException extends BusinessException {
    private static final long serialVersionUID = 1L;

    public CaptchaException() {
        super(AuthError.E2001);
    }
}
