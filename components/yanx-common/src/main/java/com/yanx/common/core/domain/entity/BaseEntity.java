package com.yanx.common.core.domain.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import static com.yanx.common.generator.GeneratorType.GENERATOR_NAME;
import static com.yanx.common.generator.GeneratorType.SNOWFLAKE;

/**
 * 基类
 *
 * @author gotanks
 */
@Data
@MappedSuperclass
public class BaseEntity implements IBaseEntity<Long> {

    private static final long serialVersionUID = 1L;
    /**
     * uuid主键
     */
    @Id
    @Column(length = 20)
    @GenericGenerator(name = GENERATOR_NAME, strategy = SNOWFLAKE)
    @GeneratedValue(generator = GENERATOR_NAME)
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    @Override
//    public void setId(Long id) {
//        this.id = id;
//    }

//    @Override
//    public Long generateId() {
//        return Snowflake_.INSTANCE.nextId();
//    }

}