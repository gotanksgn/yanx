package com.yanx.common.core.command;

import cn.hutool.extra.spring.SpringUtil;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.yanx.common.command.Command;
import lombok.Getter;
import lombok.Setter;

/**
 * 查询命令基类
 *
 * @author gotanks
 * @date 2021-04-19 18:40:00
 */
@Setter
@Getter
public abstract class QryCommand<T> implements Command<T> {

    protected JPAQueryFactory queryFactory = SpringUtil.getBean(JPAQueryFactory.class);

}
