package com.yanx.common.utils;

import cn.hutool.core.net.NetUtil;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * 网络工具类
 * 基于Hutool
 *
 * @author gotanks
 * @version 1.0
 * @since 2020-05-13
 */
public final class Net_ extends NetUtil {

    public static boolean tryConnect(String ip, int port, int timeout) {
        try (Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress(ip, port), timeout);
            return true;
        } catch (IOException e) {
            System.out.println("连接不通");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
