package com.yanx.common.core.domain.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

/**
 * 更新时间实体基类
 *
 * @author gotanks
 */
@Data
@EqualsAndHashCode(callSuper = true)
@MappedSuperclass
public class BaseVersionTimeEntity extends BaseTimeEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 乐观锁
     */
    @Column
    @Version
    private Long version;
}