package com.yanx.common.command.executor;

import com.yanx.common.command.Command;
import com.yanx.common.command.receiver.QueryReceiver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class QueryExecutor extends BaseExecutor {

    @Autowired
    public QueryExecutor(Map<String, QueryReceiver> receiverMap) {
        this.receiverMap = receiverMap;
    }

    /**
     * Execute a command with the default
     */
    @Override
    public <T> T execute(Command<T> command) {
        return command.execute(this);
    }
}
