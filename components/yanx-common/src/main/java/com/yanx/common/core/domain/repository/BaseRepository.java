package com.yanx.common.core.domain.repository;

import com.yanx.common.core.domain.entity.IBaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

@NoRepositoryBean
public interface BaseRepository<T extends IBaseEntity, ID extends Serializable> extends JpaRepository<T, ID> {

    @Transactional(readOnly = true)
    @Query("select e from #{#entityName} e where e.id in ?1")
    Iterable<T> findAll(Iterable<ID> ids);

    @Transactional(readOnly = true)
    @Query("select e from #{#entityName} e where e.id in ?1")
    Iterable<T> findAll(ID... ids);

    @Transactional(readOnly = true)
    default boolean exists(ID id) {
        return findById(id).isPresent();
    }

    @Transactional
    @Query("delete from #{#entityName} e where e.id in ?1")
    @Modifying
    void deleteInBatchById(Iterable<ID> ids);
}