package com.yanx.common.exception;


import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.CharUtil;

public interface IExceptionEnum {

    /**
     * 获取异常编码
     *
     * @return
     */
    default int toCode() {
        StringBuilder num = new StringBuilder();
        for (char c : this.toString().toCharArray()) {
            if (CharUtil.isNumber(c)) {
                num.append(c);
            }
        }
        return Convert.toInt(num, -1);
    }

    /**
     * 获取异常信息
     *
     * @return
     */
    String getMsg();

}
