package com.yanx.common.core.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
public class BasePageDto<ID extends Serializable> extends BaseDto<ID> {

    private static final long serialVersionUID = 1L;

    private Integer pageNo;

    private Integer pageSize;

}
