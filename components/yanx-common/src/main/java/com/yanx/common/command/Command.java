package com.yanx.common.command;

/**
 * The command.
 *
 * @author gotanks
 */
public interface Command<T> {

    T execute(Executor executor);

}
