package com.yanx.common.command;

/**
 * The undo command.
 *
 * @author gotanks
 */
public interface UndoCommand<T> extends Command<T> {

    default T undo(Executor executor) {
        return null;
    }

}
