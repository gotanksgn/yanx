package com.yanx.common.generator;

import com.yanx.common.core.domain.entity.IBaseEntity;
import com.yanx.common.utils.Snowflake_;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentityGenerator;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class SnowflakeGenerator extends IdentityGenerator {
    private final static Snowflake_ snowflake = Snowflake_.INSTANCE;

    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
        if (object instanceof IBaseEntity) {
            Serializable oriId = ((IBaseEntity) object).getId();
            if (oriId != null) {
                return oriId;
            }
        }
        Object id = snowflake.nextId();
        if (id != null) {
            return (Serializable) id;
        }
        return super.generate(session, object);
    }

    private static long getWorkerId() {
        InetAddress address;
        try {
            address = InetAddress.getLocalHost();
        } catch (final UnknownHostException e) {
            throw new IllegalStateException("Cannot get LocalHost InetAddress, please check your network!");
        }
        byte[] ipAddressByteArray = address.getAddress();
        long workerId = 0L;
        // IPV4
        if (ipAddressByteArray.length == 4) {
            for (int i = 2; i < 4; i++) {
                workerId += ipAddressByteArray[i] & 0xFF;
            }
        }
        // IPV6
        else if (ipAddressByteArray.length == 16) {
            for (byte byteNum : ipAddressByteArray) {
                workerId += byteNum & 0B111111;
            }
        } else {
            throw new IllegalStateException("Bad LocalHost InetAddress, please check your network!");
        }
        return workerId;
    }

}
