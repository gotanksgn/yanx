package com.yanx.common.core.command;

import com.yanx.common.command.UndoCommand;

/**
 * 普通命令基类
 *
 * @author gotanks
 * @date 2024-09-18
 */
public abstract class CommonCommand<T> implements UndoCommand<T> {

}
