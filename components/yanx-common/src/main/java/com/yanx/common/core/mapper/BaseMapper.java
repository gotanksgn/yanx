package com.yanx.common.core.mapper;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface BaseMapper<S, T> {
    /**
     * 将源对象转换为目标对象
     *
     * @param s
     * @return T
     */
    T to(S s);

    /**
     * 将源对象集合转换为目标对象集合
     *
     * @param es
     * @return List<T>
     */
    List<T> to(List<S> es);

    /**
     * 将源对象集合转换为目标对象集合
     *
     * @param es
     * @return Set<T>
     */
    Set<T> to(Set<S> es);

    /**
     * 将源对象Map转换为目标对象Map
     *
     * @param es
     * @return Map<Long, T>
     */
    Map<Serializable, T> to(Map<Serializable, S> es);

}
