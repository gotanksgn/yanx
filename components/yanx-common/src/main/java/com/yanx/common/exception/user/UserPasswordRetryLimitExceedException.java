package com.yanx.common.exception.user;

import com.yanx.common.enums.AuthError;
import com.yanx.common.exception.BusinessException;

/**
 * 用户错误最大次数异常类
 *
 * @author ruoyi
 */
public class UserPasswordRetryLimitExceedException extends BusinessException {
    private static final long serialVersionUID = 1L;

    public UserPasswordRetryLimitExceedException(int retryLimitCount, int lockTime) {
        super(AuthError.E2004);
    }
}
