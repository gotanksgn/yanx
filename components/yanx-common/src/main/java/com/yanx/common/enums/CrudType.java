package com.yanx.common.enums;

/**
 * CRUD类别
 *
 * @author gotanks
 */
public enum CrudType {
    CREATE,
    UPDATE,
    REMOVE,
    SELECT,
    ;
}
