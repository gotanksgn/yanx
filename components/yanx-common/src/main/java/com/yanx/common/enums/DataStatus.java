package com.yanx.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 数据状态
 *
 * @author gotanks
 */
public enum DataStatus {
    /**
     * 正常
     */
    ENABLE,

    /**
     * 异常
     */
    DISABLE,
    ;

    @JsonValue
    public int toValue() {
        return ordinal();
    }
}
