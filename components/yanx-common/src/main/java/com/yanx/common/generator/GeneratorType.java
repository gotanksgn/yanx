package com.yanx.common.generator;

/**
 * @author gotanks
 * @create 2022/10/1
 */
public final class GeneratorType {
    public final static String GENERATOR_NAME = "yanx-id";

    public final static String SNOWFLAKE = "com.yanx.common.generator.SnowflakeGenerator";
    public final static String UUID = "com.yanx.common.generator.UuidGenerator";
}
