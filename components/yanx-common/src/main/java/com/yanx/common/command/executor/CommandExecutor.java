package com.yanx.common.command.executor;

import com.yanx.common.command.Command;
import com.yanx.common.command.UndoCommand;
import com.yanx.common.command.receiver.DomainReceiver;
import com.yanx.common.core.command.VoidCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * this command class is @Deprecated, pleases use {@link DomainExecutor} instead
 */
@Deprecated
@Service
public class CommandExecutor extends BaseExecutor {

    @Autowired
    public CommandExecutor(Map<String, DomainReceiver> receiverMap) {
        this.receiverMap = receiverMap;
    }

    /**
     * Execute a command with the default
     */
    @Override
    @Transactional
    public <T> T execute(Command<T> command) {
        return command.execute(this);
    }

    /**
     * Execute a void command with the default
     *
     * @return
     */
    @Transactional
    public CommandExecutor execute(VoidCommand command) {
        command.execute(this);
        return this;
    }

    /**
     * undo a command with the default
     */
    @Transactional
    public <T> T undo(UndoCommand<T> command) {
        return command.undo(this);
    }
}
