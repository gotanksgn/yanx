package com.yanx.common.core.api;

import com.yanx.common.command.executor.ExecutorHolder;
import org.springframework.web.bind.annotation.GetMapping;

public class BaseApi extends ExecutorHolder {

    @GetMapping(value = "/isalive")
    public String isAlive() {
        return "ok";
    }
}
