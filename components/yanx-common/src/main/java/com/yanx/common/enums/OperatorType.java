package com.yanx.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 操作人类别
 *
 * @author gotanks
 */
public enum OperatorType {
    /**
     * 其它
     */
    OTHER,

    /**
     * 后台用户
     */
    MANAGE,

    /**
     * 手机端用户
     */
    MOBILE;

    @JsonValue
    public int toValue() {
        return ordinal();
    }
}
