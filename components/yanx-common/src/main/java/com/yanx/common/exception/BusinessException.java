package com.yanx.common.exception;

import com.yanx.common.enums.SystemError;
import com.yanx.common.utils.String_;

import java.io.Serializable;

public class BusinessException extends BaseException implements Serializable {

    private static final long serialVersionUID = 1L;

    public BusinessException() {
        this(SystemError.E990.getMsg());
    }

    public BusinessException(String msg, Object... objects) {
        super(SystemError.E990.toCode(), String_.format(msg, objects));
    }

    public BusinessException(Throwable cause) {
        super(cause instanceof BaseException ? ((BaseException) cause).getCode() : SystemError.E999.toCode(), cause);
    }

    public BusinessException(IExceptionEnum exceptionEnum, Object... objects) {
        this(null, exceptionEnum, objects);
    }

    public BusinessException(Throwable cause, IExceptionEnum exceptionEnum, Object... objects) {
        super(exceptionEnum.toCode(), String_.format(exceptionEnum.getMsg(), objects), cause);
    }

    public BusinessException(Integer code, String message) {
        super(code, message);
    }

    public BusinessException(Integer code, Throwable cause) {
        super(code, cause);
    }

    public BusinessException(Integer code, String message, Throwable cause) {
        super(code, message, cause);
    }
}
