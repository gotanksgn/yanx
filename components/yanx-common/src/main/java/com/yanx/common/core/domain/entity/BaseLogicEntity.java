package com.yanx.common.core.domain.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * 基类
 *
 * @author gotanks
 */
@Data
@MappedSuperclass
@EqualsAndHashCode(callSuper = true)
public class BaseLogicEntity extends BaseEntity implements IBaseLogicEntity<Long> {

    private static final long serialVersionUID = 1L;

    /**
     * 删除状态：0:未删除,1:已删除
     */
    @Column
    private Boolean deleted = false;

    @Override
    public Boolean getDeleted() {
        if (deleted == null) {
            return false;
        }
        return deleted;
    }

}