package com.yanx.common.core;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author gotanks
 * @date 2021-09-18 09:10:00
 */
@Slf4j
public class BaseApplication {

    private static final String SERVER_PORT = "server.port";
    private static final String SPRING_APPLICATION_NAME = "spring.application.name";

    public static void startApplication(Class clazz, String[] args) {
        try {
            String hostAddress = InetAddress.getLocalHost().getHostAddress();
            System.setProperty("ip.address", hostAddress);
            SpringApplication app = new SpringApplication(clazz);
            final ApplicationContext applicationContext = app.run(args);
            Environment env = applicationContext.getEnvironment();
            log.info("\n--------------------------------------------------------------\n\t" +
                            "Application '{}' is running! Access URLs:\n\t" +
                            "Local: \t\thttp://localhost:{}\n\t" +
                            "External: \thttp://{}:{}\n" +
                            "--------------------------------------------------------------",
                    env.getProperty(SPRING_APPLICATION_NAME),
                    env.getProperty(SERVER_PORT),
                    hostAddress,
                    env.getProperty(SERVER_PORT));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
