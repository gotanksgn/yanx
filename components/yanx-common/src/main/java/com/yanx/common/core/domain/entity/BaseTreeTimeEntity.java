package com.yanx.common.core.domain.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * 树状实体基类
 *
 * @author gotanks
 */
@Data
@EqualsAndHashCode(callSuper = true)
@MappedSuperclass
public class BaseTreeTimeEntity extends BaseTimeEntity {

    private static final long serialVersionUID = 1L;

    @Column(length = 20)
    private Long parentId;

    @Column
    private String ancestors;

    @Column
    private Integer treeLevel;
}