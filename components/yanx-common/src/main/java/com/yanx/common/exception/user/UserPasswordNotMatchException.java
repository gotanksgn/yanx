package com.yanx.common.exception.user;

import com.yanx.common.enums.AuthError;
import com.yanx.common.exception.BusinessException;

/**
 * 用户密码不正确或不符合规范异常类
 *
 * @author ruoyi
 */
public class UserPasswordNotMatchException extends BusinessException {
    private static final long serialVersionUID = 1L;

    public UserPasswordNotMatchException() {
        super(AuthError.E2003);
    }
}
