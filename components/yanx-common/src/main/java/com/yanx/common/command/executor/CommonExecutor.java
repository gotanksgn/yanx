package com.yanx.common.command.executor;

import com.yanx.common.command.Command;
import com.yanx.common.command.Receiver;
import com.yanx.common.command.UndoCommand;
import com.yanx.common.core.command.VoidCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class CommonExecutor extends BaseExecutor {

    @Autowired
    public CommonExecutor(Map<String, Receiver> receiverMap) {
        this.receiverMap = receiverMap;
    }

    /**
     * Execute a command with the default
     */
    @Override
    public <T> T execute(Command<T> command) {
        return command.execute(this);
    }

    /**
     * Execute a void command with the default
     *
     * @return
     */
    public CommonExecutor execute(VoidCommand command) {
        command.execute(this);
        return this;
    }

    /**
     * undo a command with the default
     */
    public <T> T undo(UndoCommand<T> command) {
        return command.undo(this);
    }
}
