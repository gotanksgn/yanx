package com.yanx.common.core.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class BaseTreeVo<ID extends Serializable> extends BaseVo<ID> {

    private static final long serialVersionUID = 1L;

    /**
     * 父节点ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private ID parentId;

    /**
     * 父节点名称
     */
    private String parentName;

    /**
     * 祖先节点
     */
    private String ancestors;

    /**
     * 树层级
     */
    private Integer treeLevel;

    /**
     * 子节点
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<? extends BaseTreeVo<ID>> children = new ArrayList<>();
}
