package com.yanx.common.core.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 对应quasar树组件的节点模型结构
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuasarTreeVo {

    private String key;

    private String label;

    private String icon;

    private String iconColor;

    private String img;

    private String avatar;

    private List<QuasarTreeVo> children;

    private Boolean disabled;

    private Boolean expandable;

    private Boolean selectable;

    private Boolean tickable;

    private Boolean noTick;

    private String tickStrategy;

    private Boolean lazy;

    public void addChild(QuasarTreeVo quasarTreeVo) {
        if (children == null) {
            children = new ArrayList<>();
        }
        children.add(quasarTreeVo);
    }
}
