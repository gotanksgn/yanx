package com.yanx.common.core.domain.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

/**
 * 更新时间实体基类
 *
 * @author gotanks
 */
@Data
@EqualsAndHashCode(callSuper = true)
@MappedSuperclass
public class BaseTimeLogicEntity extends BaseLogicEntity {

    private static final long serialVersionUID = 1L;

    @Column(updatable = false)
    @CreationTimestamp
    private LocalDateTime createTime;

    @Column
    @UpdateTimestamp
    private LocalDateTime updateTime;
}