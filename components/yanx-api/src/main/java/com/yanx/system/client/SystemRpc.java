package com.yanx.system.client;

import com.yanx.system.client.dto.LoginUserVo;
import com.yanx.system.client.dto.RouterVo;

import java.util.List;

/**
 * 系统RPC接口
 *
 * @author gotanks
 * @create 2021-01-29 23:38
 **/
public interface SystemRpc {

    /**
     * 根据用户名获取用户登录信息
     *
     * @param username
     * @return
     */
    LoginUserVo getLoginUserByUsername(String username);

    /**
     * 获取当前用户路由信息
     *
     * @return
     */
    List<RouterVo> getCurrentRouters();

    /**
     * 获取是否开启验证码
     */
    boolean captchaEnabled();

    /**
     * 获取是否开启注册
     */
    boolean registerEnabled();

    /**
     * 注册
     */
    void register(String username, String password);
}
