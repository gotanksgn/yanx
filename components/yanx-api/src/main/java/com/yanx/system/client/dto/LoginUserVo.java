package com.yanx.system.client.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

/**
 * 用户登录信息
 *
 * @author gotanks
 * @create 2021-01-29 23:38
 **/
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginUserVo implements Serializable {

    private static final long serialVersionUID = 5522226719559509524L;
    
    /**
     * 用户信息
     */
    private Map<String, Object> user;

    /**
     * 是否禁用
     */
    private boolean disabled;

    /**
     * 是否锁定
     */
    private boolean locked;

    /**
     * 是否删除
     */
    private boolean deleted;

    /**
     * 角色列表
     */
    private Set<String> roles;

    /**
     * 权限列表
     */
    private Set<String> permissions;
}
