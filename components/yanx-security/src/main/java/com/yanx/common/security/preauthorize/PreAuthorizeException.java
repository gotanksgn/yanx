package com.yanx.common.security.preauthorize;

import com.yanx.common.enums.SystemError;
import com.yanx.common.exception.BaseException;

/**
 * 权限异常
 *
 * @author gotanks
 */
public class PreAuthorizeException extends BaseException {
    private static final long serialVersionUID = 1L;

    public PreAuthorizeException() {
        super(SystemError.E403.toCode(), SystemError.E403.getMsg());
    }

}
