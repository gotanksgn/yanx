package com.yanx.common.security;

import com.yanx.common.security.service.LoginExtendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * 用户验证处理
 *
 * @author gotanks
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private LoginExtendService loginExtendService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return loginExtendService.getLoginUserByUsername(username);
    }
}
