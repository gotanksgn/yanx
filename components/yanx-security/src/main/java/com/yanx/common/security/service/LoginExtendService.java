package com.yanx.common.security.service;

import com.ruoyi.common.core.domain.model.LoginUser;

/**
 * @author gotanks
 * @create 2023/2/6
 */
public interface LoginExtendService {
    /**
     * 根据用户账号获取用户信息
     *
     * @param username
     * @return
     */
    LoginUser getLoginUserByUsername(String username);

    /**
     * 校验验证码
     */
    void validateCaptcha(String username, String code, String uuid);
}
