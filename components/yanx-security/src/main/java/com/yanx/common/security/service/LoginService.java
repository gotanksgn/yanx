package com.yanx.common.security.service;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.yanx.common.exception.BusinessException;
import com.yanx.common.exception.user.UserPasswordNotMatchException;
import com.yanx.common.security.LoginInforRecordUtils;
import com.yanx.common.security.model.LoginDto;
import com.yanx.common.security.model.LoginVo;
import com.yanx.common.utils.Message_;
import com.yanx.common.utils.ThreadLocal_;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 登录校验方法
 *
 * @author gotanks
 */
@Service
public class LoginService {

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private PasswordService passwordService;

    @Autowired
    private LoginExtendService loginExtendService;

    /**
     * 登录验证
     *
     * @param loginDto 用户登录信息
     * @return LoginVo 结果
     */
    public LoginVo login(LoginDto loginDto) {
        String username = loginDto.getUsername();
        String password = loginDto.getPassword();
        String code = loginDto.getCode();
        String uuid = loginDto.getUuid();

        // 校验验证码
        loginExtendService.validateCaptcha(username, code, uuid);

        // 用户验证
        Authentication authentication;
        try {
            passwordService.validateRetry(username);
            // 该方法会去调用UserDetailsServiceImpl.loadUserByUsername
            Authentication authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
            ThreadLocal_.set("AuthenticationToken", authenticationToken);
            authentication = authenticationManager.authenticate(authenticationToken);
        } catch (LockedException |
                 CredentialsExpiredException |
                 AccountExpiredException |
                 DisabledException e) {
//            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.not.match")));
            LoginInforRecordUtils.error(username, Message_.message("user.password.not.match"));
            throw new UserPasswordNotMatchException();
        } catch (BadCredentialsException e) {
//            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.not.match")));
            passwordService.increaseRetry(username);
            throw new UserPasswordNotMatchException();
        } catch (BusinessException e) {
            LoginInforRecordUtils.error(username, e.getMessage());
//            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, e.getMessage()));
            throw e;
        } catch (Exception e) {
            LoginInforRecordUtils.error(username, e.getMessage());
//            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, e.getMessage()));
            throw new BusinessException(e);
        } finally {
            ThreadLocal_.remove("AuthenticationToken");
        }
        passwordService.cleanRetry(username);

//        AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_SUCCESS, MessageUtils.message("user.login.success")));
        LoginInforRecordUtils.success(username, Message_.message("user.login.success"));

        LoginUser loginUser = (LoginUser) authentication.getPrincipal();
        // 生成token
        String token = tokenService.createToken(loginUser);
        LoginVo loginVo = new LoginVo(token, loginUser.getUsername(), loginUser.get("fullname"));
        return loginVo;
    }

}
