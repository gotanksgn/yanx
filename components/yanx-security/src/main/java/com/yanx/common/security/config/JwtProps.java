package com.yanx.common.security.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "jwt", ignoreUnknownFields = false)
public class JwtProps {

    // 令牌自定义标识
    private String header = "Authorization";

    // 令牌秘钥
    private String secret = "abcdefghijklmnopqrstuvwxyz";

    // 令牌有效期（默认30分钟）
    private int expireTime = 30;

}
