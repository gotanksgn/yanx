package com.yanx.common.security;

import cn.hutool.extra.spring.SpringUtil;
import com.yanx.common.core.event.LoginInforRecordEvent;

/**
 * 扩展事件
 *
 * @author gotanks
 * @create 2022/9/13
 */
public class LoginInforRecordUtils {

    public static void success(String username, String message) {
        LoginInforRecordEvent loginInforRecordEvent = new LoginInforRecordEvent(0);
        loginInforRecordEvent.setUsername(username);
        loginInforRecordEvent.setMessage(message);
        SpringUtil.getApplicationContext().publishEvent(loginInforRecordEvent);
    }

    public static void error(String username, String message) {
        LoginInforRecordEvent loginInforRecordEvent = new LoginInforRecordEvent(1);
        loginInforRecordEvent.setUsername(username);
        loginInforRecordEvent.setMessage(message);
        SpringUtil.getApplicationContext().publishEvent(loginInforRecordEvent);
    }
}
