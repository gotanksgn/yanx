package com.yanx.common.security.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 用户登录信息
 *
 * @author gotanks
 * @create 2021-01-29 23:38
 **/
@Data
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginVo {

    /**
     * token
     */
    private String token;

    /**
     * 用户账号
     */
    private String username;

    /**
     * 用户全名
     */
    private String fullname;

}
